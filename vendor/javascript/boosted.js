// Software Name : NIF TZ
// SPDX-FileCopyrightText: Copyright (c) Orange SA
// SPDX-License-Identifier: Apache-2.0
//
// This software is distributed under the Apache-2.0
// ,
// See the "LICENSES" directory for more details.
//
// Authors:
// - Efflam Castel <efflam.castel@orange.com>

import * as t from "@popperjs/core";
(function (t, e) {
  "object" === typeof exports && "undefined" !== typeof module
    ? e()
    : "function" === typeof define && define.amd
      ? define(e)
      : e();
})(0, function () {
  /**
   * Applies the :focus-visible polyfill at the given scope.
   * A scope in this case is either the top-level Document or a Shadow Root.
   *
   * @param {(Document|ShadowRoot)} scope
   * @see https://github.com/WICG/focus-visible
   */
  function applyFocusVisiblePolyfill(t) {
    var e = true;
    var s = false;
    var n = null;
    var i = {
      text: true,
      search: true,
      url: true,
      tel: true,
      email: true,
      password: true,
      number: true,
      date: true,
      month: true,
      week: true,
      time: true,
      datetime: true,
      "datetime-local": true,
    };
    /**
     * Helper function for legacy browsers and iframes which sometimes focus
     * elements like document, body, and non-interactive SVG.
     * @param {Element} el
     */ function isValidFocusTarget(t) {
      return !!(
        t &&
        t !== document &&
        "HTML" !== t.nodeName &&
        "BODY" !== t.nodeName &&
        "classList" in t &&
        "contains" in t.classList
      );
    }
    /**
     * Computes whether the given element should automatically trigger the
     * `focus-visible` class being added, i.e. whether it should always match
     * `:focus-visible` when focused.
     * @param {Element} el
     * @return {boolean}
     */ function focusTriggersKeyboardModality(t) {
      var e = t.type;
      var s = t.tagName;
      return (
        !("INPUT" !== s || !i[e] || t.readOnly) ||
        ("TEXTAREA" === s && !t.readOnly) ||
        !!t.isContentEditable
      );
    }
    /**
     * Add the `focus-visible` class to the given element if it was not added by
     * the author.
     * @param {Element} el
     */ function addFocusVisibleClass(t) {
      if (!t.classList.contains("focus-visible")) {
        t.classList.add("focus-visible");
        t.setAttribute("data-focus-visible-added", "");
      }
    }
    /**
     * Remove the `focus-visible` class from the given element if it was not
     * originally added by the author.
     * @param {Element} el
     */ function removeFocusVisibleClass(t) {
      if (t.hasAttribute("data-focus-visible-added")) {
        t.classList.remove("focus-visible");
        t.removeAttribute("data-focus-visible-added");
      }
    }
    /**
     * If the most recent user interaction was via the keyboard;
     * and the key press did not include a meta, alt/option, or control key;
     * then the modality is keyboard. Otherwise, the modality is not keyboard.
     * Apply `focus-visible` to any current active element and keep track
     * of our keyboard modality state with `hadKeyboardEvent`.
     * @param {KeyboardEvent} e
     */ function onKeyDown(s) {
      if (!(s.metaKey || s.altKey || s.ctrlKey)) {
        isValidFocusTarget(t.activeElement) &&
          addFocusVisibleClass(t.activeElement);
        e = true;
      }
    }
    /**
     * If at any point a user clicks with a pointing device, ensure that we change
     * the modality away from keyboard.
     * This avoids the situation where a user presses a key on an already focused
     * element, and then clicks on a different element, focusing it with a
     * pointing device, while we still think we're in keyboard modality.
     * @param {Event} e
     */ function onPointerDown(t) {
      e = false;
    }
    /**
     * On `focus`, add the `focus-visible` class to the target if:
     * - the target received focus as a result of keyboard navigation, or
     * - the event target is an element that will likely require interaction
     *   via the keyboard (e.g. a text box)
     * @param {Event} e
     */ function onFocus(t) {
      isValidFocusTarget(t.target) &&
        (e || focusTriggersKeyboardModality(t.target)) &&
        addFocusVisibleClass(t.target);
    }
    /**
     * On `blur`, remove the `focus-visible` class from the target.
     * @param {Event} e
     */ function onBlur(t) {
      if (
        isValidFocusTarget(t.target) &&
        (t.target.classList.contains("focus-visible") ||
          t.target.hasAttribute("data-focus-visible-added"))
      ) {
        s = true;
        window.clearTimeout(n);
        n = window.setTimeout(function () {
          s = false;
        }, 100);
        removeFocusVisibleClass(t.target);
      }
    }
    /**
     * If the user changes tabs, keep track of whether or not the previously
     * focused element had .focus-visible.
     * @param {Event} e
     */ function onVisibilityChange(t) {
      if ("hidden" === document.visibilityState) {
        s && (e = true);
        addInitialPointerMoveListeners();
      }
    }
    function addInitialPointerMoveListeners() {
      document.addEventListener("mousemove", onInitialPointerMove);
      document.addEventListener("mousedown", onInitialPointerMove);
      document.addEventListener("mouseup", onInitialPointerMove);
      document.addEventListener("pointermove", onInitialPointerMove);
      document.addEventListener("pointerdown", onInitialPointerMove);
      document.addEventListener("pointerup", onInitialPointerMove);
      document.addEventListener("touchmove", onInitialPointerMove);
      document.addEventListener("touchstart", onInitialPointerMove);
      document.addEventListener("touchend", onInitialPointerMove);
    }
    function removeInitialPointerMoveListeners() {
      document.removeEventListener("mousemove", onInitialPointerMove);
      document.removeEventListener("mousedown", onInitialPointerMove);
      document.removeEventListener("mouseup", onInitialPointerMove);
      document.removeEventListener("pointermove", onInitialPointerMove);
      document.removeEventListener("pointerdown", onInitialPointerMove);
      document.removeEventListener("pointerup", onInitialPointerMove);
      document.removeEventListener("touchmove", onInitialPointerMove);
      document.removeEventListener("touchstart", onInitialPointerMove);
      document.removeEventListener("touchend", onInitialPointerMove);
    }
    /**
     * When the polfyill first loads, assume the user is in keyboard modality.
     * If any event is received from a pointing device (e.g. mouse, pointer,
     * touch), turn off keyboard modality.
     * This accounts for situations where focus enters the page from the URL bar.
     * @param {Event} e
     */ function onInitialPointerMove(t) {
      if (!t.target.nodeName || "html" !== t.target.nodeName.toLowerCase()) {
        e = false;
        removeInitialPointerMoveListeners();
      }
    }
    document.addEventListener("keydown", onKeyDown, true);
    document.addEventListener("mousedown", onPointerDown, true);
    document.addEventListener("pointerdown", onPointerDown, true);
    document.addEventListener("touchstart", onPointerDown, true);
    document.addEventListener("visibilitychange", onVisibilityChange, true);
    addInitialPointerMoveListeners();
    t.addEventListener("focus", onFocus, true);
    t.addEventListener("blur", onBlur, true);
    if (t.nodeType === Node.DOCUMENT_FRAGMENT_NODE && t.host)
      t.host.setAttribute("data-js-focus-visible", "");
    else if (t.nodeType === Node.DOCUMENT_NODE) {
      document.documentElement.classList.add("js-focus-visible");
      document.documentElement.setAttribute("data-js-focus-visible", "");
    }
  }
  if ("undefined" !== typeof window && "undefined" !== typeof document) {
    window.applyFocusVisiblePolyfill = applyFocusVisiblePolyfill;
    var t;
    try {
      t = new CustomEvent("focus-visible-polyfill-ready");
    } catch (e) {
      t = document.createEvent("CustomEvent");
      t.initCustomEvent("focus-visible-polyfill-ready", false, false, {});
    }
    window.dispatchEvent(t);
  }
  "undefined" !== typeof document && applyFocusVisiblePolyfill(document);
});
const e = new Map();
const s = {
  set(t, s, n) {
    e.has(t) || e.set(t, new Map());
    const i = e.get(t);
    i.has(s) || 0 === i.size
      ? i.set(s, n)
      : console.error(
          `Bootstrap doesn't allow more than one instance per element. Bound instance: ${Array.from(i.keys())[0]}.`,
        );
  },
  get(t, s) {
    return (e.has(t) && e.get(t).get(s)) || null;
  },
  remove(t, s) {
    if (!e.has(t)) return;
    const n = e.get(t);
    n.delete(s);
    0 === n.size && e.delete(t);
  },
};
const n = 1e6;
const i = 1e3;
const o = "transitionend";
/**
 * Properly escape IDs selectors to handle weird IDs
 * @param {string} selector
 * @returns {string}
 */ const parseSelector = (t) => {
  t &&
    window.CSS &&
    window.CSS.escape &&
    (t = t.replace(/#([^\s"#']+)/g, (t, e) => `#${CSS.escape(e)}`));
  return t;
};
const toType = (t) =>
  null === t || void 0 === t
    ? `${t}`
    : Object.prototype.toString
        .call(t)
        .match(/\s([a-z]+)/i)[1]
        .toLowerCase();
const getUID = (t) => {
  do {
    t += Math.floor(Math.random() * n);
  } while (document.getElementById(t));
  return t;
};
const getTransitionDurationFromElement = (t) => {
  if (!t) return 0;
  let { transitionDuration: e, transitionDelay: s } =
    window.getComputedStyle(t);
  const n = Number.parseFloat(e);
  const o = Number.parseFloat(s);
  if (!n && !o) return 0;
  e = e.split(",")[0];
  s = s.split(",")[0];
  return (Number.parseFloat(e) + Number.parseFloat(s)) * i;
};
const triggerTransitionEnd = (t) => {
  t.dispatchEvent(new Event(o));
};
const isElement = (t) => {
  if (!t || "object" !== typeof t) return false;
  "undefined" !== typeof t.jquery && (t = t[0]);
  return "undefined" !== typeof t.nodeType;
};
const getElement = (t) =>
  isElement(t)
    ? t.jquery
      ? t[0]
      : t
    : "string" === typeof t && t.length > 0
      ? document.querySelector(parseSelector(t))
      : null;
const isVisible = (t) => {
  if (!isElement(t) || 0 === t.getClientRects().length) return false;
  const e = "visible" === getComputedStyle(t).getPropertyValue("visibility");
  const s = t.closest("details:not([open])");
  if (!s) return e;
  if (s !== t) {
    const e = t.closest("summary");
    if (e && e.parentNode !== s) return false;
    if (null === e) return false;
  }
  return e;
};
const isDisabled = (t) =>
  !t ||
  t.nodeType !== Node.ELEMENT_NODE ||
  !!t.classList.contains("disabled") ||
  ("undefined" !== typeof t.disabled
    ? t.disabled
    : t.hasAttribute("disabled") && "false" !== t.getAttribute("disabled"));
const findShadowRoot = (t) => {
  if (!document.documentElement.attachShadow) return null;
  if ("function" === typeof t.getRootNode) {
    const e = t.getRootNode();
    return e instanceof ShadowRoot ? e : null;
  }
  return t instanceof ShadowRoot
    ? t
    : t.parentNode
      ? findShadowRoot(t.parentNode)
      : null;
};
const noop = () => {};
/**
 * Trick to restart an element's animation
 *
 * @param {HTMLElement} element
 * @return void
 *
 * @see https://www.charistheo.io/blog/2021/02/restart-a-css-animation-with-javascript/#restarting-a-css-animation
 */ const reflow = (t) => {
  t.offsetHeight;
};
const getjQuery = () =>
  window.jQuery && !document.body.hasAttribute("data-bs-no-jquery")
    ? window.jQuery
    : null;
const r = [];
const onDOMContentLoaded = (t) => {
  if ("loading" === document.readyState) {
    r.length ||
      document.addEventListener("DOMContentLoaded", () => {
        for (const t of r) t();
      });
    r.push(t);
  } else t();
};
const isRTL = () => "rtl" === document.documentElement.dir;
const defineJQueryPlugin = (t) => {
  onDOMContentLoaded(() => {
    const e = getjQuery();
    if (e) {
      const s = t.NAME;
      const n = e.fn[s];
      e.fn[s] = t.jQueryInterface;
      e.fn[s].Constructor = t;
      e.fn[s].noConflict = () => {
        e.fn[s] = n;
        return t.jQueryInterface;
      };
    }
  });
};
const execute = (t, e = [], s = t) => ("function" === typeof t ? t(...e) : s);
const executeAfterTransition = (t, e, s = true) => {
  if (!s) {
    execute(t);
    return;
  }
  const n = 5;
  const i = getTransitionDurationFromElement(e) + n;
  let r = false;
  const handler = ({ target: s }) => {
    if (s === e) {
      r = true;
      e.removeEventListener(o, handler);
      execute(t);
    }
  };
  e.addEventListener(o, handler);
  setTimeout(() => {
    r || triggerTransitionEnd(e);
  }, i);
};
/**
 * Return the previous/next element of a list.
 *
 * @param {array} list    The list of elements
 * @param activeElement   The active element
 * @param shouldGetNext   Choose to get next or previous element
 * @param isCycleAllowed
 * @return {Element|elem} The proper element
 */ const getNextActiveElement = (t, e, s, n) => {
  const i = t.length;
  let o = t.indexOf(e);
  if (-1 === o) return !s && n ? t[i - 1] : t[0];
  o += s ? 1 : -1;
  n && (o = (o + i) % i);
  return t[Math.max(0, Math.min(o, i - 1))];
};
const a = /[^.]*(?=\..*)\.|.*/;
const c = /\..*/;
const l = /::\d+$/;
const u = {};
let h = 1;
const d = { mouseenter: "mouseover", mouseleave: "mouseout" };
const f = new Set([
  "click",
  "dblclick",
  "mouseup",
  "mousedown",
  "contextmenu",
  "mousewheel",
  "DOMMouseScroll",
  "mouseover",
  "mouseout",
  "mousemove",
  "selectstart",
  "selectend",
  "keydown",
  "keypress",
  "keyup",
  "orientationchange",
  "touchstart",
  "touchmove",
  "touchend",
  "touchcancel",
  "pointerdown",
  "pointermove",
  "pointerup",
  "pointerleave",
  "pointercancel",
  "gesturestart",
  "gesturechange",
  "gestureend",
  "focus",
  "blur",
  "change",
  "reset",
  "select",
  "submit",
  "focusin",
  "focusout",
  "load",
  "unload",
  "beforeunload",
  "resize",
  "move",
  "DOMContentLoaded",
  "readystatechange",
  "error",
  "abort",
  "scroll",
]);
function makeEventUid(t, e) {
  return (e && `${e}::${h++}`) || t.uidEvent || h++;
}
function getElementEvents(t) {
  const e = makeEventUid(t);
  t.uidEvent = e;
  u[e] = u[e] || {};
  return u[e];
}
function bootstrapHandler(t, e) {
  return function handler(s) {
    hydrateObj(s, { delegateTarget: t });
    handler.oneOff && m.off(t, s.type, e);
    return e.apply(t, [s]);
  };
}
function bootstrapDelegationHandler(t, e, s) {
  return function handler(n) {
    const i = t.querySelectorAll(e);
    for (let { target: o } = n; o && o !== this; o = o.parentNode)
      for (const r of i)
        if (r === o) {
          hydrateObj(n, { delegateTarget: o });
          handler.oneOff && m.off(t, n.type, e, s);
          return s.apply(o, [n]);
        }
  };
}
function findHandler(t, e, s = null) {
  return Object.values(t).find(
    (t) => t.callable === e && t.delegationSelector === s,
  );
}
function normalizeParameters(t, e, s) {
  const n = "string" === typeof e;
  const i = n ? s : e || s;
  let o = getTypeEvent(t);
  f.has(o) || (o = t);
  return [n, i, o];
}
function addHandler(t, e, s, n, i) {
  if ("string" !== typeof e || !t) return;
  let [o, r, c] = normalizeParameters(e, s, n);
  if (e in d) {
    const wrapFunction = (t) =>
      function (e) {
        if (
          !e.relatedTarget ||
          (e.relatedTarget !== e.delegateTarget &&
            !e.delegateTarget.contains(e.relatedTarget))
        )
          return t.call(this, e);
      };
    r = wrapFunction(r);
  }
  const l = getElementEvents(t);
  const u = l[c] || (l[c] = {});
  const h = findHandler(u, r, o ? s : null);
  if (h) {
    h.oneOff = h.oneOff && i;
    return;
  }
  const f = makeEventUid(r, e.replace(a, ""));
  const m = o ? bootstrapDelegationHandler(t, s, r) : bootstrapHandler(t, r);
  m.delegationSelector = o ? s : null;
  m.callable = r;
  m.oneOff = i;
  m.uidEvent = f;
  u[f] = m;
  t.addEventListener(c, m, o);
}
function removeHandler(t, e, s, n, i) {
  const o = findHandler(e[s], n, i);
  if (o) {
    t.removeEventListener(s, o, Boolean(i));
    delete e[s][o.uidEvent];
  }
}
function removeNamespacedHandlers(t, e, s, n) {
  const i = e[s] || {};
  for (const [o, r] of Object.entries(i))
    o.includes(n) && removeHandler(t, e, s, r.callable, r.delegationSelector);
}
function getTypeEvent(t) {
  t = t.replace(c, "");
  return d[t] || t;
}
const m = {
  on(t, e, s, n) {
    addHandler(t, e, s, n, false);
  },
  one(t, e, s, n) {
    addHandler(t, e, s, n, true);
  },
  off(t, e, s, n) {
    if ("string" !== typeof e || !t) return;
    const [i, o, r] = normalizeParameters(e, s, n);
    const a = r !== e;
    const c = getElementEvents(t);
    const u = c[r] || {};
    const h = e.startsWith(".");
    if ("undefined" === typeof o) {
      if (h)
        for (const s of Object.keys(c))
          removeNamespacedHandlers(t, c, s, e.slice(1));
      for (const [s, n] of Object.entries(u)) {
        const i = s.replace(l, "");
        (a && !e.includes(i)) ||
          removeHandler(t, c, r, n.callable, n.delegationSelector);
      }
    } else {
      if (!Object.keys(u).length) return;
      removeHandler(t, c, r, o, i ? s : null);
    }
  },
  trigger(t, e, s) {
    if ("string" !== typeof e || !t) return null;
    const n = getjQuery();
    const i = getTypeEvent(e);
    const o = e !== i;
    let r = null;
    let a = true;
    let c = true;
    let l = false;
    if (o && n) {
      r = n.Event(e, s);
      n(t).trigger(r);
      a = !r.isPropagationStopped();
      c = !r.isImmediatePropagationStopped();
      l = r.isDefaultPrevented();
    }
    const u = hydrateObj(new Event(e, { bubbles: a, cancelable: true }), s);
    l && u.preventDefault();
    c && t.dispatchEvent(u);
    u.defaultPrevented && r && r.preventDefault();
    return u;
  },
};
function hydrateObj(t, e = {}) {
  for (const [s, n] of Object.entries(e))
    try {
      t[s] = n;
    } catch (e) {
      Object.defineProperty(t, s, {
        configurable: true,
        get() {
          return n;
        },
      });
    }
  return t;
}
function normalizeData(t) {
  if ("true" === t) return true;
  if ("false" === t) return false;
  if (t === Number(t).toString()) return Number(t);
  if ("" === t || "null" === t) return null;
  if ("string" !== typeof t) return t;
  try {
    return JSON.parse(decodeURIComponent(t));
  } catch (e) {
    return t;
  }
}
function normalizeDataKey(t) {
  return t.replace(/[A-Z]/g, (t) => `-${t.toLowerCase()}`);
}
const g = {
  setDataAttribute(t, e, s) {
    t.setAttribute(`data-bs-${normalizeDataKey(e)}`, s);
  },
  removeDataAttribute(t, e) {
    t.removeAttribute(`data-bs-${normalizeDataKey(e)}`);
  },
  getDataAttributes(t) {
    if (!t) return {};
    const e = {};
    const s = Object.keys(t.dataset).filter(
      (t) => t.startsWith("bs") && !t.startsWith("bsConfig"),
    );
    for (const n of s) {
      let s = n.replace(/^bs/, "");
      s = s.charAt(0).toLowerCase() + s.slice(1, s.length);
      e[s] = normalizeData(t.dataset[n]);
    }
    return e;
  },
  getDataAttribute(t, e) {
    return normalizeData(t.getAttribute(`data-bs-${normalizeDataKey(e)}`));
  },
};
class Config {
  static get Default() {
    return {};
  }
  static get DefaultType() {
    return {};
  }
  static get NAME() {
    throw new Error(
      'You have to implement the static method "NAME", for each component!',
    );
  }
  _getConfig(t) {
    t = this._mergeConfigObj(t);
    t = this._configAfterMerge(t);
    this._typeCheckConfig(t);
    return t;
  }
  _configAfterMerge(t) {
    return t;
  }
  _mergeConfigObj(t, e) {
    const s = isElement(e) ? g.getDataAttribute(e, "config") : {};
    return {
      ...this.constructor.Default,
      ...("object" === typeof s ? s : {}),
      ...(isElement(e) ? g.getDataAttributes(e) : {}),
      ...("object" === typeof t ? t : {}),
    };
  }
  _typeCheckConfig(t, e = this.constructor.DefaultType) {
    for (const [s, n] of Object.entries(e)) {
      const e = t[s];
      const i = isElement(e) ? "element" : toType(e);
      if (!new RegExp(n).test(i))
        throw new TypeError(
          `${this.constructor.NAME.toUpperCase()}: Option "${s}" provided type "${i}" but expected type "${n}".`,
        );
    }
  }
}
const p = "5.3.2";
class BaseComponent extends Config {
  constructor(t, e) {
    super();
    t = getElement(t);
    if (t) {
      this._element = t;
      this._config = this._getConfig(e);
      s.set(this._element, this.constructor.DATA_KEY, this);
    }
  }
  dispose() {
    s.remove(this._element, this.constructor.DATA_KEY);
    m.off(this._element, this.constructor.EVENT_KEY);
    for (const t of Object.getOwnPropertyNames(this)) this[t] = null;
  }
  _queueCallback(t, e, s = true) {
    executeAfterTransition(t, e, s);
  }
  _getConfig(t) {
    t = this._mergeConfigObj(t, this._element);
    t = this._configAfterMerge(t);
    this._typeCheckConfig(t);
    return t;
  }
  static getInstance(t) {
    return s.get(getElement(t), this.DATA_KEY);
  }
  static getOrCreateInstance(t, e = {}) {
    return this.getInstance(t) || new this(t, "object" === typeof e ? e : null);
  }
  static get VERSION() {
    return p;
  }
  static get DATA_KEY() {
    return `bs.${this.NAME}`;
  }
  static get EVENT_KEY() {
    return `.${this.DATA_KEY}`;
  }
  static eventName(t) {
    return `${t}${this.EVENT_KEY}`;
  }
}
const getSelector = (t) => {
  let e = t.getAttribute("data-bs-target");
  if (!e || "#" === e) {
    let s = t.getAttribute("href");
    if (!s || (!s.includes("#") && !s.startsWith("."))) return null;
    s.includes("#") && !s.startsWith("#") && (s = `#${s.split("#")[1]}`);
    e = s && "#" !== s ? parseSelector(s.trim()) : null;
  }
  return e;
};
const _ = {
  find(t, e = document.documentElement) {
    return [].concat(...Element.prototype.querySelectorAll.call(e, t));
  },
  findOne(t, e = document.documentElement) {
    return Element.prototype.querySelector.call(e, t);
  },
  children(t, e) {
    return [].concat(...t.children).filter((t) => t.matches(e));
  },
  parents(t, e) {
    const s = [];
    let n = t.parentNode.closest(e);
    while (n) {
      s.push(n);
      n = n.parentNode.closest(e);
    }
    return s;
  },
  prev(t, e) {
    let s = t.previousElementSibling;
    while (s) {
      if (s.matches(e)) return [s];
      s = s.previousElementSibling;
    }
    return [];
  },
  next(t, e) {
    let s = t.nextElementSibling;
    while (s) {
      if (s.matches(e)) return [s];
      s = s.nextElementSibling;
    }
    return [];
  },
  focusableChildren(t) {
    const e = [
      "a",
      "button",
      "input",
      "textarea",
      "select",
      "details",
      "[tabindex]",
      '[contenteditable="true"]',
    ]
      .map((t) => `${t}:not([tabindex^="-"])`)
      .join(",");
    return this.find(e, t).filter((t) => !isDisabled(t) && isVisible(t));
  },
  getSelectorFromElement(t) {
    const e = getSelector(t);
    return e && _.findOne(e) ? e : null;
  },
  getElementFromSelector(t) {
    const e = getSelector(t);
    return e ? _.findOne(e) : null;
  },
  getMultipleElementsFromSelector(t) {
    const e = getSelector(t);
    return e ? _.find(e) : [];
  },
};
const enableDismissTrigger = (t, e = "hide") => {
  const s = `click.dismiss${t.EVENT_KEY}`;
  const n = t.NAME;
  m.on(document, s, `[data-bs-dismiss="${n}"]`, function (s) {
    ["A", "AREA"].includes(this.tagName) && s.preventDefault();
    if (isDisabled(this)) return;
    const i = _.getElementFromSelector(this) || this.closest(`.${n}`);
    const o = t.getOrCreateInstance(i);
    o[e]();
  });
};
const b = "alert";
const v = "bs.alert";
const y = `.${v}`;
const w = `close${y}`;
const E = `closed${y}`;
const A = "fade";
const C = "show";
class Alert extends BaseComponent {
  static get NAME() {
    return b;
  }
  close() {
    const t = m.trigger(this._element, w);
    if (t.defaultPrevented) return;
    this._element.classList.remove(C);
    const e = this._element.classList.contains(A);
    this._queueCallback(() => this._destroyElement(), this._element, e);
  }
  _destroyElement() {
    this._element.remove();
    m.trigger(this._element, E);
    this.dispose();
  }
  static jQueryInterface(t) {
    return this.each(function () {
      const e = Alert.getOrCreateInstance(this);
      if ("string" === typeof t) {
        if (void 0 === e[t] || t.startsWith("_") || "constructor" === t)
          throw new TypeError(`No method named "${t}"`);
        e[t](this);
      }
    });
  }
}
enableDismissTrigger(Alert, "close");
defineJQueryPlugin(Alert);
const T = "button";
const L = "bs.button";
const $ = `.${L}`;
const S = ".data-api";
const k = "active";
const I = '[data-bs-toggle="button"]';
const O = `click${$}${S}`;
class Button extends BaseComponent {
  static get NAME() {
    return T;
  }
  toggle() {
    this._element.setAttribute(
      "aria-pressed",
      this._element.classList.toggle(k),
    );
  }
  static jQueryInterface(t) {
    return this.each(function () {
      const e = Button.getOrCreateInstance(this);
      "toggle" === t && e[t]();
    });
  }
}
m.on(document, O, I, (t) => {
  t.preventDefault();
  const e = t.target.closest(I);
  const s = Button.getOrCreateInstance(e);
  s.toggle();
});
defineJQueryPlugin(Button);
const P = "swipe";
const N = ".bs.swipe";
const D = `touchstart${N}`;
const M = `touchmove${N}`;
const x = `touchend${N}`;
const B = `pointerdown${N}`;
const F = `pointerup${N}`;
const j = "touch";
const H = "pen";
const z = "pointer-event";
const q = 40;
const V = { endCallback: null, leftCallback: null, rightCallback: null };
const K = {
  endCallback: "(function|null)",
  leftCallback: "(function|null)",
  rightCallback: "(function|null)",
};
class Swipe extends Config {
  constructor(t, e) {
    super();
    this._element = t;
    if (t && Swipe.isSupported()) {
      this._config = this._getConfig(e);
      this._deltaX = 0;
      this._supportPointerEvents = Boolean(window.PointerEvent);
      this._initEvents();
    }
  }
  static get Default() {
    return V;
  }
  static get DefaultType() {
    return K;
  }
  static get NAME() {
    return P;
  }
  dispose() {
    m.off(this._element, N);
  }
  _start(t) {
    this._supportPointerEvents
      ? this._eventIsPointerPenTouch(t) && (this._deltaX = t.clientX)
      : (this._deltaX = t.touches[0].clientX);
  }
  _end(t) {
    this._eventIsPointerPenTouch(t) &&
      (this._deltaX = t.clientX - this._deltaX);
    this._handleSwipe();
    execute(this._config.endCallback);
  }
  _move(t) {
    this._deltaX =
      t.touches && t.touches.length > 1
        ? 0
        : t.touches[0].clientX - this._deltaX;
  }
  _handleSwipe() {
    const t = Math.abs(this._deltaX);
    if (t <= q) return;
    const e = t / this._deltaX;
    this._deltaX = 0;
    e &&
      execute(e > 0 ? this._config.rightCallback : this._config.leftCallback);
  }
  _initEvents() {
    if (this._supportPointerEvents) {
      m.on(this._element, B, (t) => this._start(t));
      m.on(this._element, F, (t) => this._end(t));
      this._element.classList.add(z);
    } else {
      m.on(this._element, D, (t) => this._start(t));
      m.on(this._element, M, (t) => this._move(t));
      m.on(this._element, x, (t) => this._end(t));
    }
  }
  _eventIsPointerPenTouch(t) {
    return (
      this._supportPointerEvents && (t.pointerType === H || t.pointerType === j)
    );
  }
  static isSupported() {
    return (
      "ontouchstart" in document.documentElement || navigator.maxTouchPoints > 0
    );
  }
}
const W = "carousel";
const Q = "bs.carousel";
const R = `.${Q}`;
const U = ".data-api";
const Y = "ArrowLeft";
const X = "ArrowRight";
const G = 500;
const J = "next";
const Z = "prev";
const tt = "left";
const et = "right";
const st = `slide${R}`;
const nt = `slid${R}`;
const it = `keydown${R}`;
const ot = `mouseenter${R}`;
const rt = `mouseleave${R}`;
const at = `dragstart${R}`;
const ct = `load${R}${U}`;
const lt = `click${R}${U}`;
const ut = "carousel";
const ht = "active";
const dt = "slide";
const ft = "carousel-item-end";
const mt = "carousel-item-start";
const gt = "carousel-item-next";
const pt = "carousel-item-prev";
const _t = "is-paused";
const bt = "is-done";
const vt = ".active";
const yt = ".carousel-item";
const wt = vt + yt;
const Et = ".carousel-item img";
const At = ".carousel-indicators";
const Ct = "[data-bs-slide], [data-bs-slide-to]";
const Tt = '[data-bs-ride="carousel"]';
const Lt = ".carousel-control-prev";
const $t = ".carousel-control-next";
const St = ".carousel-control-play-pause";
const kt = "data-bs-target";
const It = "data-bs-play-text";
const Ot = "data-bs-pause-text";
const Pt = "Play Carousel";
const Nt = "Pause Carousel";
const Dt = "bs-";
const Mt = { [Y]: et, [X]: tt };
const xt = {
  interval: 5e3,
  keyboard: true,
  pause: "hover",
  ride: false,
  touch: true,
  wrap: true,
};
const Bt = {
  interval: "(number|boolean)",
  keyboard: "boolean",
  pause: "(string|boolean)",
  ride: "(boolean|string)",
  touch: "boolean",
  wrap: "boolean",
};
class Carousel extends BaseComponent {
  constructor(t, e) {
    super(t, e);
    this._interval = null;
    this._activeElement = null;
    this._isSliding = false;
    this.touchTimeout = null;
    this._swipeHelper = null;
    this._indicatorsElement = _.findOne(At, this._element);
    this._playPauseButton = _.findOne(`${St}[${kt}="#${this._element.id}"]`);
    this._addEventListeners();
    this._config.ride === ut
      ? this.cycle()
      : this._indicatorsElement && this._element.classList.add(_t);
  }
  static get Default() {
    return xt;
  }
  static get DefaultType() {
    return Bt;
  }
  static get NAME() {
    return W;
  }
  next() {
    this._slide(J);
  }
  nextWhenVisible() {
    !document.hidden && isVisible(this._element) && this.next();
  }
  prev() {
    this._slide(Z);
  }
  pause() {
    this._indicatorsElement && this._element.classList.add(_t);
    if (
      null !== this._playPauseButton &&
      this._playPauseButton.classList.contains("pause")
    ) {
      this._playPauseButton.classList.remove("pause");
      this._playPauseButton.classList.add("play");
      if (this._playPauseButton.getAttribute(It)) {
        this._playPauseButton.setAttribute(
          "title",
          this._playPauseButton.getAttribute(It),
        );
        this._playPauseButton.querySelector("span.visually-hidden").innerHTML =
          this._playPauseButton.getAttribute(It);
      } else {
        this._playPauseButton.setAttribute("title", Pt);
        this._playPauseButton.querySelector("span.visually-hidden").innerHTML =
          Pt;
      }
      this._stayPaused = true;
    }
    this._isSliding && triggerTransitionEnd(this._element);
    this._clearInterval();
  }
  cycle() {
    this._indicatorsElement && this._element.classList.remove(_t);
    if (
      null !== this._playPauseButton &&
      this._playPauseButton.classList.contains("play")
    ) {
      this._playPauseButton.classList.remove("play");
      this._playPauseButton.classList.add("pause");
      if (this._playPauseButton.getAttribute(Ot)) {
        this._playPauseButton.setAttribute(
          "title",
          this._playPauseButton.getAttribute(Ot),
        );
        this._playPauseButton.querySelector("span.visually-hidden").innerHTML =
          this._playPauseButton.getAttribute(Ot);
      } else {
        this._playPauseButton.setAttribute("title", Nt);
        this._playPauseButton.querySelector("span.visually-hidden").innerHTML =
          Nt;
      }
      this._stayPaused = false;
    }
    this._clearInterval();
    this._updateInterval();
    this._interval = setInterval(
      () => this.nextWhenVisible(),
      this._config.interval,
    );
  }
  _maybeEnableCycle() {
    this._config.ride &&
      (this._isSliding
        ? m.one(this._element, nt, () => this.cycle())
        : this.cycle());
  }
  to(t) {
    this._indicatorsElement && this._element.classList.remove(bt);
    const e = this._getItems();
    if (t > e.length - 1 || t < 0) return;
    if (this._isSliding) {
      m.one(this._element, nt, () => this.to(t));
      return;
    }
    const s = this._getItemIndex(this._getActive());
    if (s === t) return;
    const n = t > s ? J : Z;
    this._slide(n, e[t]);
  }
  dispose() {
    this._swipeHelper && this._swipeHelper.dispose();
    super.dispose();
  }
  _configAfterMerge(t) {
    t.defaultInterval = t.interval;
    return t;
  }
  _addEventListeners() {
    this._config.keyboard && m.on(this._element, it, (t) => this._keydown(t));
    if ("hover" === this._config.pause) {
      m.on(this._element, ot, () => this.pause());
      m.on(this._element, rt, () => this._maybeEnableCycle());
    }
    this._config.touch && Swipe.isSupported() && this._addTouchEventListeners();
  }
  _addTouchEventListeners() {
    for (const t of _.find(Et, this._element))
      m.on(t, at, (t) => t.preventDefault());
    const endCallBack = () => {
      if ("hover" === this._config.pause) {
        this.pause();
        this.touchTimeout && clearTimeout(this.touchTimeout);
        this.touchTimeout = setTimeout(
          () => this._maybeEnableCycle(),
          G + this._config.interval,
        );
      }
    };
    const t = {
      leftCallback: () => this._slide(this._directionToOrder(tt)),
      rightCallback: () => this._slide(this._directionToOrder(et)),
      endCallback: endCallBack,
    };
    this._swipeHelper = new Swipe(this._element, t);
  }
  _keydown(t) {
    if (/input|textarea/i.test(t.target.tagName)) return;
    const e = Mt[t.key];
    if (e) {
      t.preventDefault();
      this._slide(this._directionToOrder(e));
    }
  }
  _disableControl(t) {
    if ("BUTTON" === t.nodeName) t.disabled = true;
    else {
      t.setAttribute("aria-disabled", true);
      t.setAttribute("tabindex", "-1");
    }
  }
  _enableControl(t) {
    if ("BUTTON" === t.nodeName) t.disabled = false;
    else {
      t.removeAttribute("aria-disabled");
      t.removeAttribute("tabindex");
    }
  }
  _getItemIndex(t) {
    return this._getItems().indexOf(t);
  }
  _setActiveIndicatorElement(t) {
    if (!this._indicatorsElement) return;
    const e = _.findOne(vt, this._indicatorsElement);
    e.classList.remove(ht);
    e.removeAttribute("aria-current");
    const s = _.findOne(`[data-bs-slide-to="${t}"]`, this._indicatorsElement);
    if (s) {
      s.classList.add(ht);
      s.setAttribute("aria-current", "true");
    }
  }
  _updateInterval() {
    const t = this._activeElement || this._getActive();
    if (!t) return;
    const e = Number.parseInt(t.getAttribute("data-bs-interval"), 10);
    this._config.interval = e || this._config.defaultInterval;
    if (this._indicatorsElement && this._config.interval !== xt.interval) {
      const e = this._getItemIndex(t);
      const s = _.findOne(`:nth-child(${e + 1})`, this._indicatorsElement);
      s.style.setProperty(
        `--${Dt}carousel-interval`,
        `${this._config.interval}ms`,
      );
    }
  }
  _slide(t, e = null) {
    if (this._isSliding) return;
    const s = this._getActive();
    const n = t === J;
    if (!this._config.wrap) {
      const e = t === Z;
      const i = this._getItemIndex(s);
      const o = this._getItems().length - 1;
      const r = (e && 0 === i) || (n && i === o);
      if (r) {
        n &&
          this._indicatorsElement &&
          !this._element.hasAttribute("data-bs-slide") &&
          this._element.classList.add(bt);
        return s;
      }
      this._indicatorsElement && this._element.classList.remove(bt);
    }
    const i =
      e || getNextActiveElement(this._getItems(), s, n, this._config.wrap);
    if (i === s) return;
    const o = this._getItemIndex(i);
    const triggerEvent = (e) =>
      m.trigger(this._element, e, {
        relatedTarget: i,
        direction: this._orderToDirection(t),
        from: this._getItemIndex(s),
        to: o,
      });
    const r = triggerEvent(st);
    if (r.defaultPrevented) return;
    if (!s || !i) return;
    const a = Boolean(this._interval);
    this.pause();
    this._isSliding = true;
    this._setActiveIndicatorElement(o);
    this._activeElement = i;
    if (!this._config.wrap) {
      const t = _.findOne(Lt, this._element);
      const e = _.findOne($t, this._element);
      this._enableControl(t);
      this._enableControl(e);
      0 === o
        ? this._disableControl(t)
        : o === this._getItems().length - 1 && this._disableControl(e);
    }
    const c = n ? mt : ft;
    const l = n ? gt : pt;
    i.classList.add(l);
    reflow(i);
    s.classList.add(c);
    i.classList.add(c);
    const completeCallBack = () => {
      i.classList.remove(c, l);
      i.classList.add(ht);
      s.classList.remove(ht, l, c);
      this._isSliding = false;
      triggerEvent(nt);
    };
    this._queueCallback(completeCallBack, s, this._isAnimated());
    a && this.cycle();
  }
  _isAnimated() {
    return this._element.classList.contains(dt);
  }
  _getActive() {
    return _.findOne(wt, this._element);
  }
  _getItems() {
    return _.find(yt, this._element);
  }
  _clearInterval() {
    if (this._interval) {
      clearInterval(this._interval);
      this._interval = null;
    }
  }
  _directionToOrder(t) {
    return isRTL() ? (t === tt ? Z : J) : t === tt ? J : Z;
  }
  _orderToDirection(t) {
    return isRTL() ? (t === Z ? tt : et) : t === Z ? et : tt;
  }
  static PauseCarousel(t) {
    const e = t.target;
    const s = e.getAttribute(kt);
    const n = Carousel.getOrCreateInstance(document.querySelector(s));
    e.classList.contains("pause") ? n.pause() : n.cycle();
  }
  static jQueryInterface(t) {
    return this.each(function () {
      const e = Carousel.getOrCreateInstance(this, t);
      if ("number" !== typeof t) {
        if ("string" === typeof t) {
          if (void 0 === e[t] || t.startsWith("_") || "constructor" === t)
            throw new TypeError(`No method named "${t}"`);
          e[t]();
        }
      } else e.to(t);
    });
  }
}
m.on(document, lt, Ct, function (t) {
  const e = _.getElementFromSelector(this);
  if (!e || !e.classList.contains(ut)) return;
  t.preventDefault();
  const s = Carousel.getOrCreateInstance(e);
  const n = this.getAttribute("data-bs-slide-to");
  if (n) {
    s.to(n);
    s._maybeEnableCycle();
  } else if ("next" !== g.getDataAttribute(this, "slide")) {
    s.prev();
    s._maybeEnableCycle();
  } else {
    s.next();
    s._maybeEnableCycle();
  }
});
m.on(document, lt, St, Carousel.PauseCarousel);
m.on(window, ct, () => {
  const t = _.find(Tt);
  for (const e of t) Carousel.getOrCreateInstance(e);
});
defineJQueryPlugin(Carousel);
const Ft = "collapse";
const jt = "bs.collapse";
const Ht = `.${jt}`;
const zt = ".data-api";
const qt = `show${Ht}`;
const Vt = `shown${Ht}`;
const Kt = `hide${Ht}`;
const Wt = `hidden${Ht}`;
const Qt = `click${Ht}${zt}`;
const Rt = "show";
const Ut = "collapse";
const Yt = "collapsing";
const Xt = "collapsed";
const Gt = `:scope .${Ut} .${Ut}`;
const Jt = "collapse-horizontal";
const Zt = "width";
const te = "height";
const ee = ".collapse.show, .collapse.collapsing";
const se = '[data-bs-toggle="collapse"]';
const ne = { parent: null, toggle: true };
const ie = { parent: "(null|element)", toggle: "boolean" };
class Collapse extends BaseComponent {
  constructor(t, e) {
    super(t, e);
    this._isTransitioning = false;
    this._triggerArray = [];
    const s = _.find(se);
    for (const t of s) {
      const e = _.getSelectorFromElement(t);
      const s = _.find(e).filter((t) => t === this._element);
      null !== e && s.length && this._triggerArray.push(t);
    }
    this._initializeChildren();
    this._config.parent ||
      this._addAriaAndCollapsedClass(this._triggerArray, this._isShown());
    this._config.toggle && this.toggle();
  }
  static get Default() {
    return ne;
  }
  static get DefaultType() {
    return ie;
  }
  static get NAME() {
    return Ft;
  }
  toggle() {
    this._isShown() ? this.hide() : this.show();
  }
  show() {
    if (this._isTransitioning || this._isShown()) return;
    let t = [];
    this._config.parent &&
      (t = this._getFirstLevelChildren(ee)
        .filter((t) => t !== this._element)
        .map((t) => Collapse.getOrCreateInstance(t, { toggle: false })));
    if (t.length && t[0]._isTransitioning) return;
    const e = m.trigger(this._element, qt);
    if (e.defaultPrevented) return;
    for (const e of t) e.hide();
    const s = this._getDimension();
    this._element.classList.remove(Ut);
    this._element.classList.add(Yt);
    this._element.style[s] = 0;
    this._addAriaAndCollapsedClass(this._triggerArray, true);
    this._isTransitioning = true;
    const complete = () => {
      this._isTransitioning = false;
      this._element.classList.remove(Yt);
      this._element.classList.add(Ut, Rt);
      this._element.style[s] = "";
      m.trigger(this._element, Vt);
    };
    const n = s[0].toUpperCase() + s.slice(1);
    const i = `scroll${n}`;
    this._queueCallback(complete, this._element, true);
    this._element.style[s] = `${this._element[i]}px`;
  }
  hide() {
    if (this._isTransitioning || !this._isShown()) return;
    const t = m.trigger(this._element, Kt);
    if (t.defaultPrevented) return;
    const e = this._getDimension();
    this._element.style[e] = `${this._element.getBoundingClientRect()[e]}px`;
    reflow(this._element);
    this._element.classList.add(Yt);
    this._element.classList.remove(Ut, Rt);
    this._isTransitioning = true;
    const complete = () => {
      this._isTransitioning = false;
      this._element.classList.remove(Yt);
      this._element.classList.add(Ut);
      for (const t of this._triggerArray) {
        const e = _.getElementFromSelector(t);
        e && !this._isShown(e) && this._addAriaAndCollapsedClass([t], false);
      }
      m.trigger(this._element, Wt);
    };
    this._element.style[e] = "";
    this._queueCallback(complete, this._element, true);
  }
  _isShown(t = this._element) {
    return t.classList.contains(Rt);
  }
  _configAfterMerge(t) {
    t.toggle = Boolean(t.toggle);
    t.parent = getElement(t.parent);
    return t;
  }
  _getDimension() {
    return this._element.classList.contains(Jt) ? Zt : te;
  }
  _initializeChildren() {
    if (!this._config.parent) return;
    const t = this._getFirstLevelChildren(se);
    for (const e of t) {
      const t = _.getElementFromSelector(e);
      t && this._addAriaAndCollapsedClass([e], this._isShown(t));
    }
  }
  _getFirstLevelChildren(t) {
    const e = _.find(Gt, this._config.parent);
    return _.find(t, this._config.parent).filter((t) => !e.includes(t));
  }
  _addAriaAndCollapsedClass(t, e) {
    if (t.length)
      for (const s of t) {
        s.classList.toggle(Xt, !e);
        s.setAttribute("aria-expanded", e);
      }
  }
  static jQueryInterface(t) {
    const e = {};
    "string" === typeof t && /show|hide/.test(t) && (e.toggle = false);
    return this.each(function () {
      const s = Collapse.getOrCreateInstance(this, e);
      if ("string" === typeof t) {
        if ("undefined" === typeof s[t])
          throw new TypeError(`No method named "${t}"`);
        s[t]();
      }
    });
  }
}
m.on(document, Qt, se, function (t) {
  ("A" === t.target.tagName ||
    (t.delegateTarget && "A" === t.delegateTarget.tagName)) &&
    t.preventDefault();
  for (const t of _.getMultipleElementsFromSelector(this))
    Collapse.getOrCreateInstance(t, { toggle: false }).toggle();
});
defineJQueryPlugin(Collapse);
const oe = "dropdown";
const re = "bs.dropdown";
const ae = `.${re}`;
const ce = ".data-api";
const le = "Escape";
const ue = "Tab";
const he = "ArrowUp";
const de = "ArrowDown";
const fe = 2;
const me = `hide${ae}`;
const ge = `hidden${ae}`;
const pe = `show${ae}`;
const _e = `shown${ae}`;
const be = `click${ae}${ce}`;
const ve = `keydown${ae}${ce}`;
const ye = `keyup${ae}${ce}`;
const we = "show";
const Ee = "dropup";
const Ae = "dropend";
const Ce = "dropstart";
const Te = "dropup-center";
const Le = "dropdown-center";
const $e = '[data-bs-toggle="dropdown"]:not(.disabled):not(:disabled)';
const Se = `${$e}.${we}`;
const ke = ".dropdown-menu";
const Ie = ".navbar";
const Oe = ".navbar-nav";
const Pe = ".dropdown-menu .dropdown-item:not(.disabled):not(:disabled)";
const Ne = isRTL() ? "top-end" : "top-start";
const De = isRTL() ? "top-start" : "top-end";
const Me = isRTL() ? "bottom-end" : "bottom-start";
const xe = isRTL() ? "bottom-start" : "bottom-end";
const Be = isRTL() ? "left-start" : "right-start";
const Fe = isRTL() ? "right-start" : "left-start";
const je = "top";
const He = "bottom";
const ze = {
  autoClose: true,
  boundary: "clippingParents",
  display: "dynamic",
  offset: [0, 0],
  popperConfig: null,
  reference: "toggle",
};
const qe = {
  autoClose: "(boolean|string)",
  boundary: "(string|element)",
  display: "string",
  offset: "(array|string|function)",
  popperConfig: "(null|object|function)",
  reference: "(string|element|object)",
};
class Dropdown extends BaseComponent {
  constructor(t, e) {
    super(t, e);
    this._popper = null;
    this._parent = this._element.parentNode;
    this._menu =
      _.next(this._element, ke)[0] ||
      _.prev(this._element, ke)[0] ||
      _.findOne(ke, this._parent);
    this._inNavbar = this._detectNavbar();
  }
  static get Default() {
    return ze;
  }
  static get DefaultType() {
    return qe;
  }
  static get NAME() {
    return oe;
  }
  toggle() {
    return this._isShown() ? this.hide() : this.show();
  }
  show() {
    if (isDisabled(this._element) || this._isShown()) return;
    const t = { relatedTarget: this._element };
    const e = m.trigger(this._element, pe, t);
    if (!e.defaultPrevented) {
      this._createPopper();
      if (
        "ontouchstart" in document.documentElement &&
        !this._parent.closest(Oe)
      )
        for (const t of [].concat(...document.body.children))
          m.on(t, "mouseover", noop);
      this._element.focus();
      this._element.setAttribute("aria-expanded", true);
      this._menu.classList.add(we);
      this._element.classList.add(we);
      m.trigger(this._element, _e, t);
    }
  }
  hide() {
    if (isDisabled(this._element) || !this._isShown()) return;
    const t = { relatedTarget: this._element };
    this._completeHide(t);
  }
  dispose() {
    this._popper && this._popper.destroy();
    super.dispose();
  }
  update() {
    this._inNavbar = this._detectNavbar();
    this._popper && this._popper.update();
  }
  _completeHide(t) {
    const e = m.trigger(this._element, me, t);
    if (!e.defaultPrevented) {
      if ("ontouchstart" in document.documentElement)
        for (const t of [].concat(...document.body.children))
          m.off(t, "mouseover", noop);
      this._popper && this._popper.destroy();
      this._menu.classList.remove(we);
      this._element.classList.remove(we);
      this._element.setAttribute("aria-expanded", "false");
      g.removeDataAttribute(this._menu, "popper");
      m.trigger(this._element, ge, t);
    }
  }
  _getConfig(t) {
    t = super._getConfig(t);
    if (
      "object" === typeof t.reference &&
      !isElement(t.reference) &&
      "function" !== typeof t.reference.getBoundingClientRect
    )
      throw new TypeError(
        `${oe.toUpperCase()}: Option "reference" provided type "object" without a required "getBoundingClientRect" method.`,
      );
    return t;
  }
  _createPopper() {
    if ("undefined" === typeof t)
      throw new TypeError(
        "Bootstrap's dropdowns require Popper (https://popper.js.org)",
      );
    let e = this._element;
    "parent" === this._config.reference
      ? (e = this._parent)
      : isElement(this._config.reference)
        ? (e = getElement(this._config.reference))
        : "object" === typeof this._config.reference &&
          (e = this._config.reference);
    const s = this._getPopperConfig();
    this._popper = t.createPopper(e, this._menu, s);
  }
  _isShown() {
    return this._menu.classList.contains(we);
  }
  _getPlacement() {
    const t = this._parent;
    if (t.classList.contains(Ae)) return Be;
    if (t.classList.contains(Ce)) return Fe;
    if (t.classList.contains(Te)) return je;
    if (t.classList.contains(Le)) return He;
    const e =
      "end" ===
      getComputedStyle(this._menu).getPropertyValue("--bs-position").trim();
    return t.classList.contains(Ee) ? (e ? De : Ne) : e ? xe : Me;
  }
  _detectNavbar() {
    return null !== this._element.closest(Ie);
  }
  _getOffset() {
    const { offset: t } = this._config;
    return "string" === typeof t
      ? t.split(",").map((t) => Number.parseInt(t, 10))
      : "function" === typeof t
        ? (e) => t(e, this._element)
        : t;
  }
  _getPopperConfig() {
    const t = {
      placement: this._getPlacement(),
      modifiers: [
        {
          name: "preventOverflow",
          options: { boundary: this._config.boundary },
        },
        { name: "offset", options: { offset: this._getOffset() } },
      ],
    };
    if (this._inNavbar || "static" === this._config.display) {
      g.setDataAttribute(this._menu, "popper", "static");
      t.modifiers = [{ name: "applyStyles", enabled: false }];
    }
    return { ...t, ...execute(this._config.popperConfig, [t]) };
  }
  _selectMenuItem({ key: t, target: e }) {
    const s = _.find(Pe, this._menu).filter((t) => isVisible(t));
    s.length && getNextActiveElement(s, e, t === de, !s.includes(e)).focus();
  }
  static jQueryInterface(t) {
    return this.each(function () {
      const e = Dropdown.getOrCreateInstance(this, t);
      if ("string" === typeof t) {
        if ("undefined" === typeof e[t])
          throw new TypeError(`No method named "${t}"`);
        e[t]();
      }
    });
  }
  static clearMenus(t) {
    if (t.button === fe || ("keyup" === t.type && t.key !== ue)) return;
    const e = _.find(Se);
    for (const s of e) {
      const e = Dropdown.getInstance(s);
      if (!e || false === e._config.autoClose) continue;
      const n = t.composedPath();
      const i = n.includes(e._menu);
      if (
        n.includes(e._element) ||
        ("inside" === e._config.autoClose && !i) ||
        ("outside" === e._config.autoClose && i)
      )
        continue;
      if (
        e._menu.contains(t.target) &&
        (("keyup" === t.type && t.key === ue) ||
          /input|select|option|textarea|form/i.test(t.target.tagName))
      )
        continue;
      const o = { relatedTarget: e._element };
      "click" === t.type && (o.clickEvent = t);
      e._completeHide(o);
    }
  }
  static dataApiKeydownHandler(t) {
    const e = /input|textarea/i.test(t.target.tagName);
    const s = t.key === le;
    const n = [he, de].includes(t.key);
    if (!n && !s) return;
    if (e && !s) return;
    t.preventDefault();
    const i = this.matches($e)
      ? this
      : _.prev(this, $e)[0] ||
        _.next(this, $e)[0] ||
        _.findOne($e, t.delegateTarget.parentNode);
    const o = Dropdown.getOrCreateInstance(i);
    if (n) {
      t.stopPropagation();
      o.show();
      o._selectMenuItem(t);
    } else if (o._isShown()) {
      t.stopPropagation();
      o.hide();
      i.focus();
    }
  }
}
m.on(document, ve, $e, Dropdown.dataApiKeydownHandler);
m.on(document, ve, ke, Dropdown.dataApiKeydownHandler);
m.on(document, be, Dropdown.clearMenus);
m.on(document, ye, Dropdown.clearMenus);
m.on(document, be, $e, function (t) {
  t.preventDefault();
  Dropdown.getOrCreateInstance(this).toggle();
});
defineJQueryPlugin(Dropdown);
const Ve = "backdrop";
const Ke = "fade";
const We = "show";
const Qe = `mousedown.bs.${Ve}`;
const Re = {
  className: "modal-backdrop",
  clickCallback: null,
  isAnimated: false,
  isVisible: true,
  rootElement: "body",
};
const Ue = {
  className: "string",
  clickCallback: "(function|null)",
  isAnimated: "boolean",
  isVisible: "boolean",
  rootElement: "(element|string)",
};
class Backdrop extends Config {
  constructor(t) {
    super();
    this._config = this._getConfig(t);
    this._isAppended = false;
    this._element = null;
  }
  static get Default() {
    return Re;
  }
  static get DefaultType() {
    return Ue;
  }
  static get NAME() {
    return Ve;
  }
  show(t) {
    if (!this._config.isVisible) {
      execute(t);
      return;
    }
    this._append();
    const e = this._getElement();
    this._config.isAnimated && reflow(e);
    e.classList.add(We);
    this._emulateAnimation(() => {
      execute(t);
    });
  }
  hide(t) {
    if (this._config.isVisible) {
      this._getElement().classList.remove(We);
      this._emulateAnimation(() => {
        this.dispose();
        execute(t);
      });
    } else execute(t);
  }
  dispose() {
    if (this._isAppended) {
      m.off(this._element, Qe);
      this._element.remove();
      this._isAppended = false;
    }
  }
  _getElement() {
    if (!this._element) {
      const t = document.createElement("div");
      t.className = this._config.className;
      this._config.isAnimated && t.classList.add(Ke);
      this._element = t;
    }
    return this._element;
  }
  _configAfterMerge(t) {
    t.rootElement = getElement(t.rootElement);
    return t;
  }
  _append() {
    if (this._isAppended) return;
    const t = this._getElement();
    this._config.rootElement.append(t);
    m.on(t, Qe, () => {
      execute(this._config.clickCallback);
    });
    this._isAppended = true;
  }
  _emulateAnimation(t) {
    executeAfterTransition(t, this._getElement(), this._config.isAnimated);
  }
}
const Ye = "focustrap";
const Xe = "bs.focustrap";
const Ge = `.${Xe}`;
const Je = `focusin${Ge}`;
const Ze = `keydown.tab${Ge}`;
const ts = "Tab";
const es = "forward";
const ss = "backward";
const ns = { autofocus: true, trapElement: null };
const is = { autofocus: "boolean", trapElement: "element" };
class FocusTrap extends Config {
  constructor(t) {
    super();
    this._config = this._getConfig(t);
    this._isActive = false;
    this._lastTabNavDirection = null;
  }
  static get Default() {
    return ns;
  }
  static get DefaultType() {
    return is;
  }
  static get NAME() {
    return Ye;
  }
  activate() {
    if (!this._isActive) {
      this._config.autofocus && this._config.trapElement.focus();
      m.off(document, Ge);
      m.on(document, Je, (t) => this._handleFocusin(t));
      m.on(document, Ze, (t) => this._handleKeydown(t));
      this._isActive = true;
    }
  }
  deactivate() {
    if (this._isActive) {
      this._isActive = false;
      m.off(document, Ge);
    }
  }
  _handleFocusin(t) {
    const { trapElement: e } = this._config;
    if (t.target === document || t.target === e || e.contains(t.target)) return;
    const s = _.focusableChildren(e);
    0 === s.length
      ? e.focus()
      : this._lastTabNavDirection === ss
        ? s[s.length - 1].focus()
        : s[0].focus();
  }
  _handleKeydown(t) {
    t.key === ts && (this._lastTabNavDirection = t.shiftKey ? ss : es);
  }
}
const os = ".fixed-top, .fixed-bottom, .is-fixed, .sticky-top";
const rs = ".sticky-top";
const as = "padding-right";
const cs = "margin-right";
class ScrollBarHelper {
  constructor() {
    this._element = document.body;
  }
  getWidth() {
    const t = document.documentElement.clientWidth;
    return Math.abs(window.innerWidth - t);
  }
  hide() {
    const t = this.getWidth();
    this._disableOverFlow();
    this._setElementAttributes(this._element, as, (e) => e + t);
    this._setElementAttributes(os, as, (e) => e + t);
    this._setElementAttributes(rs, cs, (e) => e - t);
  }
  reset() {
    this._resetElementAttributes(this._element, "overflow");
    this._resetElementAttributes(this._element, as);
    this._resetElementAttributes(os, as);
    this._resetElementAttributes(rs, cs);
  }
  isOverflowing() {
    return this.getWidth() > 0;
  }
  _disableOverFlow() {
    this._saveInitialAttribute(this._element, "overflow");
    this._element.style.overflow = "hidden";
  }
  _setElementAttributes(t, e, s) {
    const n = this.getWidth();
    const manipulationCallBack = (t) => {
      if (t !== this._element && window.innerWidth > t.clientWidth + n) return;
      this._saveInitialAttribute(t, e);
      const i = window.getComputedStyle(t).getPropertyValue(e);
      t.style.setProperty(e, `${s(Number.parseFloat(i))}px`);
    };
    this._applyManipulationCallback(t, manipulationCallBack);
  }
  _saveInitialAttribute(t, e) {
    const s = t.style.getPropertyValue(e);
    s && g.setDataAttribute(t, e, s);
  }
  _resetElementAttributes(t, e) {
    const manipulationCallBack = (t) => {
      const s = g.getDataAttribute(t, e);
      if (null !== s) {
        g.removeDataAttribute(t, e);
        t.style.setProperty(e, s);
      } else t.style.removeProperty(e);
    };
    this._applyManipulationCallback(t, manipulationCallBack);
  }
  _applyManipulationCallback(t, e) {
    if (isElement(t)) e(t);
    else for (const s of _.find(t, this._element)) e(s);
  }
}
const ls = "modal";
const us = "bs.modal";
const hs = `.${us}`;
const ds = ".data-api";
const fs = "Escape";
const ms = `hide${hs}`;
const gs = `hidePrevented${hs}`;
const ps = `hidden${hs}`;
const _s = `show${hs}`;
const bs = `shown${hs}`;
const vs = `resize${hs}`;
const ys = `click.dismiss${hs}`;
const ws = `mousedown.dismiss${hs}`;
const Es = `keydown.dismiss${hs}`;
const As = `click${hs}${ds}`;
const Cs = "modal-open";
const Ts = "fade";
const Ls = "show";
const $s = "modal-static";
const Ss = ".modal.show";
const ks = ".modal-dialog";
const Is = ".modal-body";
const Os = '[data-bs-toggle="modal"]';
const Ps = { backdrop: true, focus: true, keyboard: true };
const Ns = {
  backdrop: "(boolean|string)",
  focus: "boolean",
  keyboard: "boolean",
};
class Modal extends BaseComponent {
  constructor(t, e) {
    super(t, e);
    this._dialog = _.findOne(ks, this._element);
    this._backdrop = this._initializeBackDrop();
    this._focustrap = this._initializeFocusTrap();
    this._isShown = false;
    this._isTransitioning = false;
    this._scrollBar = new ScrollBarHelper();
    this._addEventListeners();
  }
  static get Default() {
    return Ps;
  }
  static get DefaultType() {
    return Ns;
  }
  static get NAME() {
    return ls;
  }
  toggle(t) {
    return this._isShown ? this.hide() : this.show(t);
  }
  show(t) {
    if (this._isShown || this._isTransitioning) return;
    const e = m.trigger(this._element, _s, { relatedTarget: t });
    if (!e.defaultPrevented) {
      this._isShown = true;
      this._isTransitioning = true;
      this._scrollBar.hide();
      document.body.classList.add(Cs);
      this._adjustDialog();
      this._backdrop.show(() => this._showElement(t));
    }
  }
  hide() {
    if (!this._isShown || this._isTransitioning) return;
    const t = m.trigger(this._element, ms);
    if (!t.defaultPrevented) {
      this._isShown = false;
      this._isTransitioning = true;
      this._focustrap.deactivate();
      this._element.classList.remove(Ls);
      this._queueCallback(
        () => this._hideModal(),
        this._element,
        this._isAnimated(),
      );
    }
  }
  dispose() {
    m.off(window, hs);
    m.off(this._dialog, hs);
    this._backdrop.dispose();
    this._focustrap.deactivate();
    super.dispose();
  }
  handleUpdate() {
    this._adjustDialog();
  }
  _initializeBackDrop() {
    return new Backdrop({
      isVisible: Boolean(this._config.backdrop),
      isAnimated: this._isAnimated(),
    });
  }
  _initializeFocusTrap() {
    return new FocusTrap({ trapElement: this._element });
  }
  _showElement(t) {
    document.body.contains(this._element) ||
      document.body.append(this._element);
    this._element.style.display = "block";
    this._element.removeAttribute("aria-hidden");
    this._element.setAttribute("aria-modal", true);
    this._element.setAttribute("role", "dialog");
    this._element.scrollTop = 0;
    const e = _.findOne(Is, this._dialog);
    e && (e.scrollTop = 0);
    reflow(this._element);
    this._element.classList.add(Ls);
    const transitionComplete = () => {
      this._config.focus && this._focustrap.activate();
      this._isTransitioning = false;
      m.trigger(this._element, bs, { relatedTarget: t });
    };
    this._queueCallback(transitionComplete, this._dialog, this._isAnimated());
  }
  _addEventListeners() {
    m.on(this._element, Es, (t) => {
      t.key === fs &&
        (this._config.keyboard
          ? this.hide()
          : this._triggerBackdropTransition());
    });
    m.on(window, vs, () => {
      this._isShown && !this._isTransitioning && this._adjustDialog();
    });
    m.on(this._element, ws, (t) => {
      m.one(this._element, ys, (e) => {
        this._element === t.target &&
          this._element === e.target &&
          ("static" !== this._config.backdrop
            ? this._config.backdrop && this.hide()
            : this._triggerBackdropTransition());
      });
    });
  }
  _hideModal() {
    this._element.style.display = "none";
    this._element.setAttribute("aria-hidden", true);
    this._element.removeAttribute("aria-modal");
    this._element.removeAttribute("role");
    this._isTransitioning = false;
    this._backdrop.hide(() => {
      document.body.classList.remove(Cs);
      this._resetAdjustments();
      this._scrollBar.reset();
      m.trigger(this._element, ps);
    });
  }
  _isAnimated() {
    return this._element.classList.contains(Ts);
  }
  _triggerBackdropTransition() {
    const t = m.trigger(this._element, gs);
    if (t.defaultPrevented) return;
    const e =
      this._element.scrollHeight > document.documentElement.clientHeight;
    const s = this._element.style.overflowY;
    if ("hidden" !== s && !this._element.classList.contains($s)) {
      e || (this._element.style.overflowY = "hidden");
      this._element.classList.add($s);
      this._queueCallback(() => {
        this._element.classList.remove($s);
        this._queueCallback(() => {
          this._element.style.overflowY = s;
        }, this._dialog);
      }, this._dialog);
      this._element.focus();
    }
  }
  _adjustDialog() {
    const t =
      this._element.scrollHeight > document.documentElement.clientHeight;
    const e = this._scrollBar.getWidth();
    const s = e > 0;
    if (s && !t) {
      const t = isRTL() ? "paddingLeft" : "paddingRight";
      this._element.style[t] = `${e}px`;
    }
    if (!s && t) {
      const t = isRTL() ? "paddingRight" : "paddingLeft";
      this._element.style[t] = `${e}px`;
    }
  }
  _resetAdjustments() {
    this._element.style.paddingLeft = "";
    this._element.style.paddingRight = "";
  }
  static jQueryInterface(t, e) {
    return this.each(function () {
      const s = Modal.getOrCreateInstance(this, t);
      if ("string" === typeof t) {
        if ("undefined" === typeof s[t])
          throw new TypeError(`No method named "${t}"`);
        s[t](e);
      }
    });
  }
}
m.on(document, As, Os, function (t) {
  const e = _.getElementFromSelector(this);
  ["A", "AREA"].includes(this.tagName) && t.preventDefault();
  m.one(e, _s, (t) => {
    t.defaultPrevented ||
      m.one(e, ps, () => {
        isVisible(this) && this.focus();
      });
  });
  const s = _.findOne(Ss);
  s && Modal.getInstance(s).hide();
  const n = Modal.getOrCreateInstance(e);
  n.toggle(this);
});
enableDismissTrigger(Modal);
defineJQueryPlugin(Modal);
const Ds = "offcanvas";
const Ms = "bs.offcanvas";
const xs = `.${Ms}`;
const Bs = ".data-api";
const Fs = `load${xs}${Bs}`;
const js = "Escape";
const Hs = "show";
const zs = "showing";
const qs = "hiding";
const Vs = "offcanvas-backdrop";
const Ks = ".offcanvas.show";
const Ws = `show${xs}`;
const Qs = `shown${xs}`;
const Rs = `hide${xs}`;
const Us = `hidePrevented${xs}`;
const Ys = `hidden${xs}`;
const Xs = `resize${xs}`;
const Gs = `click${xs}${Bs}`;
const Js = `keydown.dismiss${xs}`;
const Zs = '[data-bs-toggle="offcanvas"]';
const tn = { backdrop: true, keyboard: true, scroll: false };
const en = {
  backdrop: "(boolean|string)",
  keyboard: "boolean",
  scroll: "boolean",
};
class Offcanvas extends BaseComponent {
  constructor(t, e) {
    super(t, e);
    this._isShown = false;
    this._backdrop = this._initializeBackDrop();
    this._focustrap = this._initializeFocusTrap();
    this._addEventListeners();
  }
  static get Default() {
    return tn;
  }
  static get DefaultType() {
    return en;
  }
  static get NAME() {
    return Ds;
  }
  toggle(t) {
    return this._isShown ? this.hide() : this.show(t);
  }
  show(t) {
    if (this._isShown) return;
    const e = m.trigger(this._element, Ws, { relatedTarget: t });
    if (e.defaultPrevented) return;
    this._isShown = true;
    this._backdrop.show();
    this._config.scroll || new ScrollBarHelper().hide();
    this._element.setAttribute("aria-modal", true);
    this._element.setAttribute("role", "dialog");
    this._element.classList.add(zs);
    const completeCallBack = () => {
      (this._config.scroll && !this._config.backdrop) ||
        this._focustrap.activate();
      this._element.classList.add(Hs);
      this._element.classList.remove(zs);
      m.trigger(this._element, Qs, { relatedTarget: t });
    };
    this._queueCallback(completeCallBack, this._element, true);
  }
  hide() {
    if (!this._isShown) return;
    const t = m.trigger(this._element, Rs);
    if (t.defaultPrevented) return;
    this._focustrap.deactivate();
    this._element.blur();
    this._isShown = false;
    this._element.classList.add(qs);
    this._backdrop.hide();
    const completeCallback = () => {
      this._element.classList.remove(Hs, qs);
      this._element.removeAttribute("aria-modal");
      this._element.removeAttribute("role");
      this._config.scroll || new ScrollBarHelper().reset();
      m.trigger(this._element, Ys);
    };
    this._queueCallback(completeCallback, this._element, true);
  }
  dispose() {
    this._backdrop.dispose();
    this._focustrap.deactivate();
    super.dispose();
  }
  _initializeBackDrop() {
    const clickCallback = () => {
      "static" !== this._config.backdrop
        ? this.hide()
        : m.trigger(this._element, Us);
    };
    const t = Boolean(this._config.backdrop);
    return new Backdrop({
      className: Vs,
      isVisible: t,
      isAnimated: true,
      rootElement: this._element.parentNode,
      clickCallback: t ? clickCallback : null,
    });
  }
  _initializeFocusTrap() {
    return new FocusTrap({ trapElement: this._element });
  }
  _addEventListeners() {
    m.on(this._element, Js, (t) => {
      t.key === js &&
        (this._config.keyboard ? this.hide() : m.trigger(this._element, Us));
    });
  }
  static jQueryInterface(t) {
    return this.each(function () {
      const e = Offcanvas.getOrCreateInstance(this, t);
      if ("string" === typeof t) {
        if (void 0 === e[t] || t.startsWith("_") || "constructor" === t)
          throw new TypeError(`No method named "${t}"`);
        e[t](this);
      }
    });
  }
}
m.on(document, Gs, Zs, function (t) {
  const e = _.getElementFromSelector(this);
  ["A", "AREA"].includes(this.tagName) && t.preventDefault();
  if (isDisabled(this)) return;
  m.one(e, Ys, () => {
    isVisible(this) && this.focus();
  });
  const s = _.findOne(Ks);
  s && s !== e && Offcanvas.getInstance(s).hide();
  const n = Offcanvas.getOrCreateInstance(e);
  n.toggle(this);
});
m.on(window, Fs, () => {
  for (const t of _.find(Ks)) Offcanvas.getOrCreateInstance(t).show();
});
m.on(window, Xs, () => {
  for (const t of _.find("[aria-modal][class*=show][class*=offcanvas-]"))
    "fixed" !== getComputedStyle(t).position &&
      Offcanvas.getOrCreateInstance(t).hide();
});
enableDismissTrigger(Offcanvas);
defineJQueryPlugin(Offcanvas);
const sn = "orangenavbar";
const nn = "bs.orangenavbar";
const on = `.${nn}`;
const rn = ".data-api";
const an = `scroll${on}${rn}`;
const cn = `load${on}${rn}`;
const ln = "header.sticky-top";
class OrangeNavbar extends BaseComponent {
  static get NAME() {
    return sn;
  }
  static enableMinimizing(t) {
    window.scrollY > 0
      ? t.classList.add("header-minimized")
      : t.classList.remove("header-minimized");
  }
  static jQueryInterface(t) {
    return this.each(function () {
      const e = OrangeNavbar.getOrCreateInstance(this, t);
      if ("string" === typeof t) {
        if ("undefined" === typeof e[t])
          throw new TypeError(`No method named "${t}"`);
        e[t]();
      }
    });
  }
}
m.on(window, an, () => {
  for (const t of _.find(ln)) OrangeNavbar.enableMinimizing(t);
});
m.on(window, cn, () => {
  for (const t of _.find(ln)) OrangeNavbar.enableMinimizing(t);
});
defineJQueryPlugin(OrangeNavbar);
const un = /^aria-[\w-]*$/i;
const hn = {
  "*": ["class", "dir", "id", "lang", "role", un],
  a: ["target", "href", "title", "rel"],
  area: [],
  b: [],
  br: [],
  col: [],
  code: [],
  div: [],
  em: [],
  hr: [],
  h1: [],
  h2: [],
  h3: [],
  h4: [],
  h5: [],
  h6: [],
  i: [],
  img: ["src", "srcset", "alt", "title", "width", "height"],
  li: [],
  ol: [],
  p: [],
  pre: [],
  s: [],
  small: [],
  span: [],
  sub: [],
  sup: [],
  strong: [],
  u: [],
  ul: [],
};
const dn = new Set([
  "background",
  "cite",
  "href",
  "itemtype",
  "longdesc",
  "poster",
  "src",
  "xlink:href",
]);
const fn = /^(?!javascript:)(?:[a-z0-9+.-]+:|[^&:/?#]*(?:[/?#]|$))/i;
const allowedAttribute = (t, e) => {
  const s = t.nodeName.toLowerCase();
  return e.includes(s)
    ? !dn.has(s) || Boolean(fn.test(t.nodeValue))
    : e.filter((t) => t instanceof RegExp).some((t) => t.test(s));
};
function sanitizeHtml(t, e, s) {
  if (!t.length) return t;
  if (s && "function" === typeof s) return s(t);
  const n = new window.DOMParser();
  const i = n.parseFromString(t, "text/html");
  const o = [].concat(...i.body.querySelectorAll("*"));
  for (const t of o) {
    const s = t.nodeName.toLowerCase();
    if (!Object.keys(e).includes(s)) {
      t.remove();
      continue;
    }
    const n = [].concat(...t.attributes);
    const i = [].concat(e["*"] || [], e[s] || []);
    for (const e of n) allowedAttribute(e, i) || t.removeAttribute(e.nodeName);
  }
  return i.body.innerHTML;
}
const mn = "TemplateFactory";
const gn = {
  allowList: hn,
  content: {},
  extraClass: "",
  html: false,
  sanitize: true,
  sanitizeFn: null,
  template: "<div></div>",
};
const pn = {
  allowList: "object",
  content: "object",
  extraClass: "(string|function)",
  html: "boolean",
  sanitize: "boolean",
  sanitizeFn: "(null|function)",
  template: "string",
};
const _n = {
  selector: "(string|element)",
  entry: "(string|element|function|null)",
};
class TemplateFactory extends Config {
  constructor(t) {
    super();
    this._config = this._getConfig(t);
  }
  static get Default() {
    return gn;
  }
  static get DefaultType() {
    return pn;
  }
  static get NAME() {
    return mn;
  }
  getContent() {
    return Object.values(this._config.content)
      .map((t) => this._resolvePossibleFunction(t))
      .filter(Boolean);
  }
  hasContent() {
    return this.getContent().length > 0;
  }
  changeContent(t) {
    this._checkContent(t);
    this._config.content = { ...this._config.content, ...t };
    return this;
  }
  toHtml() {
    const t = document.createElement("div");
    t.innerHTML = this._maybeSanitize(this._config.template);
    for (const [e, s] of Object.entries(this._config.content))
      this._setContent(t, s, e);
    const e = t.children[0];
    const s = this._resolvePossibleFunction(this._config.extraClass);
    s && e.classList.add(...s.split(" "));
    return e;
  }
  _typeCheckConfig(t) {
    super._typeCheckConfig(t);
    this._checkContent(t.content);
  }
  _checkContent(t) {
    for (const [e, s] of Object.entries(t))
      super._typeCheckConfig({ selector: e, entry: s }, _n);
  }
  _setContent(t, e, s) {
    const n = _.findOne(s, t);
    if (n) {
      e = this._resolvePossibleFunction(e);
      e
        ? isElement(e)
          ? this._putElementInTemplate(getElement(e), n)
          : this._config.html
            ? (n.innerHTML = this._maybeSanitize(e))
            : (n.textContent = e)
        : n.remove();
    }
  }
  _maybeSanitize(t) {
    return this._config.sanitize
      ? sanitizeHtml(t, this._config.allowList, this._config.sanitizeFn)
      : t;
  }
  _resolvePossibleFunction(t) {
    return execute(t, [this]);
  }
  _putElementInTemplate(t, e) {
    if (this._config.html) {
      e.innerHTML = "";
      e.append(t);
    } else e.textContent = t.textContent;
  }
}
const bn = "tooltip";
const vn = new Set(["sanitize", "allowList", "sanitizeFn"]);
const yn = "fade";
const wn = "modal";
const En = "show";
const An = ".tooltip-inner";
const Cn = `.${wn}`;
const Tn = "hide.bs.modal";
const Ln = "hover";
const $n = "focus";
const Sn = "click";
const kn = "manual";
const In = "hide";
const On = "hidden";
const Pn = "show";
const Nn = "shown";
const Dn = "inserted";
const Mn = "click";
const xn = "focusin";
const Bn = "focusout";
const Fn = "mouseenter";
const jn = "mouseleave";
const Hn = {
  AUTO: "auto",
  TOP: "top",
  RIGHT: isRTL() ? "left" : "right",
  BOTTOM: "bottom",
  LEFT: isRTL() ? "right" : "left",
};
const zn = {
  allowList: hn,
  animation: true,
  boundary: "clippingParents",
  container: false,
  customClass: "",
  delay: 0,
  fallbackPlacements: ["top", "right", "bottom", "left"],
  html: false,
  offset: [0, 10],
  placement: "top",
  popperConfig: null,
  sanitize: true,
  sanitizeFn: null,
  selector: false,
  template:
    '<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',
  title: "",
  trigger: "hover focus",
};
const qn = {
  allowList: "object",
  animation: "boolean",
  boundary: "(string|element)",
  container: "(string|element|boolean)",
  customClass: "(string|function)",
  delay: "(number|object)",
  fallbackPlacements: "array",
  html: "boolean",
  offset: "(array|string|function)",
  placement: "(string|function)",
  popperConfig: "(null|object|function)",
  sanitize: "boolean",
  sanitizeFn: "(null|function)",
  selector: "(string|boolean)",
  template: "string",
  title: "(string|element|function)",
  trigger: "string",
};
class Tooltip extends BaseComponent {
  constructor(e, s) {
    if ("undefined" === typeof t)
      throw new TypeError(
        "Bootstrap's tooltips require Popper (https://popper.js.org)",
      );
    super(e, s);
    this._isEnabled = true;
    this._timeout = 0;
    this._isHovered = null;
    this._activeTrigger = {};
    this._popper = null;
    this._templateFactory = null;
    this._newContent = null;
    this.tip = null;
    this._setListeners();
    this._config.selector || this._fixTitle();
  }
  static get Default() {
    return zn;
  }
  static get DefaultType() {
    return qn;
  }
  static get NAME() {
    return bn;
  }
  enable() {
    this._isEnabled = true;
  }
  disable() {
    this._isEnabled = false;
  }
  toggleEnabled() {
    this._isEnabled = !this._isEnabled;
  }
  toggle() {
    if (this._isEnabled) {
      this._activeTrigger.click = !this._activeTrigger.click;
      this._isShown() ? this._leave() : this._enter();
    }
  }
  dispose() {
    clearTimeout(this._timeout);
    m.off(this._element.closest(Cn), Tn, this._hideModalHandler);
    this._element.getAttribute("data-bs-original-title") &&
      this._element.setAttribute(
        "title",
        this._element.getAttribute("data-bs-original-title"),
      );
    this._disposePopper();
    super.dispose();
  }
  show() {
    if ("none" === this._element.style.display)
      throw new Error("Please use show on visible elements");
    if (!(this._isWithContent() && this._isEnabled)) return;
    const t = m.trigger(this._element, this.constructor.eventName(Pn));
    const e = findShadowRoot(this._element);
    const s = (e || this._element.ownerDocument.documentElement).contains(
      this._element,
    );
    if (t.defaultPrevented || !s) return;
    this._disposePopper();
    const n = this._getTipElement();
    this._element.setAttribute("aria-describedby", n.getAttribute("id"));
    const { container: i } = this._config;
    if (!this._element.ownerDocument.documentElement.contains(this.tip)) {
      i.append(n);
      m.trigger(this._element, this.constructor.eventName(Dn));
    }
    this._popper = this._createPopper(n);
    n.classList.add(En);
    if ("ontouchstart" in document.documentElement)
      for (const t of [].concat(...document.body.children))
        m.on(t, "mouseover", noop);
    const complete = () => {
      m.trigger(this._element, this.constructor.eventName(Nn));
      false === this._isHovered && this._leave();
      this._isHovered = false;
    };
    this._queueCallback(complete, this.tip, this._isAnimated());
  }
  hide() {
    if (!this._isShown()) return;
    const t = m.trigger(this._element, this.constructor.eventName(In));
    if (t.defaultPrevented) return;
    const e = this._getTipElement();
    e.classList.remove(En);
    if ("ontouchstart" in document.documentElement)
      for (const t of [].concat(...document.body.children))
        m.off(t, "mouseover", noop);
    this._activeTrigger[Sn] = false;
    this._activeTrigger[$n] = false;
    this._activeTrigger[Ln] = false;
    this._isHovered = null;
    const complete = () => {
      if (!this._isWithActiveTrigger()) {
        this._isHovered || this._disposePopper();
        this._element.removeAttribute("aria-describedby");
        m.trigger(this._element, this.constructor.eventName(On));
      }
    };
    this._queueCallback(complete, this.tip, this._isAnimated());
  }
  update() {
    this._popper && this._popper.update();
  }
  _isWithContent() {
    return Boolean(this._getTitle());
  }
  _getTipElement() {
    this.tip ||
      (this.tip = this._createTipElement(
        this._newContent || this._getContentForTemplate(),
      ));
    return this.tip;
  }
  _createTipElement(t) {
    const e = this._getTemplateFactory(t).toHtml();
    if (!e) return null;
    e.classList.remove(yn, En);
    e.classList.add(`bs-${this.constructor.NAME}-auto`);
    const s = getUID(this.constructor.NAME).toString();
    e.setAttribute("id", s);
    this._isAnimated() && e.classList.add(yn);
    return e;
  }
  setContent(t) {
    this._newContent = t;
    if (this._isShown()) {
      this._disposePopper();
      this.show();
    }
  }
  _getTemplateFactory(t) {
    this._templateFactory
      ? this._templateFactory.changeContent(t)
      : (this._templateFactory = new TemplateFactory({
          ...this._config,
          content: t,
          extraClass: this._resolvePossibleFunction(this._config.customClass),
        }));
    return this._templateFactory;
  }
  _getContentForTemplate() {
    return { [An]: this._getTitle() };
  }
  _getTitle() {
    return (
      this._resolvePossibleFunction(this._config.title) ||
      this._element.getAttribute("data-bs-original-title")
    );
  }
  _initializeOnDelegatedTarget(t) {
    return this.constructor.getOrCreateInstance(
      t.delegateTarget,
      this._getDelegateConfig(),
    );
  }
  _isAnimated() {
    return (
      this._config.animation || (this.tip && this.tip.classList.contains(yn))
    );
  }
  _isShown() {
    return this.tip && this.tip.classList.contains(En);
  }
  _createPopper(e) {
    const s = execute(this._config.placement, [this, e, this._element]);
    const n = Hn[s.toUpperCase()];
    return t.createPopper(this._element, e, this._getPopperConfig(n));
  }
  _getOffset() {
    const { offset: t } = this._config;
    return "string" === typeof t
      ? t.split(",").map((t) => Number.parseInt(t, 10))
      : "function" === typeof t
        ? (e) => t(e, this._element)
        : t;
  }
  _resolvePossibleFunction(t) {
    return execute(t, [this._element]);
  }
  _getPopperConfig(t) {
    const e = {
      placement: t,
      modifiers: [
        {
          name: "flip",
          options: { fallbackPlacements: this._config.fallbackPlacements },
        },
        { name: "offset", options: { offset: this._getOffset() } },
        {
          name: "preventOverflow",
          options: { boundary: this._config.boundary },
        },
        {
          name: "arrow",
          options: { element: `.${this.constructor.NAME}-arrow` },
        },
        {
          name: "preSetPlacement",
          enabled: true,
          phase: "beforeMain",
          fn: (t) => {
            this._getTipElement().setAttribute(
              "data-popper-placement",
              t.state.placement,
            );
          },
        },
      ],
    };
    return { ...e, ...execute(this._config.popperConfig, [e]) };
  }
  _setListeners() {
    const t = this._config.trigger.split(" ");
    for (const e of t)
      if ("click" === e)
        m.on(
          this._element,
          this.constructor.eventName(Mn),
          this._config.selector,
          (t) => {
            const e = this._initializeOnDelegatedTarget(t);
            e.toggle();
          },
        );
      else if (e !== kn) {
        const t =
          e === Ln
            ? this.constructor.eventName(Fn)
            : this.constructor.eventName(xn);
        const s =
          e === Ln
            ? this.constructor.eventName(jn)
            : this.constructor.eventName(Bn);
        m.on(this._element, t, this._config.selector, (t) => {
          const e = this._initializeOnDelegatedTarget(t);
          e._activeTrigger["focusin" === t.type ? $n : Ln] = true;
          e._enter();
        });
        m.on(this._element, s, this._config.selector, (t) => {
          const e = this._initializeOnDelegatedTarget(t);
          e._activeTrigger["focusout" === t.type ? $n : Ln] =
            e._element.contains(t.relatedTarget);
          e._leave();
        });
      }
    this._hideModalHandler = () => {
      this._element && this.hide();
    };
    m.on(this._element.closest(Cn), Tn, this._hideModalHandler);
  }
  _fixTitle() {
    const t = this._element.getAttribute("title");
    if (t) {
      this._element.getAttribute("aria-label") ||
        this._element.textContent.trim() ||
        this._element.setAttribute("aria-label", t);
      this._element.setAttribute("data-bs-original-title", t);
      this._element.removeAttribute("title");
    }
  }
  _enter() {
    if (this._isShown() || this._isHovered) this._isHovered = true;
    else {
      this._isHovered = true;
      this._setTimeout(() => {
        this._isHovered && this.show();
      }, this._config.delay.show);
    }
  }
  _leave() {
    if (!this._isWithActiveTrigger()) {
      this._isHovered = false;
      this._setTimeout(() => {
        this._isHovered || this.hide();
      }, this._config.delay.hide);
    }
  }
  _setTimeout(t, e) {
    clearTimeout(this._timeout);
    this._timeout = setTimeout(t, e);
  }
  _isWithActiveTrigger() {
    return Object.values(this._activeTrigger).includes(true);
  }
  _getConfig(t) {
    const e = g.getDataAttributes(this._element);
    for (const t of Object.keys(e)) vn.has(t) && delete e[t];
    t = { ...e, ...("object" === typeof t && t ? t : {}) };
    t = this._mergeConfigObj(t);
    t = this._configAfterMerge(t);
    this._typeCheckConfig(t);
    return t;
  }
  _configAfterMerge(t) {
    t.container =
      false === t.container ? document.body : getElement(t.container);
    "number" === typeof t.delay && (t.delay = { show: t.delay, hide: t.delay });
    "number" === typeof t.title && (t.title = t.title.toString());
    "number" === typeof t.content && (t.content = t.content.toString());
    return t;
  }
  _getDelegateConfig() {
    const t = {};
    for (const [e, s] of Object.entries(this._config))
      this.constructor.Default[e] !== s && (t[e] = s);
    t.selector = false;
    t.trigger = "manual";
    return t;
  }
  _disposePopper() {
    if (this._popper) {
      this._popper.destroy();
      this._popper = null;
    }
    if (this.tip) {
      this.tip.remove();
      this.tip = null;
    }
  }
  static jQueryInterface(t) {
    return this.each(function () {
      const e = Tooltip.getOrCreateInstance(this, t);
      if ("string" === typeof t) {
        if ("undefined" === typeof e[t])
          throw new TypeError(`No method named "${t}"`);
        e[t]();
      }
    });
  }
}
defineJQueryPlugin(Tooltip);
const Vn = "popover";
const Kn = ".popover-header";
const Wn = ".popover-body";
const Qn = {
  ...Tooltip.Default,
  content: "",
  offset: [0, 15],
  placement: "right",
  template:
    '<div class="popover" role="tooltip"><div class="popover-arrow"></div><h3 class="popover-header"></h3><div class="popover-body"></div></div>',
  trigger: "click",
};
const Rn = {
  ...Tooltip.DefaultType,
  content: "(null|string|element|function)",
};
class Popover extends Tooltip {
  static get Default() {
    return Qn;
  }
  static get DefaultType() {
    return Rn;
  }
  static get NAME() {
    return Vn;
  }
  _isWithContent() {
    return this._getTitle() || this._getContent();
  }
  _getContentForTemplate() {
    return { [Kn]: this._getTitle(), [Wn]: this._getContent() };
  }
  _getContent() {
    return this._resolvePossibleFunction(this._config.content);
  }
  static jQueryInterface(t) {
    return this.each(function () {
      const e = Popover.getOrCreateInstance(this, t);
      if ("string" === typeof t) {
        if ("undefined" === typeof e[t])
          throw new TypeError(`No method named "${t}"`);
        e[t]();
      }
    });
  }
}
defineJQueryPlugin(Popover);
const Un = "quantityselector";
const Yn = "bs.quantityselector";
const Xn = `.${Yn}`;
const Gn = ".data-api";
const Jn = `load${Xn}${Gn}`;
const Zn = `change${Xn}${Gn}`;
const ti = `click${Xn}${Gn}`;
const ei = '[data-bs-step="up"]';
const si = '[data-bs-step="down"]';
const ni = '[data-bs-step="counter"]';
const ii = ".input-group.quantity-selector";
class QuantitySelector extends BaseComponent {
  static get NAME() {
    return Un;
  }
  ValueOnLoad(t) {
    const e = t.querySelector(ni);
    const s = t.querySelector(ei);
    const n = t.querySelector(si);
    const i = e.getAttribute("min");
    const o = e.getAttribute("max");
    const r = Number(e.getAttribute("step"));
    Number(e.value) - r < i && n.setAttribute("disabled", "");
    Number(e.value) + r > o && s.setAttribute("disabled", "");
  }
  static StepUp(t) {
    const e = t.target.closest(ii);
    const s = e.querySelector(ni);
    const n = s.getAttribute("max");
    const i = Number(s.getAttribute("step"));
    const o = Number(s.getAttribute("data-bs-round"));
    const r = new Event("change");
    Number(s.value) < n &&
      (s.value = (Number(s.value) + i).toFixed(o).toString());
    s.dispatchEvent(r);
  }
  static StepDown(t) {
    const e = t.target.closest(ii);
    const s = e.querySelector(ni);
    const n = s.getAttribute("min");
    const i = Number(s.getAttribute("step"));
    const o = Number(s.getAttribute("data-bs-round"));
    const r = new Event("change");
    Number(s.value) > n &&
      (s.value = (Number(s.value) - i).toFixed(o).toString());
    s.dispatchEvent(r);
  }
  static CheckIfDisabledOnChange(t) {
    const e = t.target.closest(ii);
    const s = e.querySelector(ni);
    const n = e.querySelector(ei);
    const i = e.querySelector(si);
    const o = s.getAttribute("min");
    const r = s.getAttribute("max");
    const a = Number(s.getAttribute("step"));
    n.removeAttribute("disabled", "");
    i.removeAttribute("disabled", "");
    Number(s.value) - a < o && i.setAttribute("disabled", "");
    Number(s.value) + a > r && n.setAttribute("disabled", "");
  }
  static jQueryInterface(t) {
    return this.each(function () {
      const e = QuantitySelector.getOrCreateInstance(this, t);
      if ("string" === typeof t) {
        if ("undefined" === typeof e[t])
          throw new TypeError(`No method named "${t}"`);
        e[t]();
      }
    });
  }
}
m.on(document, Zn, ni, QuantitySelector.CheckIfDisabledOnChange);
m.on(document, ti, ei, QuantitySelector.StepUp);
m.on(document, ti, si, QuantitySelector.StepDown);
m.on(window, Jn, () => {
  for (const t of _.find(ii))
    QuantitySelector.getOrCreateInstance(t).ValueOnLoad(t);
});
defineJQueryPlugin(QuantitySelector);
const oi = "scrollspy";
const ri = "bs.scrollspy";
const ai = `.${ri}`;
const ci = ".data-api";
const li = `activate${ai}`;
const ui = `click${ai}`;
const hi = `load${ai}${ci}`;
const di = "dropdown-item";
const fi = "active";
const mi = '[data-bs-spy="scroll"]';
const gi = "[href]";
const pi = ".nav, .list-group";
const _i = ".nav-link";
const bi = ".nav-item";
const vi = ".list-group-item";
const yi = `${_i}, ${bi} > ${_i}, ${vi}`;
const wi = ".dropdown";
const Ei = ".dropdown-toggle";
const Ai = {
  offset: null,
  rootMargin: "0px 0px -25%",
  smoothScroll: false,
  target: null,
  threshold: [0.1, 0.5, 1],
};
const Ci = {
  offset: "(number|null)",
  rootMargin: "string",
  smoothScroll: "boolean",
  target: "element",
  threshold: "array",
};
class ScrollSpy extends BaseComponent {
  constructor(t, e) {
    super(t, e);
    this._targetLinks = new Map();
    this._observableSections = new Map();
    this._rootElement =
      "visible" === getComputedStyle(this._element).overflowY
        ? null
        : this._element;
    this._activeTarget = null;
    this._observer = null;
    this._previousScrollData = { visibleEntryTop: 0, parentScrollTop: 0 };
    this.refresh();
  }
  static get Default() {
    return Ai;
  }
  static get DefaultType() {
    return Ci;
  }
  static get NAME() {
    return oi;
  }
  refresh() {
    this._initializeTargetsAndObservables();
    this._maybeEnableSmoothScroll();
    this._observer
      ? this._observer.disconnect()
      : (this._observer = this._getNewObserver());
    for (const t of this._observableSections.values())
      this._observer.observe(t);
  }
  dispose() {
    this._observer.disconnect();
    super.dispose();
  }
  _configAfterMerge(t) {
    t.target = getElement(t.target) || document.body;
    t.rootMargin = t.offset ? `${t.offset}px 0px -30%` : t.rootMargin;
    "string" === typeof t.threshold &&
      (t.threshold = t.threshold.split(",").map((t) => Number.parseFloat(t)));
    return t;
  }
  _maybeEnableSmoothScroll() {
    if (this._config.smoothScroll) {
      m.off(this._config.target, ui);
      m.on(this._config.target, ui, gi, (t) => {
        const e = this._observableSections.get(t.target.hash);
        if (e) {
          t.preventDefault();
          const s = this._rootElement || window;
          const n = e.offsetTop - this._element.offsetTop;
          if (s.scrollTo) {
            s.scrollTo({ top: n, behavior: "smooth" });
            return;
          }
          s.scrollTop = n;
        }
      });
    }
  }
  _getNewObserver() {
    const t = {
      root: this._rootElement,
      threshold: this._config.threshold,
      rootMargin: this._config.rootMargin,
    };
    return new IntersectionObserver((t) => this._observerCallback(t), t);
  }
  _observerCallback(t) {
    const targetElement = (t) => this._targetLinks.get(`#${t.target.id}`);
    const activate = (t) => {
      this._previousScrollData.visibleEntryTop = t.target.offsetTop;
      this._process(targetElement(t));
    };
    const e = (this._rootElement || document.documentElement).scrollTop;
    const s = e >= this._previousScrollData.parentScrollTop;
    this._previousScrollData.parentScrollTop = e;
    for (const n of t) {
      if (!n.isIntersecting) {
        this._activeTarget = null;
        this._clearActiveClass(targetElement(n));
        continue;
      }
      const t = n.target.offsetTop >= this._previousScrollData.visibleEntryTop;
      if (s && t) {
        activate(n);
        if (!e) return;
      } else s || t || activate(n);
    }
  }
  _initializeTargetsAndObservables() {
    this._targetLinks = new Map();
    this._observableSections = new Map();
    const t = _.find(gi, this._config.target);
    for (const e of t) {
      if (!e.hash || isDisabled(e)) continue;
      const t = _.findOne(decodeURI(e.hash), this._element);
      if (isVisible(t)) {
        this._targetLinks.set(decodeURI(e.hash), e);
        this._observableSections.set(e.hash, t);
      }
    }
  }
  _process(t) {
    if (this._activeTarget !== t) {
      this._clearActiveClass(this._config.target);
      this._activeTarget = t;
      t.classList.add(fi);
      this._activateParents(t);
      m.trigger(this._element, li, { relatedTarget: t });
    }
  }
  _activateParents(t) {
    if (t.classList.contains(di))
      _.findOne(Ei, t.closest(wi)).classList.add(fi);
    else
      for (const e of _.parents(t, pi))
        for (const t of _.prev(e, yi)) t.classList.add(fi);
  }
  _clearActiveClass(t) {
    t.classList.remove(fi);
    const e = _.find(`${gi}.${fi}`, t);
    for (const t of e) t.classList.remove(fi);
  }
  static jQueryInterface(t) {
    return this.each(function () {
      const e = ScrollSpy.getOrCreateInstance(this, t);
      if ("string" === typeof t) {
        if (void 0 === e[t] || t.startsWith("_") || "constructor" === t)
          throw new TypeError(`No method named "${t}"`);
        e[t]();
      }
    });
  }
}
m.on(window, hi, () => {
  for (const t of _.find(mi)) ScrollSpy.getOrCreateInstance(t);
});
defineJQueryPlugin(ScrollSpy);
const Ti = "tab";
const Li = "bs.tab";
const $i = `.${Li}`;
const Si = `hide${$i}`;
const ki = `hidden${$i}`;
const Ii = `show${$i}`;
const Oi = `shown${$i}`;
const Pi = `click${$i}`;
const Ni = `keydown${$i}`;
const Di = `load${$i}`;
const Mi = "ArrowLeft";
const xi = "ArrowRight";
const Bi = "ArrowUp";
const Fi = "ArrowDown";
const ji = "Home";
const Hi = "End";
const zi = "active";
const qi = "fade";
const Vi = "show";
const Ki = "dropdown";
const Wi = ".dropdown-toggle";
const Qi = ".dropdown-menu";
const Ri = `:not(${Wi})`;
const Ui = '.list-group, .nav, [role="tablist"]';
const Yi = ".nav-item, .list-group-item";
const Xi = `.nav-link${Ri}, .list-group-item${Ri}, [role="tab"]${Ri}`;
const Gi =
  '[data-bs-toggle="tab"], [data-bs-toggle="pill"], [data-bs-toggle="list"]';
const Ji = `${Xi}, ${Gi}`;
const Zi = `.${zi}[data-bs-toggle="tab"], .${zi}[data-bs-toggle="pill"], .${zi}[data-bs-toggle="list"]`;
class Tab extends BaseComponent {
  constructor(t) {
    super(t);
    this._parent = this._element.closest(Ui);
    if (this._parent) {
      this._setInitialAttributes(this._parent, this._getChildren());
      m.on(this._element, Ni, (t) => this._keydown(t));
    }
  }
  static get NAME() {
    return Ti;
  }
  show() {
    const t = this._element;
    if (this._elemIsActive(t)) return;
    const e = this._getActiveElem();
    const s = e ? m.trigger(e, Si, { relatedTarget: t }) : null;
    const n = m.trigger(t, Ii, { relatedTarget: e });
    if (!(n.defaultPrevented || (s && s.defaultPrevented))) {
      this._deactivate(e, t);
      this._activate(t, e);
    }
  }
  _activate(t, e) {
    if (!t) return;
    t.classList.add(zi);
    this._activate(_.getElementFromSelector(t));
    const complete = () => {
      if ("tab" === t.getAttribute("role")) {
        t.removeAttribute("tabindex");
        t.setAttribute("aria-selected", true);
        this._toggleDropDown(t, true);
        m.trigger(t, Oi, { relatedTarget: e });
      } else t.classList.add(Vi);
    };
    this._queueCallback(complete, t, t.classList.contains(qi));
  }
  _deactivate(t, e) {
    if (!t) return;
    t.classList.remove(zi);
    t.blur();
    this._deactivate(_.getElementFromSelector(t));
    const complete = () => {
      if ("tab" === t.getAttribute("role")) {
        t.setAttribute("aria-selected", false);
        t.setAttribute("tabindex", "-1");
        this._toggleDropDown(t, false);
        m.trigger(t, ki, { relatedTarget: e });
      } else t.classList.remove(Vi);
    };
    this._queueCallback(complete, t, t.classList.contains(qi));
  }
  _keydown(t) {
    if (![Mi, xi, Bi, Fi, ji, Hi].includes(t.key)) return;
    t.stopPropagation();
    t.preventDefault();
    const e = this._getChildren().filter((t) => !isDisabled(t));
    let s;
    if ([ji, Hi].includes(t.key)) s = e[t.key === ji ? 0 : e.length - 1];
    else {
      const n = [xi, Fi].includes(t.key);
      s = getNextActiveElement(e, t.target, n, true);
    }
    if (s) {
      s.focus({ preventScroll: true });
      Tab.getOrCreateInstance(s).show();
    }
  }
  _getChildren() {
    return _.find(Ji, this._parent);
  }
  _getActiveElem() {
    return this._getChildren().find((t) => this._elemIsActive(t)) || null;
  }
  _setInitialAttributes(t, e) {
    this._setAttributeIfNotExists(t, "role", "tablist");
    for (const t of e) this._setInitialAttributesOnChild(t);
  }
  _setInitialAttributesOnChild(t) {
    t = this._getInnerElement(t);
    const e = this._elemIsActive(t);
    const s = this._getOuterElement(t);
    t.setAttribute("aria-selected", e);
    s !== t && this._setAttributeIfNotExists(s, "role", "presentation");
    e || t.setAttribute("tabindex", "-1");
    this._setAttributeIfNotExists(t, "role", "tab");
    this._setInitialAttributesOnTargetPanel(t);
  }
  _setInitialAttributesOnTargetPanel(t) {
    const e = _.getElementFromSelector(t);
    if (e) {
      this._setAttributeIfNotExists(e, "role", "tabpanel");
      t.id && this._setAttributeIfNotExists(e, "aria-labelledby", `${t.id}`);
    }
  }
  _toggleDropDown(t, e) {
    const s = this._getOuterElement(t);
    if (!s.classList.contains(Ki)) return;
    const toggle = (t, n) => {
      const i = _.findOne(t, s);
      i && i.classList.toggle(n, e);
    };
    toggle(Wi, zi);
    toggle(Qi, Vi);
    s.setAttribute("aria-expanded", e);
  }
  _setAttributeIfNotExists(t, e, s) {
    t.hasAttribute(e) || t.setAttribute(e, s);
  }
  _elemIsActive(t) {
    return t.classList.contains(zi);
  }
  _getInnerElement(t) {
    return t.matches(Ji) ? t : _.findOne(Ji, t);
  }
  _getOuterElement(t) {
    return t.closest(Yi) || t;
  }
  static jQueryInterface(t) {
    return this.each(function () {
      const e = Tab.getOrCreateInstance(this);
      if ("string" === typeof t) {
        if (void 0 === e[t] || t.startsWith("_") || "constructor" === t)
          throw new TypeError(`No method named "${t}"`);
        e[t]();
      }
    });
  }
}
m.on(document, Pi, Gi, function (t) {
  ["A", "AREA"].includes(this.tagName) && t.preventDefault();
  isDisabled(this) || Tab.getOrCreateInstance(this).show();
});
m.on(window, Di, () => {
  for (const t of _.find(Zi)) Tab.getOrCreateInstance(t);
});
defineJQueryPlugin(Tab);
const to = "toast";
const eo = "bs.toast";
const so = `.${eo}`;
const no = `mouseover${so}`;
const io = `mouseout${so}`;
const oo = `focusin${so}`;
const ro = `focusout${so}`;
const ao = `hide${so}`;
const co = `hidden${so}`;
const lo = `show${so}`;
const uo = `shown${so}`;
const ho = "fade";
const fo = "hide";
const mo = "show";
const go = "showing";
const po = { animation: "boolean", autohide: "boolean", delay: "number" };
const _o = { animation: true, autohide: true, delay: 5e3 };
class Toast extends BaseComponent {
  constructor(t, e) {
    super(t, e);
    this._timeout = null;
    this._hasMouseInteraction = false;
    this._hasKeyboardInteraction = false;
    this._setListeners();
  }
  static get Default() {
    return _o;
  }
  static get DefaultType() {
    return po;
  }
  static get NAME() {
    return to;
  }
  show() {
    const t = m.trigger(this._element, lo);
    if (t.defaultPrevented) return;
    this._clearTimeout();
    this._config.animation && this._element.classList.add(ho);
    const complete = () => {
      this._element.classList.remove(go);
      m.trigger(this._element, uo);
      this._maybeScheduleHide();
    };
    this._element.classList.remove(fo);
    reflow(this._element);
    this._element.classList.add(mo, go);
    this._queueCallback(complete, this._element, this._config.animation);
  }
  hide() {
    if (!this.isShown()) return;
    const t = m.trigger(this._element, ao);
    if (t.defaultPrevented) return;
    const complete = () => {
      this._element.classList.add(fo);
      this._element.classList.remove(go, mo);
      m.trigger(this._element, co);
    };
    this._element.classList.add(go);
    this._queueCallback(complete, this._element, this._config.animation);
  }
  dispose() {
    this._clearTimeout();
    this.isShown() && this._element.classList.remove(mo);
    super.dispose();
  }
  isShown() {
    return this._element.classList.contains(mo);
  }
  _maybeScheduleHide() {
    this._config.autohide &&
      (this._hasMouseInteraction ||
        this._hasKeyboardInteraction ||
        (this._timeout = setTimeout(() => {
          this.hide();
        }, this._config.delay)));
  }
  _onInteraction(t, e) {
    switch (t.type) {
      case "mouseover":
      case "mouseout":
        this._hasMouseInteraction = e;
        break;
      case "focusin":
      case "focusout":
        this._hasKeyboardInteraction = e;
        break;
    }
    if (e) {
      this._clearTimeout();
      return;
    }
    const s = t.relatedTarget;
    this._element === s ||
      this._element.contains(s) ||
      this._maybeScheduleHide();
  }
  _setListeners() {
    m.on(this._element, no, (t) => this._onInteraction(t, true));
    m.on(this._element, io, (t) => this._onInteraction(t, false));
    m.on(this._element, oo, (t) => this._onInteraction(t, true));
    m.on(this._element, ro, (t) => this._onInteraction(t, false));
  }
  _clearTimeout() {
    clearTimeout(this._timeout);
    this._timeout = null;
  }
  static jQueryInterface(t) {
    return this.each(function () {
      const e = Toast.getOrCreateInstance(this, t);
      if ("string" === typeof t) {
        if ("undefined" === typeof e[t])
          throw new TypeError(`No method named "${t}"`);
        e[t](this);
      }
    });
  }
}
enableDismissTrigger(Toast);
defineJQueryPlugin(Toast);
export {
  Alert,
  Button,
  Carousel,
  Collapse,
  Dropdown,
  Modal,
  Offcanvas,
  OrangeNavbar,
  Popover,
  QuantitySelector,
  ScrollSpy,
  Tab,
  Toast,
  Tooltip,
};
