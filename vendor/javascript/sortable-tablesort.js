// Software Name : NIF TZ
// SPDX-FileCopyrightText: Copyright (c) Orange SA
// SPDX-License-Identifier: Apache-2.0
//
// This software is distributed under the Apache-2.0
// ,
// See the "LICENSES" directory for more details.
//
// Authors:
// - Efflam Castel <efflam.castel@orange.com>

document.addEventListener("click",(function(e){try{function h(e,a){return e.nodeName===a?e:h(e.parentNode,a)}var a=e.shiftKey||e.altKey,r=h(e.target,"TH"),n=r.parentNode,s=n.parentNode,o=s.parentNode;function p(e){var r;return a?e.dataset.sortAlt:null!==(r=e.dataset.sort)&&void 0!==r?r:e.textContent}if("THEAD"===s.nodeName&&o.classList.contains("sortable")&&!r.classList.contains("no-sort")){var i,c=n.cells,l=parseInt(r.dataset.sortTbr);for(e=0;e<c.length;e++)c[e]===r?i=parseInt(r.dataset.sortCol)||e:c[e].setAttribute("aria-sort","none");c="descending";("descending"===r.getAttribute("aria-sort")||o.classList.contains("asc")&&"ascending"!==r.getAttribute("aria-sort"))&&(c="ascending");r.setAttribute("aria-sort",c);var d="ascending"===c,u=o.classList.contains("n-last"),t=function(e,a,r){a=p(a.cells[r]);e=p(e.cells[r]);if(u){if(""===a&&""!==e)return-1;if(""===e&&""!==a)return 1}r=Number(a)-Number(e);a=isNaN(r)?a.localeCompare(e):r;return d?-a:a};for(e=0;e<o.tBodies.length;e++){var f=o.tBodies[e],N=[].slice.call(f.rows,0);N.sort((function(e,a){var r=t(e,a,i);return 0!==r||isNaN(l)?r:t(e,a,l)}));var g=f.cloneNode();g.append.apply(g,N);o.replaceChild(g,f)}}}catch(v){}}));var e={};export{e as default};

