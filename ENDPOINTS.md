<!--
Software Name : NIF TZ
SPDX-FileCopyrightText: Copyright (c) Orange SA
SPDX-License-Identifier: Apache-2.0

This software is distributed under the Apache-2.0
,
See the "LICENSES" directory for more details.

Authors:
- Efflam Castel <efflam.castel@orange.com>
-->

# XTesting-API Endpoints

## HTML Endpoints

| Path      |
|-----------|
| /results  |
| /metrics  |

## JSON Endpoints

<!-- markdownlint-disable line-length -->
| HTTP Method | Path                                               |
|-------------|----------------------------------------------------|
| GET         | /api/v1/pods                                       |
| POST        | /api/v1/pods                                       |
| GET         | /api/v1/pods/{pod_name}                            |
| GET         | /api/v1/projects                                   |
| POST        | /api/v1/projects                                   |
| GET         | /api/v1/projects/{project_name}                    |
| DELETE      | /api/v1/projects/{project_name}                    |
| PUT         | /api/v1/projects/{project_name}                    |
| GET         | /api/v1/projects/{project_name}/cases              |
| POST        | /api/v1/projects/{project_name}/cases              |
| GET         | /api/v1/projects/{project_name}/cases/{case_name}  |
| PUT         | /api/v1/projects/{project_name}/cases/{case_name}  |
| DELETE      | /api/v1/projects/{project_name}/cases/{case_name}  |
| GET         | /api/v1/scenarios                                  |
| POST        | /api/v1/scenarios                                  |
| DELETE      | /api/v1/scenarios/{scenario_name}                  |
| GET         | /api/v1/scenarios/{scenario_name}                  |
| POST        | /api/v1/scenarios/{scenario_name}/installers       |
| PUT         | /api/v1/scenarios/{scenario_name}/owner            |
| POST        | /api/v1/scenarios/{scenario_name}/versions         |
| POST        | /api/v1/scenarios/{scenario_name}/customs          |
| POST        | /api/v1/scenarios/{scenario_name}/projects         |
| POST        | /api/v1/scenarios/{scenario_name}/scores           |
| POST        | /api/v1/scenarios/{scenario_name}/trust_indicators |
| DELETE      | /api/v1/scenarios/{scenario_name}/customs          |
| DELETE      | /api/v1/scenarios/{scenario_name}/projects         |
| DELETE      | /api/v1/scenarios/{scenario_name}/versions         |
| DELETE      | /api/v1/scenarios/{scenario_name}/installers       |
| PUT         | /api/v1/scenarios/{scenario_name}                  |
| PUT         | /api/v1/scenarios/{scenario_name}/installers       |
| PUT         | /api/v1/scenarios/{scenario_name}/versions         |
| PUT         | /api/v1/scenarios/{scenario_name}/projects         |
| PUT         | /api/v1/scenarios/{scenario_name}/customs          |
| POST        | /api/v1/results                                    |
| GET         | /api/v1/results                                    |
| GET         | /api/v1/results/{result_id}                        |
| POST        | /api/v1/deployresults                              |
| GET         | /api/v1/deployresults                              |
| GET         | /api/v1/deployresults/{result_id}                  |
| GET         | /metrics                                           |
