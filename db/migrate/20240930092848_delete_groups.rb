# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

# frozen_string_literal: true

class DeleteGroups < ActiveRecord::Migration[7.1]
  def change
    remove_reference :results, :group, index: true, foreign_key: true
    drop_table :groups, if_exist: true
  end
end
