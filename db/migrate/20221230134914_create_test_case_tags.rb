# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

# frozen_string_literal: true

class CreateTestCaseTags < ActiveRecord::Migration[7.0]
  def change
    create_table :test_case_tags do |t|
      t.string :value

      t.timestamps
    end
  end
end
