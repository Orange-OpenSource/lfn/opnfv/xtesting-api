# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

# frozen_string_literal: true

class AddProjectToInstallerVersionProjects < ActiveRecord::Migration[7.0]
  def change
    add_reference :installer_version_projects, :project, null: false, foreign_key: true
  end
end
