# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

# frozen_string_literal: true

class AddTagToTestCaseTags < ActiveRecord::Migration[7.1]
  def change
    add_column :test_case_tags, :tag_id, :integer

    TestCaseTag.all.each do |test_case_tag|
      test_case_tag.update(tag: Tag.find_by(value: test_case_tag.value))
      test_case_tag.save
    end

    add_foreign_key :test_case_tags, :tags
    remove_column :test_case_tags, :value, :string
  end
end
