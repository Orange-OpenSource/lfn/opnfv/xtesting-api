# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

# frozen_string_literal: true

class CreateTestCases < ActiveRecord::Migration[7.0]
  def change
    create_table :test_cases do |t|
      t.string :run
      t.references :project, null: false, foreign_key: true
      t.string :name
      t.string :ci_loop
      t.string :url
      t.string :catalog_description
      t.string :tier
      t.string :version
      t.string :criteria
      t.string :trust
      t.string :blocking
      t.string :description

      t.timestamps
    end
  end
end
