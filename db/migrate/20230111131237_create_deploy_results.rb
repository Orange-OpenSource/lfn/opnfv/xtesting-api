# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

# frozen_string_literal: true

class CreateDeployResults < ActiveRecord::Migration[7.0]
  def change
    create_table :deploy_results do |t|
      t.string :build_id
      t.datetime :stop_date
      t.string :job_name
      t.string :upstream_job_name
      t.references :installer_version, null: false, foreign_key: true
      t.references :pod, null: false, foreign_key: true
      t.string :criteria
      t.string :upstream_build_id
      t.datetime :start_date
      t.string :details

      t.timestamps
    end
  end
end
