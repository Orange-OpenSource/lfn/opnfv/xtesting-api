# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

# frozen_string_literal: true

class CreateAddTestCaseToTestCaseDependencies < ActiveRecord::Migration[7.0]
  def change
    create_table :add_test_case_to_test_case_dependencies do |t|
      t.references :test_case, null: false, foreign_key: true

      t.timestamps
    end
  end
end
