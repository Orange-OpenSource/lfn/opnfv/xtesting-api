# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.1].define(version: 2024_09_30_092848) do
  create_table "add_test_case_to_test_case_dependencies", force: :cascade do |t|
    t.integer "test_case_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["test_case_id"], name: "index_add_test_case_to_test_case_dependencies_on_test_case_id"
  end

  create_table "deploy_results", force: :cascade do |t|
    t.string "build_id"
    t.datetime "stop_date"
    t.string "job_name"
    t.string "upstream_job_name"
    t.integer "installer_version_id", null: false
    t.integer "pod_id", null: false
    t.string "criteria"
    t.string "upstream_build_id"
    t.datetime "start_date"
    t.string "details"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["installer_version_id"], name: "index_deploy_results_on_installer_version_id"
    t.index ["pod_id"], name: "index_deploy_results_on_pod_id"
  end

  create_table "installer_version_projects", force: :cascade do |t|
    t.integer "installer_version_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "project_id", null: false
    t.index ["installer_version_id"], name: "index_installer_version_projects_on_installer_version_id"
    t.index ["project_id"], name: "index_installer_version_projects_on_project_id"
  end

  create_table "installer_versions", force: :cascade do |t|
    t.string "owner"
    t.string "version"
    t.integer "installer_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["installer_id"], name: "index_installer_versions_on_installer_id"
  end

  create_table "installers", force: :cascade do |t|
    t.string "installer"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "scenario_id", null: false
    t.index ["scenario_id"], name: "index_installers_on_scenario_id"
  end

  create_table "ivp_customs", force: :cascade do |t|
    t.string "value"
    t.integer "installer_version_project_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["installer_version_project_id"], name: "index_ivp_customs_on_installer_version_project_id"
  end

  create_table "ivp_scores", force: :cascade do |t|
    t.integer "installer_version_project_id", null: false
    t.string "score"
    t.date "date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["installer_version_project_id"], name: "index_ivp_scores_on_installer_version_project_id"
  end

  create_table "pods", force: :cascade do |t|
    t.string "role"
    t.string "name"
    t.integer "user_id"
    t.string "details"
    t.string "mode"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_pods_on_user_id"
  end

  create_table "projects", force: :cascade do |t|
    t.integer "user_id"
    t.string "description"
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_projects_on_user_id"
  end

  create_table "result_tags", force: :cascade do |t|
    t.bigint "tag_id", null: false
    t.bigint "result_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["result_id"], name: "index_result_tags_on_result_id"
    t.index ["tag_id"], name: "index_result_tags_on_tag_id"
  end

  create_table "results", force: :cascade do |t|
    t.string "criteria"
    t.datetime "stop_date"
    t.string "build_tag"
    t.datetime "start_date"
    t.integer "pod_id", null: false
    t.integer "test_case_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "installer_version_project_id", null: false
    t.json "details"
    t.index ["installer_version_project_id"], name: "index_results_on_installer_version_project_id"
    t.index ["pod_id"], name: "index_results_on_pod_id"
    t.index ["test_case_id"], name: "index_results_on_test_case_id"
  end

  create_table "scenarios", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "user_id"
    t.index ["user_id"], name: "index_scenarios_on_user_id"
  end

  create_table "tags", force: :cascade do |t|
    t.string "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "test_case_dependencies", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "test_case_id", null: false
    t.index ["test_case_id"], name: "index_test_case_dependencies_on_test_case_id"
  end

  create_table "test_case_domains", force: :cascade do |t|
    t.string "name"
    t.integer "test_case_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["test_case_id"], name: "index_test_case_domains_on_test_case_id"
  end

  create_table "test_case_tags", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "test_case_id", null: false
    t.integer "tag_id"
    t.index ["test_case_id"], name: "index_test_case_tags_on_test_case_id"
  end

  create_table "test_cases", force: :cascade do |t|
    t.string "run"
    t.integer "project_id", null: false
    t.string "name"
    t.string "ci_loop"
    t.string "url"
    t.string "catalog_description"
    t.string "tier"
    t.string "version"
    t.string "criteria"
    t.string "trust"
    t.string "blocking"
    t.string "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["project_id"], name: "index_test_cases_on_project_id"
  end

  create_table "trust_indicators", force: :cascade do |t|
    t.date "date"
    t.string "status"
    t.integer "installer_version_project_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["installer_version_project_id"], name: "index_trust_indicators_on_installer_version_project_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "add_test_case_to_test_case_dependencies", "test_cases"
  add_foreign_key "deploy_results", "installer_versions"
  add_foreign_key "deploy_results", "pods"
  add_foreign_key "installer_version_projects", "installer_versions"
  add_foreign_key "installer_version_projects", "projects"
  add_foreign_key "installer_versions", "installers"
  add_foreign_key "installers", "scenarios"
  add_foreign_key "ivp_customs", "installer_version_projects"
  add_foreign_key "ivp_scores", "installer_version_projects"
  add_foreign_key "pods", "users"
  add_foreign_key "projects", "users"
  add_foreign_key "result_tags", "results"
  add_foreign_key "result_tags", "tags"
  add_foreign_key "results", "installer_version_projects"
  add_foreign_key "results", "pods"
  add_foreign_key "results", "test_cases"
  add_foreign_key "scenarios", "users"
  add_foreign_key "test_case_dependencies", "test_cases"
  add_foreign_key "test_case_domains", "test_cases"
  add_foreign_key "test_case_tags", "tags"
  add_foreign_key "test_case_tags", "test_cases"
  add_foreign_key "test_cases", "projects"
  add_foreign_key "trust_indicators", "installer_version_projects"
end
