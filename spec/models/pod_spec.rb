# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Pod, type: :model do
  context 'on creation' do
    it 'has a unique name' do
      create :pod, name: 'unique_pod_name'

      pod = build :pod, name: 'unique_pod_name'
      expect(pod).not_to be_valid
    end

    it 'rejects invalid JSON' do
      pod = build :pod, details: 'definitely not a JSON'
      expect(pod).not_to be_valid
    end
  end
end
