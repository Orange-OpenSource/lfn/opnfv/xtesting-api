# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

require 'rails_helper'

RSpec.describe 'Installer' do
  let(:scenario) { create :scenario }
  let(:installer_name) { create(:installer, scenario: scenario).installer }

  it 'name is unique per scenario' do
    expect(build :installer, installer: installer_name, scenario: scenario).not_to be_valid
  end

  it 'name is not checked across scenarios' do
    expect(build :installer, installer: installer_name).to be_valid
  end
end
