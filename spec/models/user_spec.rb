# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

# frozen_string_literal: true

require 'rails_helper'

RSpec.describe User, type: :model do
  context 'on creation' do
    it 'needs a name' do
      user = build :user, name: nil
      expect(user).not_to be_valid
    end

    it 'has a unique name' do
      create :user, name: 'unique_name'

      user = build :user, name: 'unique_name'
      expect(user).not_to be_valid
    end
  end
end
