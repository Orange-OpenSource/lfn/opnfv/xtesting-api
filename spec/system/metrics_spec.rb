# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

RSpec.describe 'metrics' do
  before do
    driven_by(:rack_test)
  end

  it 'shows project count' do
    project = create :project
    project = create :project
    project = create :project
    test_case = create :test_case, project: project
    create :result, test_case: test_case, criteria: "PASS"
    create :result, test_case: test_case, criteria: "FAIL"

    visit '/metrics'

    expect(page).to have_text 'xtesting_projects 3.0'
    expect(page).to have_text 'xtesting_results'
    expect(page).to have_text 'xtesting_results_pass'
    expect(page).to have_text 'xtesting_results_duration'
  end
end
