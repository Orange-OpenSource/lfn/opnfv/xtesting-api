# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

RSpec.describe 'Show Result' do
  before do
    driven_by(:rack_test)
  end

  it 'shows info from the result' do
    result = create :result, build_tag: 'orange-nrf'

    page.driver.header 'Authorization', "Bearer #{encode(['admin'])}"
    visit "/results/#{result.id}"

    expect(page).to have_text(result.id)
    expect(page).to have_text(result.pod_name)
    expect(page).to have_text(result.project_name)
    expect(page).to have_text(result.case_name)
    expect(page).to have_text(result.installer_name)
    expect(page).to have_text(result.version)
    expect(page).to have_text(result.scenario_name)
    expect(page).to have_text(result.build_tag)
    expect(page).to have_text(result.criteria)
    expect(page).not_to have_text('Links')
  end

  context 'with links in the details' do
    it 'shows links for the result' do
      result = create :result, details: { "links": ["/opt", "/home"] }, build_tag: 'xtesting-tests'

      page.driver.header 'Authorization', "Bearer #{encode(['admin'])}"
      visit "/results/#{result.id}"

      expect(page).to have_text('Links')
      expect(page).to have_text('/opt')
      expect(page).to have_text('/home')
      expect(page).to have_link('/opt', href: '/opt')
      expect(page).to have_link('/home', href: '/home')
    end
  end
end
