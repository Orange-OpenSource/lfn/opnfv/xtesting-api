# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

RSpec.describe 'RBAC' do
  before do
    driven_by(:rack_test)
    allow(Rails.application.config).to receive(:rbac_enabled).and_return true
    allow(Rails.application.config).to receive(:jwt_secret_key).and_return 'my_awesome_secret_key'

  end
  describe 'result tags are used to filter results' do
    describe 'when the user has at least one common group' do
      it 'shows only the matching results' do
        
        page.driver.header 'Authorization', "Bearer #{encode(['orange-4g-skc-read-write'])}"

        create :result, build_tag: 'orange-5g-skc'
        create :result, build_tag: 'orange-4g-skc'

        visit '/results'

        expect(page).not_to have_text 'orange-5g-skc'
        expect(page).to have_text 'orange-4g-skc'
      end
    end
  end
end