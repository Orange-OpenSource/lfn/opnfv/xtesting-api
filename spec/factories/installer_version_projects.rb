# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

# frozen_string_literal: true

FactoryBot.define do
  factory :installer_version_project do
    project
    installer_version

    trait :with_custom do
      after :create do |project, _|
        create :ivp_custom, installer_version_project: project
      end
    end
  end
end
