# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

FactoryBot.define do
  factory :result do
    pod
    test_case
    installer_version_project {
      create :installer_version_project, project: test_case.project
    }
    criteria { "MyString" }
    stop_date { Date.today }
    build_tag { "MyString" }
    start_date { Date.today }
    details { "MyString" }
  end
end
