# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

# frozen_string_literal: true

FactoryBot.define do
  factory :trust_indicator do
    date { '2022-12-29' }
    status { 'MyString' }
    installer_version_project { nil }
  end
end
