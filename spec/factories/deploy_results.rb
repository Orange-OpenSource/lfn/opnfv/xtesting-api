# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

FactoryBot.define do
  factory :deploy_result do
    build_id { "MyString" }
    stop_date { "2023-01-11 14:12:38" }
    job_name { "MyString" }
    upstream_job_name { "MyString" }
    installer_version
    pod
    criteria { "MyString" }
    upstream_build_id { "MyString" }
    start_date { "2023-01-11 14:12:38" }
    details { "MyString" }
  end
end
