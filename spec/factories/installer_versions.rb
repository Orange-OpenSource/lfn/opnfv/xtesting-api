# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

# frozen_string_literal: true

FactoryBot.define do
  factory :installer_version do
    owner { 'MyString' }
    version { 'MyString' }
    installer

    trait :with_project do
      after :create do |version, _|
        create :installer_version_project, installer_version: version
      end
    end
  end
end
