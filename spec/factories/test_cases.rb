# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

# frozen_string_literal: true

FactoryBot.define do
  factory :test_case do
    project
    run { 'MyString' }
    sequence(:name) { |n| "test_case_name_#{'%03d' % n}" }
    ci_loop { 'MyString' }
    url { 'MyString' }
    catalog_description { 'MyString' }
    tier { 'MyString' }
    version { 'MyString' }
    criteria { 'MyString' }
    trust { 'MyString' }
    blocking { 'MyString' }
    description { 'MyString' }
  end
end
