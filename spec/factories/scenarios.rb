# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

# frozen_string_literal: true

FactoryBot.define do
  factory :scenario do
    sequence(:name) { |n| "scenario_name_#{n}" }

    trait :with_installer do
      after :create do |scenario, evaluator|
        create :installer, scenario: scenario
      end
    end
  end
end
