# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

module API
  module V1
    module Params
      def self.pod
        {
          mode: SecureRandom.uuid,
          role: SecureRandom.uuid,
          details: '{}',
          name: SecureRandom.uuid
        }
      end

      def self.project(attrs = {})
        name = attrs.fetch(:name) { "project_#{SecureRandom.uuid}" }
        {
          name: name,
          description: SecureRandom.uuid
        }
      end

      def self.scenario(attrs = {})
        installers = attrs.fetch(:installers) { 3 }
        {
          name: "scenario_#{SecureRandom.uuid}",
          installers: (0...installers).map { installer(attrs) }
        }
      end

      def self.installer(attrs = {})
        versions = attrs.fetch(:versions) { 3 }
        {
          installer: "installer_#{SecureRandom.uuid}",
          versions: (0...versions).map { version(attrs) }
        }
      end

      def self.version(attrs = {})
        projects = attrs.fetch(:projects) { 3 }
        project_name = attrs.fetch(:project_name) { nil }
        version_params = {
          version: "version_#{SecureRandom.uuid}",
          owner: SecureRandom.uuid
        }
        if project_name
          version_params.update({
            projects: (0...projects).map { version_project(project: project_name) }
          })
        end
        version_params
      end

      def self.version_project(attrs = {})
        trust_indicators = attrs.fetch(:trust_indicators) { 3 }
        customs = attrs.fetch(:customs) { 3 }
        scores = attrs.fetch(:scores) { 3 }
        project = attrs.fetch(:project) { throw :no_project_names}
        {
          project: project,
          trust_indicators: (0...3).map { trust_indicator },
          customs: (0...3).map { custom },
          scores: (0...3).map { score }
        }
      end

      def self.trust_indicator
        {
          status: SecureRandom.uuid,
          date: DateTime.now.to_s
        }
      end

      def self.custom
        SecureRandom.uuid.to_s
      end

      def self.score
        {
          score: SecureRandom.uuid,
          date: DateTime.now.to_s
        }
      end

      def self.test_case
        {
          run: SecureRandom.uuid,
          name: SecureRandom.uuid,
          ci_loop: SecureRandom.uuid,
          url: SecureRandom.uuid,
          catalog_description: SecureRandom.uuid,
          tier: SecureRandom.uuid,
          version: SecureRandom.uuid,
          criteria: SecureRandom.uuid,
          trust: SecureRandom.uuid,
          blocking: SecureRandom.uuid,
          description: SecureRandom.uuid
        }
      end

      def self.test_result(attrs = {})
        {
          project_name: attrs.fetch(:project_name) { SecureRandom.uuid },
          scenario: attrs.fetch(:scenario) { SecureRandom.uuid },
          stop_date: DateTime.now.to_s,
          case_name: attrs.fetch(:case_name) { SecureRandom.uuid },
          build_tag: attrs.fetch(:build_tag) { SecureRandom.uuid },
          version: attrs.fetch(:version) { SecureRandom.uuid },
          pod_name: attrs.fetch(:pod_name) { SecureRandom.uuid },
          criteria: SecureRandom.uuid,
          installer: attrs.fetch(:installer) { SecureRandom.uuid },
          start_date: DateTime.now.to_s,
          details: {}
        }
      end

      def self.deploy_result(attrs = {})
        {
          build_id: SecureRandom.uuid,
          scenario: attrs.fetch(:scenario) { SecureRandom.uuid },
          stop_date: DateTime.now.to_s,
          job_name: SecureRandom.uuid,
          upstream_job_name: SecureRandom.uuid,
          version: attrs.fetch(:version) { SecureRandom.uuid },
          pod_name: attrs.fetch(:pod_name) { SecureRandom.uuid },
          criteria: SecureRandom.uuid,
          installer: attrs.fetch(:installer) { SecureRandom.uuid },
          upstream_build_id: SecureRandom.uuid,
          start_date: DateTime.now.to_s,
          details: {}
        }
      end
    end
  end
end
