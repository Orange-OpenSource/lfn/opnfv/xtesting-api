# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

RSpec.describe 'XTesting metrics' do

  context 'with some result cases' do
    let(:pod) { create :pod }
    let(:project) { create :project }
    let(:installer_version_project) { create :installer_version_project, project: project }
    let(:test_case) { create :test_case, project: project }
    let(:failed_result) { create :result, criteria: "FAIL", installer_version_project: installer_version_project, pod: pod, test_case: test_case, start_date: '2023-01-01', stop_date: '2023-01-02' }
    let(:passed_result) { create :result, criteria: "PASS", installer_version_project: installer_version_project, pod: pod, test_case: test_case, start_date: '2023-01-01', stop_date: '2023-01-02' }

    it 'contains projects count' do
      get('/metrics')
      expect(response.body).to include('xtesting_projects')
    end

    it 'contains passed test results' do
      passed_result
      get('/metrics')
      expect(response.body).to include('# TYPE xtesting_results_pass gauge')
      expect(response.body).to include(%Q(xtesting_results{pod_name="#{passed_result.pod.name}",project_name="#{project.name}",scenario="#{passed_result.scenario_name}",case_name="#{test_case.name}",build_tag="#{passed_result.build_tag}",installer="#{passed_result.installer_name}"} 1.0))

    end

    it 'contains metrics for test results' do
      failed_result
      passed_result
      get('/metrics')
      expect(response.body).to include('# TYPE xtesting_results gauge')
      expect(response.body).to include(%Q(xtesting_results{pod_name="#{failed_result.pod.name}",project_name="#{project.name}",scenario="#{failed_result.scenario_name}",case_name="#{test_case.name}",build_tag="#{failed_result.build_tag}",installer="#{failed_result.installer_name}"} 2.0))
    end

    it 'contains the last duration for test results' do
      failed_result
      get '/metrics'
      expect(response.body).to include('# TYPE xtesting_results_duration gauge')
      expect(response.body).to include(%Q(xtesting_results_duration{pod_name="#{failed_result.pod.name}",project_name="#{project.name}",scenario="#{failed_result.scenario_name}",case_name="#{test_case.name}",build_tag="#{failed_result.build_tag}",installer="#{failed_result.installer_name}"} 86400.0))
    end
  end
end
