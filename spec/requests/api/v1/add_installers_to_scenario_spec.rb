# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

require 'rails_helper'

RSpec.describe 'Add Installers To scenario' do
  describe 'POST /api/v1/scenarios/:name/installers' do
    let(:scenario) { create :scenario }
    let(:installers_params) { (0...4).map { API::V1::Params.installer } }
    before do
      post "/api/v1/scenarios/#{scenario.name}/installers", params: installers_params, as: :json
    end
    it 'renders http created' do
      expect(response).to have_http_status(:created)
    end
    it 'adds the installers to the scenario' do
      expect(scenario.reload.installers.size).to eq 4
    end
  end
end
