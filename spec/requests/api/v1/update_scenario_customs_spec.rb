# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

RSpec.describe 'Update Scenario Customs' do
  describe 'PUT /api/v1/scenarios/:scenario_name/customs?installer=installer_installer&version=version_version&project=project_project' do
    let(:scenario) { create :scenario }
    let(:project) { create :project }
    let(:installer) { create :installer, scenario: scenario }
    let(:version) { create :installer_version, installer: installer }
    let(:installer_version_project) { create :installer_version_project, installer_version: version, project: project }

    let(:new_customs_params) { (0...3).map { API::V1::Params.custom } }

    before do
      installer_version_project
      put "/api/v1/scenarios/#{scenario.name}/customs?installer=#{installer.installer}&version=#{version.version}&project=#{project.name}",
        params: new_customs_params,
        as: :json
    end

    it 'updates the scenario projects' do
      expect(installer_version_project.reload.customs.size).to eq 3
    end

    it 'destroys the old scenario projects' do
      expect(IVPCustom.count).to eq 3
    end
  end
end
