# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

# frozen_string_literal: true

RSpec.describe 'Create Project', type: :request do
  describe 'POST /api/v1/projects' do
    context 'with the required name' do
      let(:project_name) { 'create_project_spec_name' }
      before do
        post '/api/v1/projects', params: API::V1::Params.project(name: project_name)
      end
      it 'returns http success' do
        expect(response).to have_http_status(:success)
      end

      it 'creates the project' do
        expect(Project.count).to eq 1
      end

      it 'includes the project url in the response' do
        expect(JSON.parse(response.body)['href']).to include(project_name)
      end
    end
  end
end
