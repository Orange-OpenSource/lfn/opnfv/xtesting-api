# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

RSpec.describe 'List Deploy Results Spec' do
  describe 'GET /api/v1/deployresults' do
    before do
      get '/api/v1/deployresults'
    end
    it 'renders pagination' do
      expect(JSON.parse(response.body)).to have_key "pagination"
    end
    it 'renders deployresults' do
      expect(JSON.parse(response.body)["deployresults"]).to respond_to :size
    end
  end
end
