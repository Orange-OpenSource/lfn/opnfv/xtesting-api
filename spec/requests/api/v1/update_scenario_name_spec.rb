# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

RSpec.describe 'Update Scenario Name' do
  describe 'PUT /api/v1/scenarios/:scenario_name' do
    let(:scenario) { create :scenario }
    let(:new_scenario_name) { 'new_scenario_name' }
    before do
      put "/api/v1/scenarios/#{scenario.name}", params: { name: new_scenario_name }
    end
    it 'updates the name of the targeted scenario' do
      expect(scenario.reload.name).to eq new_scenario_name
    end
  end
end
