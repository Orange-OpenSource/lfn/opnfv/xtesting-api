# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

# frozen_string_literal: true
#
require 'rails_helper'

RSpec.describe 'Scenario Creation feature' do
  describe 'POST /api/v1/scenarios' do
    context 'with all parameters' do
      let(:project) { create :project }
      let(:scenario_params) do API::V1::Params.scenario installers: 3, project_name: project.name end

      before do
        post '/api/v1/scenarios', params: scenario_params
      end

      it 'renders http success' do
        expect(response).to have_http_status(:success)
      end

      it 'creates a scenario' do
        expect(Scenario.count).to eq 1
      end

      it 'returns the scenario url' do
        expect(JSON.parse(response.body)).to have_key('href')
      end

      it 'builds the installers, version, projects, trust_indicators, customs and scores' do
        expect(Installer.count).to eq 3
        expect(InstallerVersion.count).to eq 9
        expect(InstallerVersionProject.count).to eq 27
        expect(IVPScore.count).to eq 81
        expect(IVPCustom.count).to eq 81
        expect(TrustIndicator.count).to eq 81
      end
    end
  end
end
