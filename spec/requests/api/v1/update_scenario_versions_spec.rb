# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

RSpec.describe 'Update Scenario Versions' do
  describe 'PUT /api/v1/scenarios/:scenario_name/versions?installer=installer_installer' do
    let(:scenario) { create :scenario }
    let(:installer) { create :installer, :with_version, scenario: scenario }

    let(:new_versions_params) { (0...3).map { API::V1::Params.version } }

    before do 
      put "/api/v1/scenarios/#{scenario.name}/versions?installer=#{installer.installer}",
        params: new_versions_params,
        as: :json
    end

    it 'updates the scenario installer versions' do
      expect(installer.reload.versions.size).to eq 3
    end

    it 'destroys the old scenario versions' do
      expect(InstallerVersion.count).to eq 3
    end
  end
end
