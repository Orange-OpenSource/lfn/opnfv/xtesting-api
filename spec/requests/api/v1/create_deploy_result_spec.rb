# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

RSpec.describe 'Create Deploy Result Spec' do
  describe 'POST /api/v1/deployresults' do
    let(:pod) { create :pod }
    let(:scenario) { create :scenario }
    let(:installer) { create :installer, scenario: scenario }
    let(:version) { create :installer_version, installer: installer }
    before do
      post '/api/v1/deployresults',
        params: API::V1::Params.deploy_result(
          pod_name: pod.name,
          scenario: scenario.name,
          installer: installer.installer,
          version: version.version,
        )
    end

    it 'renders http created' do
      expect(response).to have_http_status(:created), response.body
    end

    it 'creates the deploy_result' do
      expect(DeployResult.count).to eq 1
    end
  end
end
