# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Test Cases Feature' do
  let(:project) { create :project }
  describe 'POST /api/v1/projects/:name/cases' do
    let(:test_case_params) do
      API::V1::Params.test_case
    end
    before do
      post "/api/v1/projects/#{project.name}/cases", params: test_case_params
    end
    it 'creates the test case' do
      expect(TestCase.count).to eq 1
    end
    it 'returns http created' do
      expect(response).to have_http_status(:created)
    end

    context 'with tags' do
      let(:test_case_params) do
        {
          name: 'test_case',
          tags: %w[t1 t2]
        }
      end
      it 'creates the tags' do
        expect(TestCaseTag.count).to eq 2
      end
    end

    context 'with dependencies' do
      let(:test_case_params) do
        {
          name: 'test_case_with_dependencies',
          dependencies: %w[d1 d2]
        }
      end
      it 'creates the tags' do
        expect(TestCaseDependency.count).to eq 2
      end
    end

    context 'with domains' do
      let(:test_case_params) do
        {
          name: 'test_case_with_dependencies',
          domains: %w[d1 d2]
        }
      end
      it 'creates the domains' do
        expect(TestCaseDomain.count).to eq 2
      end
    end


    context 'when no name is provided' do
      let(:test_case_params) do
        {}
      end
      it 'renders an error' do
        expect(response).to have_http_status(:bad_request)
        expect(JSON.parse(response.body)).to have_key('name')
      end
    end
  end

  let(:rendered_attributes) {
    %w[project_name run description tags creation_date dependencies tier trust blocking name ci_loop url version criteria domains _id catalog_description]
  }

  describe 'GET /api/v1/projects/:project_name/cases/:name' do
    let(:project) { create :project }
    let(:test_case) { create :test_case, project: project }

    before do
      get "/api/v1/projects/#{project.name}/cases/#{test_case.name}"
    end

    it 'renders the test case' do
      expect(JSON.parse(response.body)['name']).to eq test_case.name
    end

    it 'renders http success' do
      expect(response).to have_http_status(:success)
    end

    it 'renders the test case attributes' do
      rendered_attributes.each do |attribute|
        expect(JSON.parse(response.body)).to have_key(attribute)
      end
    end
  end

  describe 'GET /api/v1/projects/:project_name/cases' do
    let(:project) { create :project }
    before do
      3.times { create :test_case, project: project }
      get "/api/v1/projects/#{project.name}/cases"
    end
    it 'renders the list of test cases under the project' do
      expect(JSON.parse(response.body)['testcases']).to respond_to :size
    end

    it 'renders the test cases attributes' do
      rendered_attributes.each do |attribute|
        expect(JSON.parse(response.body)['testcases'].first).to have_key(attribute)
      end
    end
  end
end
