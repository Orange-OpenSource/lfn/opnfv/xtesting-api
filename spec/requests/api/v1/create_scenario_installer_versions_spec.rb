# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

RSpec.describe 'Create Scenario Installer Version' do
  describe 'POST /api/v1/scenarios/:name/versions?installer=:installer_name' do
    context 'with project parameters' do
      let(:scenario) { create :scenario }
      let(:project) { create :project }
      let(:installer) { create :installer, scenario: scenario }
      let(:versions_params) { (0...4).map { API::V1::Params.version(project_name: project.name) } }

      before do
        post "/api/v1/scenarios/#{scenario.name}/versions?installer=#{installer.installer}",
          params: versions_params, as: :json
      end

      it 'renders http created' do
        expect(response).to have_http_status :created
      end

      it 'creates the versions, projects, trust_indicators, scores and customs' do
        expect(InstallerVersion.count).to eq 4
        expect(InstallerVersionProject.count).to eq 12
        expect(TrustIndicator.count).to eq 36
        expect(IVPScore.count).to eq 36
      end
    end
  end
end
