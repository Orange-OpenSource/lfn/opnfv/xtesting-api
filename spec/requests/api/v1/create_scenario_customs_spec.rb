# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

RSpec.describe 'Create Scenario Customized Test Cases' do
  describe 'POST /api/v1/scenarios/:scenario_name/customs?installer=installer_installer&version=version_version&project=project_project' do
    let(:scenario) { create :scenario }
    let(:project) { create :project }
    let(:installer) { create :installer, scenario: scenario }
    let(:version) { create :installer_version, installer: installer }
    let(:installer_version_project) { create :installer_version_project, installer_version: version, project: project }

    let(:customs_params) { (0...3).map { API::V1::Params.custom } }

    before do
      installer_version_project
      post "/api/v1/scenarios/#{scenario.name}/customs?installer=#{installer.installer}&version=#{version.version}&project=#{project.name}",
        params: customs_params,
        as: :json
    end

    it 'renders http created' do
      expect(response).to have_http_status :created
    end

    it 'creates the customs' do
      expect(IVPCustom.count).to eq 3
    end
  end
end
