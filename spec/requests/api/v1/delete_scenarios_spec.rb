# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

# frozen_string_literal: true

RSpec.describe 'delete scenarios feature', type: :request do
  context 'with a known scenario name' do
    let(:scenario) { create :scenario }
    it 'renders no content' do
      delete "/api/v1/scenarios/#{scenario.name}"
      expect(response).to have_http_status :no_content
    end

    it 'deletes the targeted scenario' do
      delete "/api/v1/scenarios/#{scenario.name}"
      expect(Scenario.count).to eq 0
    end
  end

  context 'with an unknown scenario name' do
    it 'renders not found' do
      delete '/api/v1/scenarios/unknown_scenario'
      expect(response).to have_http_status :not_found
    end
  end
end
