# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

RSpec.describe 'Create Scenario Installer Version Projects' do
  let(:scenario) { create :scenario }
  let(:project) { create :project }
  let(:installer) { create :installer, scenario: scenario }
  let(:version) { create :installer_version, installer: installer }
  let(:version_projects_params) do
    (0...3).map { API::V1::Params.version_project(project: project.name) }
  end

  describe 'POST /api/v1/scenarios/:scenario_name/projects?installer=:installer_installer&version=version_version' do
    before do
      post "/api/v1/scenarios/#{scenario.name}/projects?installer=#{installer.installer}&version=#{version.version}",
        params: version_projects_params, as: :json
    end

    it 'renders http created' do
      expect(response).to have_http_status(:created), response.body
    end

    it 'creates the projects, trust_indicators, scores and customs' do
      expect(InstallerVersionProject.count).to eq 3
      expect(TrustIndicator.count).to eq 9
      expect(IVPScore.count).to eq 9
      expect(IVPCustom.count).to eq 9
    end
  end
end
