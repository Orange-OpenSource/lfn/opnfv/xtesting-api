# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

RSpec.describe 'Update Scenario Projects' do
  describe 'PUT /api/v1/scenarios/:scenario_name/projects?installer=installer_installer&version=version_version' do
    let(:scenario) { create :scenario }
    let(:project) { create :project }
    let(:installer) { create :installer, scenario: scenario }
    let(:version) { create :installer_version, installer: installer }

    let(:new_project) { create :project }
    let(:new_projects_params) { 
      [ API::V1::Params.version_project(project: new_project.name) ]
    }

    before do 
      put "/api/v1/scenarios/#{scenario.name}/projects?installer=#{installer.installer}&version=#{version.version}",
        params: new_projects_params,
        as: :json
    end

    it 'updates the scenario projects' do
      expect(version.reload.projects.size).to eq 1
    end

    it 'destroys the old scenario projects' do
      expect(InstallerVersionProject.count).to eq 1
    end
  end
end
