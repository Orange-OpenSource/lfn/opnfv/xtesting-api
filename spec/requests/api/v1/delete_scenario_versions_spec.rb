# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

RSpec.describe 'Delete Scenario Version' do
  describe 'DELETE /api/v1/scenarios/:scenario_name/versions?installer=installer_installer' do
    let(:scenario) { create :scenario }
    let(:installer) { create :installer, scenario: scenario }
    let(:version) { create :installer_version, installer: installer }

    before do
      delete "/api/v1/scenarios/#{scenario.name}/versions?installer=#{installer.installer}",
        params: [version.version],
        as: :json
    end

    it 'renders http no content' do
      expect(response).to have_http_status :no_content
    end

    it 'destroys the version' do
      expect(InstallerVersion.count).to eq 0
    end
  end
end
