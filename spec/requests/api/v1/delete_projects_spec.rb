# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

# frozen_string_literal: true

RSpec.describe 'Projects API', type: :request do
  describe 'DELETE /api/v1/projects/:name' do
    context 'with a known name' do
      let(:project) { create :project }
      it 'returns http success' do
        delete "/api/v1/projects/#{project.name}"
        expect(response).to have_http_status(:success)
      end
      it 'destroys the project' do
        delete "/api/v1/projects/#{project.name}"
        expect(Project.count).to eq 0
      end
    end
    context 'with an unknown name' do
      it 'returns http not found' do
        delete '/api/v1/projects/unknown_project'
        expect(response).to have_http_status(:not_found)
      end
    end
  end
end
