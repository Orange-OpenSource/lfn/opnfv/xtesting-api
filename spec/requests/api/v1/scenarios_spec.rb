# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Scenarios API', type: :request do
  let(:rendered_attributes) {
    %w[_id creation_date name creator installers]
  }
  describe 'GET /api/v1/scenarios' do
    before do
      create :scenario
      get '/api/v1/scenarios'
    end
    it 'returns http success' do
      expect(response).to have_http_status(:success)
    end
    it 'returns the list of Scenarios' do
      expect(JSON.parse(response.body)['scenarios']).to respond_to :size
    end

    it 'renders the scenario attributes' do
      rendered_attributes.each do |attribute|
        expect(JSON.parse(response.body)['scenarios'].first).to have_key(attribute)
      end
    end
  end

  describe 'GET /api/v1/scenarios/:name' do
    context 'with an existing scenario name' do
      let(:scenario_name) { 'get_scenario_scenario_name' }
      before do
        create :scenario, name: scenario_name
        get "/api/v1/scenarios/#{scenario_name}"
      end
      it 'returns http success' do
        expect(response).to have_http_status(:success)
      end
      it 'renders the scenario as json' do
        expect(JSON.parse(response.body)['name']).to eq scenario_name
      end

      it 'renders the scenario attributes' do
        rendered_attributes.each do |attribute|
          expect(JSON.parse(response.body)).to have_key(attribute)
        end
      end
    end

    context 'with an unexpected scenario name' do
      it 'returns http not found' do
        get '/api/v1/scenarios/blablabla'
        expect(response).to have_http_status(:not_found)
      end
    end
  end
end
