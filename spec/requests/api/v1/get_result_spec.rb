# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

RSpec.describe 'GET Test Result Spec' do
  context 'as an admin' do
    describe 'GET /api/v1/results/:result_id' do
      let(:headers) {
        { "X-Auth-Request-Groups": ['admin'] }
      }
      let(:result) { create :result }
      before do
        get "/api/v1/results/#{result.id}", params: {}, headers: headers
      end
      it 'renders the result' do
        %w[criteria build_tag start_date stop_date].each do |attribute|
          expect(JSON.parse(response.body)).to have_key(attribute)
        end
      end
      it 'renders http success' do
        expect(response).to have_http_status :success
      end
    end
  end
  context 'as an unauthorized user' do
    let(:headers) {
      { "X-Auth-Request-Groups": ['unauthorized'] }
    }
    let(:result) { create :result, build_tag: 'private_result' }
    before do
      get "/api/v1/results/#{result.id}", params: {}, headers: headers
    end
    it 'does not render the result' do
      skip
      expect(response).to have_http_status :unauthorized
    end
  end
end
