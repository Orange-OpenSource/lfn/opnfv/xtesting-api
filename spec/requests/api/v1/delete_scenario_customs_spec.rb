# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

RSpec.describe 'Delete Scenario Custom' do
  describe 'DELETE /api/v1/scenarios/:scenario_name/customs?installer=installer_installer&version=version_version&project=project_project' do
    let(:scenario) { create :scenario }
    let(:project) { create :project }
    let(:installer) { create :installer, scenario: scenario }
    let(:version) { create :installer_version, installer: installer }
    let(:installer_version_project) { create :installer_version_project, installer_version: version, project: project }
    let(:custom) { create :ivp_custom, value: 'custom_value', installer_version_project: installer_version_project }

    before do
      custom
      delete "/api/v1/scenarios/#{scenario.name}/customs?installer=#{installer.installer}&version=#{version.version}&project=#{project.name}",
        params: ['custom_value'],
        as: :json
    end

    it 'renders http no content' do
      expect(response).to have_http_status :no_content
    end

    it 'destroys the custom' do
      expect(IVPCustom.count).to eq 0
    end
  end
end
