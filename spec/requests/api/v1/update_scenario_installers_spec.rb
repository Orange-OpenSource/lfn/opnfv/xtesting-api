# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

RSpec.describe 'Update Scenario Installers' do
  describe 'PUT /api/v1/scenarios/:scenario_name/installers' do
    let(:scenario) { create :scenario, :with_installer }

    let(:new_installers_params) { (0...3).map { API::V1::Params.installer } }

    before do
      put "/api/v1/scenarios/#{scenario.name}/installers", params: new_installers_params, as: :json
    end

    it 'updates the scenario installers' do
      expect(scenario.reload.installers.size).to eq 3
    end

    it 'destroys the old scenario installers' do
      expect(Installer.count).to eq 3
    end
  end
end
