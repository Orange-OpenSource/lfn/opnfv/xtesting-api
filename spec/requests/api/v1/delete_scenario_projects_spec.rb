# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

RSpec.describe 'Delete Scenario Project' do
  describe 'DELETE /api/v1/scenarios/:scenario_name/projects?installer=installer_installer&version=version_version' do
    let(:scenario) { create :scenario }
    let(:installer) { create :installer, scenario: scenario }
    let(:version) { create :installer_version, installer: installer }
    let(:project) { create :project }
    let(:ivp) { create :installer_version_project, installer_version: version, project: project }

    before do
      ivp
      delete "/api/v1/scenarios/#{scenario.name}/projects?installer=#{installer.installer}&version=#{version.version}",
        params: [project.name],
        as: :json
    end

    it 'renders http no content' do
      expect(response).to have_http_status :no_content
    end

    it 'destroys the installer version project' do
      expect(InstallerVersionProject.count).to eq 0
    end
  end
end
