# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

RSpec.describe 'Create Scenario Scores' do
  describe 'POST /api/v1/scenarios/:scenario_name/scores?installer=installer_installer&version=version_version&project=project_project' do
    let(:scenario) { create :scenario }
    let(:project) { create :project }
    let(:installer) { create :installer, scenario: scenario }
    let(:version) { create :installer_version, installer: installer }
    let(:ivp) { create :installer_version_project, installer_version: version, project: project }

    let(:scores_params) { (0...3).map { API::V1::Params.score } }

    before do
      ivp
      post "/api/v1/scenarios/#{scenario.name}/scores?installer=#{installer.installer}&version=#{version.version}&project=#{project.name}",
        params: scores_params,
        as: :json
    end

    it 'renders http created' do
      expect(response).to have_http_status :created
    end

    it 'creates the customs' do
      expect(IVPScore.count).to eq 3
    end
  end
end
