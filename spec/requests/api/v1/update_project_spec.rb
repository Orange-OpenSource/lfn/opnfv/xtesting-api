# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

# frozen_string_literal: true

RSpec.describe 'Update project' do
  describe 'PUT /api/v1/projects/:name' do
    context 'with an unknown name' do
      it 'returns http not found' do
        put '/api/v1/projects/unknown_project'
        expect(response).to have_http_status(:not_found)
      end
    end

    context 'with a known name' do
      let(:project) { create :project }
      let(:updated_project_description) { 'new_description' }
      before do
        put "/api/v1/projects/#{project.name}", params: { 'description': updated_project_description }
      end
      it 'returns http success' do
        expect(response).to have_http_status(:success)
      end

      it 'updates the relevant attributes' do
        expect(Project.find_by(name: project.name).description).to eq updated_project_description
      end

      it 'renders the project attributes' do
        %w[_id creation_date creator description name].each do |attribute|
          expect(JSON.parse(response.body)).to have_key(attribute)
        end
      end
    end
  end
end
