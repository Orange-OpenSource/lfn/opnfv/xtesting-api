# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Pods API', type: :request do
  let(:rendered_attributes) {
    %w[role name creator details mode _id creation_date]
  }
  describe 'GET /api/v1/pods' do
    before do
      create :pod
      get '/api/v1/pods'
    end

    it 'returns http success' do
      expect(response).to have_http_status(:success)
    end

    it 'returns the list of Pods' do
      expect(JSON.parse(response.body)['pods']).to respond_to :size
    end

    it 'renders pod attributes' do
      rendered_attributes.each do |attribute|
        expect(JSON.parse(response.body)['pods'].first).to have_key(attribute)
      end
    end
  end

  describe 'POST /api/v1/pods' do
    context 'with valid attributes for a pod' do
      before do
        post '/api/v1/pods', params: API::V1::Params.pod
      end

      it 'returns http success' do
        expect(response).to have_http_status(:success)
      end

      it 'creates a Pod' do
        expect(Pod.count).to eq 1
      end

      it 'returns the pod Url' do
        expect(JSON.parse(response.body)).to have_key('href')
      end
    end
  end

  describe 'GET /api/v1/pods/:pod_name' do
    context 'with a known pod_name' do
      let(:pod_name) { 'known_pod_name' }
      before do
        create :pod, name: pod_name
        get "/api/v1/pods/#{pod_name}"
      end

      it 'returns the pod' do
        expect(response).to have_http_status(:success)
        expect(JSON.parse(response.body)['name']).to eq pod_name
      end

      it 'renders pod attributes' do
        rendered_attributes.each do |attribute|
          expect(JSON.parse(response.body)).to have_key(attribute)
        end
      end

      it 'displays the relevant pod attributes' do
        %w[_id role name creator details mode creation_date].each do |attribute|
          expect(JSON.parse(response.body)).to have_key(attribute)
        end
      end
    end

    context 'with an unknown pod_name' do
      it 'returns 404' do
        get '/api/v1/pods/unknown_pod'
        expect(response).to have_http_status(:not_found)
      end
    end
  end
end
