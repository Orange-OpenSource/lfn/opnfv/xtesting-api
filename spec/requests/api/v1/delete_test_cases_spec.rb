# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

# frozen_string_literal: true

RSpec.describe 'delete test cases feature', type: :request do
  describe 'DELETE /api/v1/projects/:project_name/cases/:name' do
    let(:project) { create :project }
    context 'with a known name' do
      let(:test_case) { create :test_case, project: }
      it 'destroys the given test case' do
        delete "/api/v1/projects/#{project.name}/cases/#{test_case.name}"
        expect(TestCase.count).to eq 0
      end
      it 'renders http no_content' do
        delete "/api/v1/projects/#{project.name}/cases/#{test_case.name}"
        expect(response).to have_http_status :no_content
      end
    end

    context 'with an unknown test case name' do
      it 'renders http not_found' do
        delete "/api/v1/projects/#{project.name}/cases/unknown_test_case_name"
        expect(response).to have_http_status :not_found
      end
    end

    context 'with tags' do
      let(:test_case) { create :test_case, project: }
      it 'removes tags' do
        create :test_case_tag, test_case: test_case
        delete "/api/v1/projects/#{project.name}/cases/#{test_case.name}"
        expect(TestCaseTag.count).to eq(0)
      end
    end

    context 'with domains' do
      let(:test_case) { create :test_case, project: }
      it 'removes domains' do
        create :test_case_domain, test_case: test_case
        delete "/api/v1/projects/#{project.name}/cases/#{test_case.name}"
        expect(TestCaseDomain.count).to eq(0)
      end
    end

    context 'with dependencies' do
      let(:test_case) { create :test_case, project: }
      it 'removes domains' do
        create :test_case_dependency, test_case: test_case
        delete "/api/v1/projects/#{project.name}/cases/#{test_case.name}"
        expect(TestCaseDependency.count).to eq(0)
      end
    end
  end
end
