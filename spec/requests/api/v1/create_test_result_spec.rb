# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

RSpec.describe 'Create Test Case Result' do
  describe 'POST /api/v1/results' do
    let(:project) { create :project }
    let(:pod) { create :pod }
    let(:test_case) { create :test_case, project: project }

    context 'Scenario does exist' do
      let(:scenario) { create :scenario }
      let(:installer) { create :installer, scenario: scenario }
      let(:version) { create :installer_version, installer: installer }
      let(:installer_version_project) { create :installer_version_project, installer_version: version, project: project }

      let(:test_result_params) do
        API::V1::Params.test_result(
          pod_name: pod.name,
          scenario: scenario.name,
          installer: installer.installer,
          version: version.version,
          project_name: project.name,
          case_name: test_case.name,
          result: {
              details: {}
          },
          build_tag: 'result_tag-with-some-other-parameters'
        )
      end
      before do
        installer_version_project
        post(
          '/api/v1/results',
          params: test_result_params
        )
      end
      it 'renders http created' do
        expect(response).to have_http_status(:created), response.body
      end
      it 'creates the test result' do
        expect(Result.count).to eq 1
      end
      it 'sets up the pod' do
        expect(Result.first.pod).to eq pod
      end
      it 'sets up the case' do
        expect(Result.first.test_case).to eq test_case
      end
      it 'has href in the response body' do
        expect(JSON.parse(response.body)).to have_key('href')
      end
    end

    context 'Scenario does not exist' do
      let(:test_result_params) do
        API::V1::Params.test_result(
          pod_name: pod.name,
          scenario: "scenario_name",
          installer: "installer_installer",
          version: "version_version",
          project_name: project.name,
          case_name: test_case.name
        )
      end
      before do
        post(
          '/api/v1/results',
          params: test_result_params
        )
      end
      it 'renders http created' do
        expect(response).to have_http_status(:created), response.body
      end
      it 'creates the test result' do
        expect(Result.count).to eq 1
      end
      it 'sets up the pod' do
        expect(Result.first.pod).to eq pod
      end
      it 'sets up the case' do
        expect(Result.first.test_case).to eq test_case
      end
    end
  end

  context 'from real life params' do
    let(:project) { create :project, name: "oracle-cnc" }
    let(:pod) { create :pod, name: "occne-dev" }
    let(:test_case) { create :test_case, project: project, name: "nrf-config_GET_ocnrfConfigurations" }

    let(:test_result_params) {
      {
        "build_tag"=>"tag-test",
        "case_name"=>"nrf-config_GET_ocnrfConfigurations",
        "criteria"=>"PASS",
        "details"=>{
          "detail_1" => "value_1"
        },
        "installer"=>"unknown",
        "pod_name"=>"occne-dev",
        "project_name"=>"oracle-cnc",
        "scenario"=>"scenario-test",
        "start_date"=>"2023-02-28 08:04:11",
        "stop_date"=>"2023-02-28 08:04:11",
        "version"=>"real_life_version"
      }
    }

    before do
      pod
      project
      test_case
      post(
        '/api/v1/results',
        params: test_result_params
      )
    end
    it 'renders http created' do
        expect(response).to have_http_status(:created), response.body
    end
    it 'saves details' do
      expect(Result.last.details).to eq({"detail_1" => "value_1"})
    end
  end
end
