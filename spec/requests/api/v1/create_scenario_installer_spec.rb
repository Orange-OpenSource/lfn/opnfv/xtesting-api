# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

# frozen_string_literal: true

RSpec.describe 'Scenario Installer Creation feature' do
  describe 'POST /api/v1/scenarios/:scenario_name/installers' do
    let(:scenario) { create :scenario }
    let(:project) { create :project }
    context 'with all parameters' do
      let(:installer_count) { 4 }
      let(:version_count) { 2 }
      let(:installers_params) { (0...installer_count).map { API::V1::Params.installer(versions: version_count, project_name: project.name) } }
      before do
        post "/api/v1/scenarios/#{scenario.name}/installers", params: installers_params, as: :json
      end

      it 'creates the scenario installers' do
        expect(Installer.count).to eq installer_count
        expect(InstallerVersion.count).to eq installer_count * version_count
        expect(InstallerVersionProject.count).to eq installer_count * version_count * 3
        expect(IVPScore.count).to eq installer_count * version_count * 3 * 3
        expect(IVPCustom.count).to eq installer_count * version_count * 3 * 3
        expect(TrustIndicator.count).to eq installer_count * version_count * 3 * 3
      end
    end
  end
end
