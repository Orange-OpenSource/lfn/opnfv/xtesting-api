# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

RSpec.describe 'Get Results Feature' do
  describe 'GET /api/v1/results' do
    it 'renders pagination info' do
      get '/api/v1/results'
      expect(JSON.parse(response.body)).to have_key('pagination')
    end
    describe 'filters' do
      describe '?case=case_name' do
        it 'filters results by case_name' do
          test_case = create :test_case
          other_test_case = create :test_case
          pod = create :pod
          create :result, test_case: test_case, pod: pod
          create :result, test_case: other_test_case, pod: pod
          get "/api/v1/results?case=#{test_case.name}"
          expect(JSON.parse(response.body)['results'].size).to eq 1
        end
      end
      describe '?buildTag=build_tag' do
        it 'filters results by build_tag' do
          create :result, build_tag: 'one'
          create :result, build_tag: 'two'
          get '/api/v1/results?buildTag=two'
          expect(JSON.parse(response.body)['results'].size).to eq 1
        end
      end
      describe '?from=start_date' do
        it 'filters results by start_date' do
          Timecop.freeze(2022, 6, 1)
          create :result, start_date: DateTime.now
          Timecop.travel(2023, 1, 1)
          create :result, start_date: DateTime.now

          get '/api/v1/results?from=2022-07-01'
          expect(JSON.parse(response.body)['results'].size).to eq 1
        end
      end
      describe '?to=end_date' do
        it 'filters results by end_date' do
          Timecop.freeze(2022, 6, 1)
          create :result, stop_date: DateTime.now
          Timecop.travel(2023, 1, 1)
          create :result, stop_date: DateTime.now

          get '/api/v1/results?to=2022-07-01'
          expect(JSON.parse(response.body)['results'].size).to eq 1
        end
      end
      describe '?last=days' do
        it 'filters results by last days' do
          create :result, start_date: DateTime.now
          Timecop.travel(Date.today + 10)
          create :result, start_date: DateTime.now

          get '/api/v1/results?last=7'
          expect(JSON.parse(response.body)['results'].size).to eq 1
        end
      end
      describe '?scenario=scenario_name' do
        it 'filters results by scenario_name' do
          project = create :project
          scenario = create :scenario, name: 'target_scenario'
          installer = create :installer, scenario: scenario
          version = create :installer_version, installer: installer
          ivp = create :installer_version_project, installer_version: version, project: project

          test_case = create :test_case, project: project
          create :result, test_case: test_case, installer_version_project: ivp

          create :result

          get '/api/v1/results?scenario=target_scenario'
          expect(JSON.parse(response.body)['results'].size).to eq 1
        end
      end
      describe '?project=project_name' do
        it 'filters results by project_name' do
          project = create :project
          test_case = create :test_case, project: project

          create :result, test_case: test_case
          create :result

          get "/api/v1/results?project=#{project.name}"

          expect(JSON.parse(response.body)['results'].size).to eq 1
        end
      end
      describe '?criteria=criteria' do
        it 'filters results by criteria' do
          create :result, criteria: 'searched'
          create :result

          get "/api/v1/results?criteria=searched"

          expect(JSON.parse(response.body)['results'].size).to eq 1
        end
      end
      describe '?installer=installer_installer' do
        it 'filters results by installer' do
          installer = create :installer
          version = create :installer_version, installer: installer
          ivp = create :installer_version_project, installer_version: version
          create :result, installer_version_project: ivp
          create :result

          get "/api/v1/results?installer=#{installer.installer}"
          expect(JSON.parse(response.body)['results'].size).to eq 1
        end
      end
      describe '?pod=pod_name' do
        it 'filters results by pod' do
          pod = create :pod
          create :result, pod: pod
          create :result

          get "/api/v1/results?pod=#{pod.name}"
          expect(JSON.parse(response.body)['results'].size).to eq 1
        end
      end
      describe '?period=period' do
        skip
      end
      describe '?descend=descend' do
        skip
      end
      describe 'pagination' do
        describe 'size' do
          it 'pages have 20 results' do
            27.times do
              create :result
            end

            get "/api/v1/results"

            results = JSON.parse(response.body)['results']
            expect(results.size).to eq 20
          end
          it 'last page may have less than 20 results' do
            27.times do
              create :result
            end
            get "/api/v1/results?page=2"

            results = JSON.parse(response.body)['results']
            expect(results.size).to eq 7
          end
        end

        it 'displays the number of pages' do
          22.times do
            create :result
          end
          get "/api/v1/results"

          pagination = JSON.parse(response.body)['pagination']
          expect(pagination['total_pages']).to eq 2
        end
        it 'displays the current page' do
          22.times do
            create :result
          end
          get "/api/v1/results?page=2"

          pagination = JSON.parse(response.body)['pagination']
          expect(pagination['current_page']).to eq 2
        end
        it 'renders the first page by default' do
          22.times do
            create :result
          end
          get "/api/v1/results"

          pagination = JSON.parse(response.body)['pagination']
          expect(pagination['current_page']).to eq 1
        end

        it 'first page has the most recent results' do
          25.times do
            create :result
          end
          get "/api/v1/results"

          results = JSON.parse(response.body)['results']
          expect(results.first).to eq JSON.parse(Result.last.to_json)
        end
      end
    end
  end
end
