# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

RSpec.describe 'Update Scenario Owner' do
  describe 'PUT /api/v1/scenarios/:name/owner' do
    let(:scenario) { create :scenario }
    context 'with a new owner name' do
      let(:owner_name) { 'new_owner' }
      let(:owner_params) {
        { owner: owner_name }
      }
      before do
        put "/api/v1/scenarios/#{scenario.name}/owner", params: owner_params
      end

      it 'updates the owner' do
        expect(Scenario.find_by(name: scenario.name).user_name).to eq owner_name
      end

      it 'renders http_success' do
        expect(response).to have_http_status(:ok)
      end
    end
  end
end
