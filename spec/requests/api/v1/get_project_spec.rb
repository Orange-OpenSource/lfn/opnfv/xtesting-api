# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

# frozen_string_literal: true

RSpec.describe 'GET project' do
  let(:rendered_attributes) {
    %w[_id description creator name creation_date]
  }

  describe 'GET /api/v1/projects' do
    before do
      3.times { create :project }
      get '/api/v1/projects'
    end

    it 'returns http success' do
      expect(response).to have_http_status(:success)
    end

    it 'returns the list of projects' do
      expect(JSON.parse(response.body)["projects"].size).to eq 3
    end

    it 'projects have the relevant attributes' do
      rendered_attributes.each do |key|
        expect(JSON.parse(response.body)["projects"].first).to have_key(key)
      end
    end
  end

  describe 'GET /api/v1/projects/:name' do
    context 'with a known name' do
      let(:project) { create :project }
      before do
        get "/api/v1/projects/#{project.name}"
      end

      it 'returns http success' do
        expect(response).to have_http_status(:success)
      end

      it 'returns the project' do
        expect(JSON.parse(response.body)['name']).to eq project.name
      end

      it 'renders the project atributes' do
        rendered_attributes.each do |attribute|
          expect(JSON.parse(response.body)).to have_key(attribute)
        end
      end
    end

    context 'with an unknown name' do
      it 'returns http not found' do
        get '/api/v1/projects/unknown_project'
        expect(response).to have_http_status(:not_found)
      end
    end
  end
end
