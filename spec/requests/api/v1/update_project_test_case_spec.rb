# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

# frozen_string_literal: true

RSpec.describe 'Update Test Case Feature' do
  let(:project) { create :project }
  let(:test_case) { create :test_case, project: project }
  let(:tag) { create :tag }
  describe 'PUT /api/v1/projects/:project_name/cases/:case_name' do
    let(:test_case_params) do
      attributes_for :test_case
    end

    it 'returns http no_content' do
      put "/api/v1/projects/#{project.name}/cases/#{test_case.name}", params: test_case_params
      expect(response).to have_http_status(:no_content)
    end

    context 'with new tags' do
      let(:test_case_params) do
        {
          tags: %w[tag_001 tag_002 tag_003]
        }
      end
      it 'updates the tags' do
        put "/api/v1/projects/#{project.name}/cases/#{test_case.name}", params: test_case_params
        expect(Tag.count).to eq 3
        expect(TestCaseTag.count).to eq 3
      end
    end

    context 'when tags are already present' do
      let(:test_case_params) do
        {
          tags: %w[updated_tag]
        }
      end
      it 'deletes the old tags' do
        create :test_case_tag, test_case: test_case, tag: tag
        expect(TestCaseTag.count).to eq 1

        put "/api/v1/projects/#{project.name}/cases/#{test_case.name}", params: test_case_params

        expect(TestCaseTag.count).to eq 1
        expect(Tag.last.value).to eq 'updated_tag'
      end
    end

    context 'when dependencies are present' do
      let(:test_case_params) {
        {
          dependencies: %w[updated_dependency]
        }
      }
      it 'deletes the unused dependencies' do
        create :test_case_dependency, test_case: test_case, name: 'old_dependency'

        put "/api/v1/projects/#{project.name}/cases/#{test_case.name}", params: test_case_params

        expect(TestCaseDependency.count).to eq 1
        expect(TestCaseDependency.first.name).to eq 'updated_dependency'
      end
    end

    context 'when domains are present' do
      let(:test_case_params) {
        {
          domains: %w[updated_domain]
        }
      }
      it 'deletes the unused dependencies' do
        create :test_case_domain, test_case: test_case, name: 'old_domain'

        put "/api/v1/projects/#{project.name}/cases/#{test_case.name}", params: test_case_params

        expect(TestCaseDomain.count).to eq 1
        expect(TestCaseDomain.first.name).to eq 'updated_domain'
      end
    end
  end
end
