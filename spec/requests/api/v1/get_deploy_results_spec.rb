# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

RSpec.describe 'Read Deploy Result Spec' do
  describe 'GET /api/v1/deployresults/:deploy_result_id' do
    let(:deploy_result) { create :deploy_result }

    before do
      get "/api/v1/deployresults/#{deploy_result.id}"
    end

    it 'renders http success' do
      expect(response).to have_http_status :success
    end

    it 'renders the deploy result' do
      %w[criteria details].each do |attribute|
        expect(JSON.parse(response.body)).to have_key(attribute)
      end
    end
  end
end
