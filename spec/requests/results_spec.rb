# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

RSpec.describe 'Results Index' do
  context 'when no test case tag exist' do
    before do
      create_list :result, 4
    end
    it 'still renders results' do
      get '/results'
      
      expect(response.parsed_body.css('tr.result-row').count).to eq 4
    end
  end

  context 'when filtering by test case tags' do
    before do
      tag = create :tag
      test_case = create :test_case
      create :test_case_tag, { tag:, test_case: }
      create :result, { test_case: }
      create :result
    end
    it 'filters results' do
      get '/results', params: { tags: [Tag.first.value] }
      expect(response.parsed_body.css('tr.result-row').count).to eq 1
    end
  end
end