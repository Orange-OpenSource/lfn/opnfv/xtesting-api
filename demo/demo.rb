# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

# frozen_string_literal: true

require 'yaml'
require 'json'
require 'open3'

api = ENV.fetch('XTESTING_URL') { raise 'XTESTING_URL ENV MISSING' }
socks_proxy = ENV.fetch('SOCKS5_PROXY')

p "API: #{api}"
p "SOCKS PROXY : #{socks_proxy}"

file = ARGV[0]

p "Running: #{file}"

data = YAML.load_file(file)

data['requests'].each do |request|
  command = 'curl'
  command << " --socks5 #{socks_proxy}"
  command << " -X #{request['method']} #{api}#{request['path']} -H 'Content-Type: application/json'"
  command << " --data #{request['params'].to_json}" if request['params']
  p command
end
