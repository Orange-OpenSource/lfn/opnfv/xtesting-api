## [1.15.34](https://gitlab.com/Orange-OpenSource/lfn/opnfv/xtesting-api/compare/1.15.33...1.15.34) (2025-03-12)


### Bug Fixes

* **deps:** update ruby packages ([59a992c](https://gitlab.com/Orange-OpenSource/lfn/opnfv/xtesting-api/commit/59a992c032084ba3163ee04db318ae42a3926453))

## [1.15.33](https://gitlab.com/Orange-OpenSource/lfn/opnfv/xtesting-api/compare/1.15.32...1.15.33) (2025-03-11)


### Bug Fixes

* **deps:** update ruby packages ([6892fb2](https://gitlab.com/Orange-OpenSource/lfn/opnfv/xtesting-api/commit/6892fb29e8ed3f2085fd38a5f8708280529bfb5b))

## [1.15.32](https://gitlab.com/Orange-OpenSource/lfn/opnfv/xtesting-api/compare/1.15.31...1.15.32) (2025-03-11)


### Bug Fixes

* **deps:** update alpine packages ([6eef6f2](https://gitlab.com/Orange-OpenSource/lfn/opnfv/xtesting-api/commit/6eef6f20d0655dbd6a70fa13e554793e1f8f926f))

## [1.15.31](https://gitlab.com/Orange-OpenSource/lfn/opnfv/xtesting-api/compare/1.15.30...1.15.31) (2025-02-27)


### Bug Fixes

* **deps:** update ruby packages ([cf7d6f7](https://gitlab.com/Orange-OpenSource/lfn/opnfv/xtesting-api/commit/cf7d6f75726384197864c93cf338e3d298a55af0))

## [1.15.30](https://gitlab.com/Orange-OpenSource/lfn/opnfv/xtesting-api/compare/1.15.29...1.15.30) (2025-02-26)


### Bug Fixes

* **deps:** update ruby packages ([a05cd90](https://gitlab.com/Orange-OpenSource/lfn/opnfv/xtesting-api/commit/a05cd90b798a168bb6268a873cb9833b91d3de1d))

## [1.15.29](https://gitlab.com/Orange-OpenSource/lfn/opnfv/xtesting-api/compare/1.15.28...1.15.29) (2025-02-24)


### Bug Fixes

* **deps:** update dependency alpine_3_21/postgresql17-client to v17.4-r0 ([0ae0f88](https://gitlab.com/Orange-OpenSource/lfn/opnfv/xtesting-api/commit/0ae0f882b53d7f304eb18bc7ad4e76537e25fefe))
* **deps:** update dependency ruby to v3.4.2 ([ea8d93c](https://gitlab.com/Orange-OpenSource/lfn/opnfv/xtesting-api/commit/ea8d93c4c27e4025be92f4e8bde6a690e988f483))
* **deps:** update ruby packages ([974419b](https://gitlab.com/Orange-OpenSource/lfn/opnfv/xtesting-api/commit/974419b9ff3b1c9525b3ade0bb2eca8cd9338769))

## [1.15.28](https://gitlab.com/Orange-OpenSource/lfn/opnfv/xtesting-api/compare/1.15.27...1.15.28) (2025-02-20)


### Bug Fixes

* **deps:** update dependency rubygems/bundler to v2.6.5 ([5ecbd65](https://gitlab.com/Orange-OpenSource/lfn/opnfv/xtesting-api/commit/5ecbd659d1352a7fb5aee078d6bccac19bc85432))

## [1.15.27](https://gitlab.com/Orange-OpenSource/lfn/opnfv/xtesting-api/compare/1.15.26...1.15.27) (2025-02-20)


### Bug Fixes

* **deps:** update ruby packages ([bcfde45](https://gitlab.com/Orange-OpenSource/lfn/opnfv/xtesting-api/commit/bcfde451d6ccaa5ca083e1a311d8dbd7a53d99c6))

## [1.15.26](https://gitlab.com/Orange-OpenSource/lfn/opnfv/xtesting-api/compare/1.15.25...1.15.26) (2025-02-18)


### Bug Fixes

* **deps:** update dependency rubygems/bundler to v2.6.4 ([9132aee](https://gitlab.com/Orange-OpenSource/lfn/opnfv/xtesting-api/commit/9132aeefb045bb57f483dcde0b8ff880453e695b))

## [1.15.25](https://gitlab.com/Orange-OpenSource/lfn/opnfv/xtesting-api/compare/1.15.24...1.15.25) (2025-02-17)


### Bug Fixes

* **deps:** update ruby packages ([a3f1cdf](https://gitlab.com/Orange-OpenSource/lfn/opnfv/xtesting-api/commit/a3f1cdf42956fa65e06e5dddc816b9e5be0aae45))

## [1.15.24](https://gitlab.com/Orange-OpenSource/lfn/opnfv/xtesting-api/compare/1.15.23...1.15.24) (2025-02-15)


### Bug Fixes

* **deps:** update ruby packages ([00a29e1](https://gitlab.com/Orange-OpenSource/lfn/opnfv/xtesting-api/commit/00a29e1d643e9363527006805dc0a17bc195ecc0))

## [1.15.23](https://gitlab.com/Orange-OpenSource/lfn/opnfv/xtesting-api/compare/1.15.22...1.15.23) (2025-02-15)


### Bug Fixes

* **deps:** update ruby:3.4.1-alpine docker digest to 1b9cac2 ([dbbc640](https://gitlab.com/Orange-OpenSource/lfn/opnfv/xtesting-api/commit/dbbc64026eaa23df92bc3758c8c2c34e42cec1d6))

## [1.15.22](https://gitlab.com/Orange-OpenSource/lfn/opnfv/xtesting-api/compare/1.15.21...1.15.22) (2025-02-13)


### Bug Fixes

* **deps:** update alpine packages to v3.3.3-r0 ([0d87cca](https://gitlab.com/Orange-OpenSource/lfn/opnfv/xtesting-api/commit/0d87ccaa6f1d13ba85418d841580e3076e141276))

## [1.15.21](https://gitlab.com/Orange-OpenSource/lfn/opnfv/xtesting-api/compare/1.15.20...1.15.21) (2025-02-11)


### Bug Fixes

* **deps:** update alpine packages ([ad45e66](https://gitlab.com/Orange-OpenSource/lfn/opnfv/xtesting-api/commit/ad45e6630f3f5e5fa0b91f2334a3bfe34a2d3cb1))
* **deps:** update ruby packages ([c20b44d](https://gitlab.com/Orange-OpenSource/lfn/opnfv/xtesting-api/commit/c20b44dc2b84174dd81df2d9d201d7877689fcf1))

## [1.15.20](https://gitlab.com/Orange-OpenSource/lfn/opnfv/xtesting-api/compare/1.15.19...1.15.20) (2025-01-24)


### Bug Fixes

* **deps:** update dependency ruby to v3.4.1 ([dabad89](https://gitlab.com/Orange-OpenSource/lfn/opnfv/xtesting-api/commit/dabad892171ea0b992126faeecb32886f5d4304f))

## [1.15.19](https://gitlab.com/Orange-OpenSource/lfn/opnfv/xtesting-api/compare/1.15.18...1.15.19) (2025-01-24)


### Bug Fixes

* **deps:** update ruby packages ([28e1bd1](https://gitlab.com/Orange-OpenSource/lfn/opnfv/xtesting-api/commit/28e1bd11b0ed529acf1820d267211b4237654992))

## [1.15.18](https://gitlab.com/Orange-OpenSource/lfn/opnfv/xtesting-api/compare/1.15.17...1.15.18) (2025-01-21)


### Bug Fixes

* **deps:** update ruby packages ([149fb01](https://gitlab.com/Orange-OpenSource/lfn/opnfv/xtesting-api/commit/149fb01edb2a8cb585153b23b300578ec865ffc3))

## [1.15.17](https://gitlab.com/Orange-OpenSource/lfn/opnfv/xtesting-api/compare/1.15.16...1.15.17) (2025-01-20)


### Bug Fixes

* **deps:** update dependency rubygems/bundler to v2.6.3 ([c802fbc](https://gitlab.com/Orange-OpenSource/lfn/opnfv/xtesting-api/commit/c802fbc5ba4f67298296038e501e70807eb471e7))

## [1.15.16](https://gitlab.com/Orange-OpenSource/lfn/opnfv/xtesting-api/compare/1.15.15...1.15.16) (2025-01-20)


### Bug Fixes

* **deps:** update alpine packages to v1.37.0-r12 ([9a4f6cb](https://gitlab.com/Orange-OpenSource/lfn/opnfv/xtesting-api/commit/9a4f6cb38a539f78a9daa8b96909c59132b33722))

## [1.15.15](https://gitlab.com/Orange-OpenSource/lfn/opnfv/xtesting-api/compare/1.15.14...1.15.15) (2025-01-20)


### Bug Fixes

* **metrics:** ensure METRICS_DAYS is an integer ([848b268](https://gitlab.com/Orange-OpenSource/lfn/opnfv/xtesting-api/commit/848b268014eff5217a275d23946f81e594968fed))

## [1.15.14](https://gitlab.com/Orange-OpenSource/lfn/opnfv/xtesting-api/compare/1.15.13...1.15.14) (2025-01-16)


### Bug Fixes

* **deps:** update ruby:3.3.6-alpine docker digest to f73a08e ([4ed3032](https://gitlab.com/Orange-OpenSource/lfn/opnfv/xtesting-api/commit/4ed30321aa822360230de15338bb69ae4910e25d))

## [1.15.13](https://gitlab.com/Orange-OpenSource/lfn/opnfv/xtesting-api/compare/1.15.12...1.15.13) (2025-01-14)


### Bug Fixes

* **deps:** update ruby packages ([1eba6b8](https://gitlab.com/Orange-OpenSource/lfn/opnfv/xtesting-api/commit/1eba6b8adf12ff4f41a22172be8e66ce99db27a3))

## [1.15.12](https://gitlab.com/Orange-OpenSource/lfn/opnfv/xtesting-api/compare/1.15.11...1.15.12) (2025-01-09)


### Bug Fixes

* **deps:** update ruby:3.3.6-alpine docker digest to e7a1543 ([39e6c05](https://gitlab.com/Orange-OpenSource/lfn/opnfv/xtesting-api/commit/39e6c0524a89a7442f1b34b5ea9a423c5d16f0e3))

## [1.15.11](https://gitlab.com/Orange-OpenSource/lfn/opnfv/xtesting-api/compare/1.15.10...1.15.11) (2025-01-08)


### Bug Fixes

* **deps:** update dependency rubygems/bundler to v2.6.2 ([92c0341](https://gitlab.com/Orange-OpenSource/lfn/opnfv/xtesting-api/commit/92c034179f5abddfe6d5802d64177cb278e02bb4))

## [1.15.10](https://gitlab.com/Orange-OpenSource/lfn/opnfv/xtesting-api/compare/1.15.9...1.15.10) (2025-01-08)


### Bug Fixes

* don't use Orange proxies ([81f2f0a](https://gitlab.com/Orange-OpenSource/lfn/opnfv/xtesting-api/commit/81f2f0a77e04eb4daeb08703c7f47162ac30c2fe))

## [1.15.9](https://gitlab.com/Orange-OpenSource/lfn/opnfv/xtesting-api/compare/1.15.8...1.15.9) (2025-01-07)


### Bug Fixes

* **deps:** update ruby packages ([7404c48](https://gitlab.com/Orange-OpenSource/lfn/opnfv/xtesting-api/commit/7404c4822f392defdae21a2968aae4acdc6e2ecc))

## [1.15.8](https://gitlab.com/Orange-OpenSource/lfn/opnfv/xtesting-api/compare/1.15.7...1.15.8) (2025-01-07)


### Bug Fixes

* **deps:** update dependency alpine_3_21/nodejs to v22.11.0-r1 ([b743474](https://gitlab.com/Orange-OpenSource/lfn/opnfv/xtesting-api/commit/b7434740b08c3eee92e596300b18e0e3c04249c3))

## [1.15.7](https://gitlab.com/Orange-OpenSource/lfn/opnfv/xtesting-api/compare/1.15.6...1.15.7) (2024-12-20)


### Bug Fixes

* **deps:** update ruby packages ([e8358ad](https://gitlab.com/Orange-OpenSource/lfn/opnfv/xtesting-api/commit/e8358ad8fabece78438b323cd6d01e27b4d8f0a8))

## [1.15.6](https://gitlab.com/Orange-OpenSource/lfn/opnfv/xtesting-api/compare/1.15.5...1.15.6) (2024-12-19)


### Bug Fixes

* **deps:** update dependency rubygems/bundler to v2.6.1 ([34bdac2](https://gitlab.com/Orange-OpenSource/lfn/opnfv/xtesting-api/commit/34bdac2658650683c9681c5d87e3b05a5363b080))

## [1.15.5](https://gitlab.com/Orange-OpenSource/lfn/opnfv/xtesting-api/compare/1.15.4...1.15.5) (2024-12-19)


### Bug Fixes

* **deps:** update ruby packages ([0cb0bdc](https://gitlab.com/Orange-OpenSource/lfn/opnfv/xtesting-api/commit/0cb0bdc050dbc2b6028f245151b018e0b5fda8d5))

## [1.15.4](https://gitlab.com/Orange-OpenSource/lfn/opnfv/xtesting-api/compare/1.15.3...1.15.4) (2024-12-17)


### Bug Fixes

* **deps:** update dependency rubygems/bundler to v2.6.0 ([3e91e76](https://gitlab.com/Orange-OpenSource/lfn/opnfv/xtesting-api/commit/3e91e7618813e413fdd010428f5929c303adba99))

## [1.15.3](https://gitlab.com/Orange-OpenSource/lfn/opnfv/xtesting-api/compare/1.15.2...1.15.3) (2024-12-16)


### Bug Fixes

* **⬆️:** bump to alpine 3.21 ([38cd1b9](https://gitlab.com/Orange-OpenSource/lfn/opnfv/xtesting-api/commit/38cd1b909a9d22ca04115840bc2dfdd42fe01b27))
* **deps:** update ruby packages ([2e3339d](https://gitlab.com/Orange-OpenSource/lfn/opnfv/xtesting-api/commit/2e3339db74df204e14723b7980be20cc39000f15))

## [1.15.2](https://gitlab.com/Orange-OpenSource/lfn/opnfv/xtesting-api/compare/1.15.1...1.15.2) (2024-11-29)


### Bug Fixes

* **deps:** update ruby packages ([6f51672](https://gitlab.com/Orange-OpenSource/lfn/opnfv/xtesting-api/commit/6f51672f72ff03b0a6123256b0c312a763bbe53c))

## [1.15.1](https://gitlab.com/Orange-OpenSource/lfn/opnfv/xtesting-api/compare/1.15.0...1.15.1) (2024-11-26)


### Bug Fixes

* **deps:** update ruby packages ([529cc38](https://gitlab.com/Orange-OpenSource/lfn/opnfv/xtesting-api/commit/529cc384a309934556df6d3a8c6da48e31a0f1ca))

# [1.15.0](https://gitlab.com/Orange-OpenSource/lfn/opnfv/xtesting-api/compare/1.14.2...1.15.0) (2024-11-26)


### Bug Fixes

* **🎨:** refresh docker generation ([9550f9a](https://gitlab.com/Orange-OpenSource/lfn/opnfv/xtesting-api/commit/9550f9ad9636cfb8242e225f7f27f118acaa9726))


### Features

* **⬆️:** uses Rails 8.x defaults ([ed3dda9](https://gitlab.com/Orange-OpenSource/lfn/opnfv/xtesting-api/commit/ed3dda93e0853c9d34aca91163565a99492dd161))
