# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

# frozen_string_literal: true

# Pin npm packages by running ./bin/importmap

pin 'application'
pin 'boosted' # @5.3.2
# use unpkg.com as ga.jspm.io contains a broken popper package
pin '@popperjs/core', to: 'https://unpkg.com/@popperjs/core@2.11.2/dist/esm/index.js'
pin 'sortable', to: 'sortable-tablesort.js' # @3.2.2
