# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

# frozen_string_literal: true

require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module XTestingApi
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 8.0

    # Configuration for the application, engines, and railties goes here.
    #
    # These settings can be overridden in specific environments using the files
    # in config/environments, which are processed later.
    #
    # config.time_zone = "Central Time (US & Canada)"
    # config.eager_load_paths << Rails.root.join("extras")
    config.logger = ActiveSupport::TaggedLogging.logger($stdout)

    config.metrics_days = ENV.fetch('METRICS_DAYS', 21).to_i

    config.admin_group = ENV.fetch('ADMIN_GROUP', 'admin')

    config.rbac_enabled = false
    config.jwt_secret_key = nil
    config.jwt_algorithm = nil

    if ENV['OIDC_CONFIGURATION_URL']
      oidc_configuration_response = HTTParty.get(ENV['OIDC_CONFIGURATION_URL'],
                                                 headers: { 'Content-Type' => 'application/json' })

      jwks_response = HTTParty.get(oidc_configuration_response['jwks_uri'],
                                   headers: { 'Content-Type' => 'application/json' })
      key = jwks_response.parsed_response['keys'].first

      config.jwt_secret_key = key['kid']
      config.jwt_algorithm = key['alg']

      config.rbac_enabled = true
    end
  end
end
