# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

# frozen_string_literal: true

require 'prometheus/client/formats/text'

module Prometheus
  class Metrics
    REGISTRY = Prometheus::Client.registry
    RESULTS_LABELS = %i[
      pod_name
      project_name
      scenario
      case_name
      build_tag
      installer
    ].freeze

    PROJECTS_GAUGE = Prometheus::Client::Gauge.new(
      :xtesting_projects,
      docstring: 'Number of projects',
      labels: {}
    )

    RESULTS_GAUGE = Prometheus::Client::Gauge.new(
      :xtesting_results,
      docstring: 'Number of results',
      labels: RESULTS_LABELS
    )

    PASS_RESULTS_GAUGE = Prometheus::Client::Gauge.new(
      :xtesting_results_pass,
      docstring: 'Number of results PASS',
      labels: RESULTS_LABELS
    )

    RESULTS_DURATION_GAUGE = Prometheus::Client::Gauge.new(
      :xtesting_results_duration,
      docstring: 'Duration of results in seconds',
      labels: RESULTS_LABELS
    )

    REGISTRY.register(PROJECTS_GAUGE)
    REGISTRY.register(RESULTS_GAUGE)
    REGISTRY.register(PASS_RESULTS_GAUGE)
    REGISTRY.register(RESULTS_DURATION_GAUGE)

    def self.dump
      Prometheus::Client::Formats::Text.marshal(REGISTRY)
    end
  end
end
