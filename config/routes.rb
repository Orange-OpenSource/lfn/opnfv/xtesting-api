# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

# frozen_string_literal: true

# rubocop: disable Metrics/BlockLength
Rails.application.routes.draw do
  resources :results, only: %i[index show]

  get '/metrics', to: 'prometheus#metrics'

  namespace :api do
    namespace :v1 do
      resources :pods, only: %i[index create show], param: :name

      resources :projects, param: :name do
        resources :test_cases, path: 'cases', param: :name
      end

      resources :scenarios, only: [:update], param: :old_name

      resources :scenarios, except: [:update], param: :name do
        resources :installers, only: [:create]
        put 'owner', to: 'scenarios#update_owner'
        resources :versions, only: [:create]
        resources :installer_version_projects, only: [:create], path: 'projects'
        resources :customs, only: [:create]
        resources :ivp_scores, only: [:create], path: 'scores'
        resources :trust_indicators, only: [:create]

        delete 'customs', to: 'customs#destroy'
        delete 'projects', to: 'installer_version_projects#destroy'
        delete 'versions', to: 'versions#destroy'
        delete 'installers', to: 'installers#destroy'

        put 'installers', to: 'installers#update'
        put 'versions', to: 'versions#update'
        put 'projects', to: 'installer_version_projects#update'
        put 'customs', to: 'customs#update'
      end

      resources :results, only: %i[create index show]

      resources :deploy_results, only: %i[create index show], path: 'deployresults'
    end
  end

  root to: 'results#index'
end
# rubocop: enable Metrics/BlockLength
