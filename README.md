<!--
Software Name : NIF TZ
SPDX-FileCopyrightText: Copyright (c) Orange SA
SPDX-License-Identifier: Apache-2.0

This software is distributed under the Apache-2.0
,
See the "LICENSES" directory for more details.

Authors:
- Efflam Castel <efflam.castel@orange.com>
-->

# XTesting API

## Features

- [Endpoints](./ENDPOINTS.md)

## Configuration

XTesting-API is configured via environment variables

<!-- markdownlint-disable line-length -->
| Environment Variable     | Usage                                            | Default | Required |
|--------------------------|--------------------------------------------------|---------|----------|
| `SECRET_KEY_BASE`        | Rails required variable                          | nil     | true     |
| `OIDC_CONFIGURATION_URL` | URL for OIDC protocol                            | nil     | false    |
| `ADMIN_GROUP`            | admins will see all results when RBAC is enabled | `admin` | false    |
<!-- markdownlint-enable line-length -->

## Database Setup

As a Rails Application, it requires a `config/database.yml`,

See
[https://guides.rubyonrails.org/configuring.html#configuring-a-database](https://guides.rubyonrails.org/configuring.html#configuring-a-database)
for more details.

## Role Based Access Control (RBAC)

RBAC depends on the setup of an OpenID solution.

Set up the environment variable `OIDC_CONFIGURATION_URL` to enable RBAC.

### OIDC Setup

see
[https://openid.net/specs/openid-connect-discovery-1_0.html](https://openid.net/specs/openid-connect-discovery-1_0.html)
for more details.

`OIDC_CONFIGURATION_URL` MUST point to your realm configuration.

Users having `ADMIN_GROUP` in their groups are considered admins and will be
able to access every result.

## Monitoring

XTesting Metrics can be collected at `/metrics`

They currently include :

- The total number of Projects
- The total number of results
- The total number of results that PASSED
- The duration of the last result

Labels are : project_name, scenario, case_name, build_tag, pod_name, installer

## Development

### Requirements

- ruby 3.3.0
- postgresql

### Installation

```shell
bundle install
rails db:setup
```

### Run postgres

```shell
docker run -d -p 5432:5432 \
  -e POSTGRES_PASSWORD=$POSTGRES_PASSWORD \
  -e POSTGRES_USER=$POSTGRES_USER \
  postgres
```

## Contributing

- Clone the repo
- Create your feature branch
- Push your branch
- Create a merge request

## License

XTesting-API is licensed under the [Apache 2.0 License](./LICENSE.txt)
