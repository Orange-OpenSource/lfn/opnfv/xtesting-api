# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Sylvain Desbureaux <sylvain.desbureaux@orange.com>
# - Efflam Castel <efflam.castel@orange.com>


FROM ruby:3.4.2-alpine@sha256:cb6a5cb7303314946b75fa64c96d8116f838b8495ffa161610bd6aaaf9a70121 AS dependencies

ARG RAILS_ENV="production"
ARG NODE_ENV="production"
ENV RAILS_ENV="${RAILS_ENV}"
ENV NODE_ENV="${NODE_ENV}"
ENV RACK_ENV="${RAILS_ENV}"
ENV RAILS_LOG_TO_STDOUT=true
ENV RAILS_ROOT=/app
ENV LANG=C.UTF-8
ENV GEM_HOME=/bundle
ENV BUNDLE_PATH=$GEM_HOME
ENV BUNDLE_APP_CONFIG=$BUNDLE_PATH
ENV BUNDLE_BIN=$BUNDLE_PATH/bin
ENV PATH=/app/bin:$BUNDLE_BIN:/root/.yarn/bin:$PATH

# renovate: datasource=repology depName=alpine_3_21/build-base versioning=loose
ENV BUILD_BASE_VERSION=0.5-r3
# renovate: datasource=repology depName=rubygems/bundler versioning=loose
ENV BUNDLER_VERSION=2.6.5
# renovate: datasource=repology depName=alpine_3_21/libffi versioning=loose
ENV LIBFFI_VERSION=3.4.7-r0
# renovate: datasource=repology depName=alpine_3_21/libxml2-dev versioning=loose
ENV LIBXML2_DEV_VERSION=2.13.4-r5
# renovate: datasource=repology depName=alpine_3_21/libxslt-dev versioning=loose
ENV LIBXSLT_DEV_VERSION=1.1.42-r1
# renovate: datasource=repology depName=alpine_3_21/linux-headers versioning=loose
ENV LINUX_HEADERS_VERSION=6.6-r1
# renovate: datasource=repology depName=alpine_3_21/nodejs versioning=loose
ENV NODEJS_VERSION=22.13.1-r0
# renovate: datasource=repology depName=alpine_3_21/openssl-dev versioning=loose
ENV OPENSSL_DEV_VERSION=3.3.3-r0
# renovate: datasource=repology depName=alpine_3_21/postgresql17-client versioning=loose
ENV POSTGRESQL17_VERSION=17.4-r0
# renovate: datasource=repology depName=alpine_3_21/readline versioning=loose
ENV READLINE_VERSION=8.2.13-r0
# renovate: datasource=repology depName=alpine_3_21/pkgconf versioning=loose
ENV PKGCONF_VERSION=2.3.0-r0
# renovate: datasource=repology depName=alpine_3_21/libcrypto3 versioning=loose
ENV LIBCRYPTO3_VERSION=3.3.3-r0
# renovate: datasource=repology depName=alpine_3_21/libssl3 versioning=loose
ENV LIBSSL3_VERSION=3.3.3-r0
# renovate: datasource=repology depName=alpine_3_21/busybox versioning=loose
ENV BUSYBOX_VERSION=1.37.0-r12
# renovate: datasource=repology depName=alpine_3_21/busybox-binsh versioning=loose
ENV BUSYBOX_BINSH_VERSION=1.37.0-r12
# renovate: datasource=repology depName=alpine_3_21/ssl_client versioning=loose
ENV SSL_CLIENT_VERSION=1.37.0-r12
# renovate: datasource=repology depName=alpine_3_21/yaml versioning=loose
ENV YAML_VERSION=0.2.5-r2

WORKDIR $RAILS_ROOT

COPY Gemfile* ./
RUN apk add --no-cache --virtual runtime-deps \
    busybox="${BUSYBOX_VERSION}" \
    busybox-binsh="${BUSYBOX_BINSH_VERSION}" \
    libcrypto3="${LIBCRYPTO3_VERSION}" \
    libffi="${LIBFFI_VERSION}" \
    libssl3="${LIBSSL3_VERSION}" \
    nodejs="${NODEJS_VERSION}" \
    postgresql17-client="${POSTGRESQL17_VERSION}" \
    readline="${READLINE_VERSION}" \
    ssl_client="${SSL_CLIENT_VERSION}" \
    &&\
    apk add --no-cache --virtual build-deps \
    build-base="${BUILD_BASE_VERSION}" \
    libxml2-dev="${LIBXML2_DEV_VERSION}" \
    libxslt-dev="${LIBXSLT_DEV_VERSION}"\
    linux-headers="${LINUX_HEADERS_VERSION}" \
    openssl-dev="${OPENSSL_DEV_VERSION}" \
    pkgconf="${PKGCONF_VERSION}" \
    postgresql17-dev="${POSTGRESQL17_VERSION}" \
    readline-dev="${READLINE_VERSION}" \
    yaml-dev="${YAML_VERSION}" \
    &&\
    gem install bundler -v "${BUNDLER_VERSION}" && \
    bundle config set without 'test development' &&\
    bundle check || bundle install -j "$(getconf _NPROCESSORS_ONLN)" && \
    rm -rf $BUNDLE_PATH/cache/*.gem && \
    find $BUNDLE_PATH/gems/ -name "*.c" -delete &&\
    find $BUNDLE_PATH/gems/ -name "*.o" -delete  &&\
    apk del build-deps

COPY . .
RUN if [ "${RAILS_ENV}" != "development" ]; then \
    apk add --no-cache --virtual build-deps \
    build-base="${BUILD_BASE_VERSION}" \
    libffi="${LIBFFI_VERSION}" \
    libxml2-dev="${LIBXML2_DEV_VERSION}" \
    libxslt-dev="${LIBXSLT_DEV_VERSION}"\
    linux-headers="${LINUX_HEADERS_VERSION}" \
    openssl-dev="${OPENSSL_DEV_VERSION}" \
    pkgconf="${PKGCONF_VERSION}" \
    postgresql17-dev="${POSTGRESQL17_VERSION}" \
    readline-dev="${READLINE_VERSION}" \
    yaml-dev="${YAML_VERSION}" \
    &&\
    SECRET_KEY_BASE_DUMMY=1 rails assets:precompile &&\
    apk del build-deps;\
    fi

CMD ["sh"]


FROM ruby:3.4.2-alpine@sha256:cb6a5cb7303314946b75fa64c96d8116f838b8495ffa161610bd6aaaf9a70121 AS app

ARG RAILS_ENV="production"
ARG NODE_ENV="production"
ENV RAILS_ENV="${RAILS_ENV}"
ENV NODE_ENV="${NODE_ENV}"
ENV RACK_ENV="${RAILS_ENV}"
ENV RAILS_LOG_TO_STDOUT=true
ENV RAILS_SERVE_STATIC_FILES=true
ENV RAILS_ROOT=/app
ENV LANG=C.UTF-8
ENV GEM_HOME=/bundle
ENV BUNDLE_PATH=$GEM_HOME
ENV BUNDLE_APP_CONFIG=$BUNDLE_PATH
ENV BUNDLE_BIN=$BUNDLE_PATH/bin
ENV PATH=/app/bin:$BUNDLE_BIN:$PATH
ENV RAILS_ROOT /app
ARG SECRET_KEY_BASE
WORKDIR $RAILS_ROOT

# renovate: datasource=repology depName=alpine_3_21/libffi versioning=loose
ENV LIBFFI_VERSION=3.4.7-r0
# renovate: datasource=repology depName=alpine_3_21/nodejs versioning=loose
ENV NODEJS_VERSION=22.13.1-r0
# renovate: datasource=repology depName=alpine_3_21/postgresql17-client versioning=loose
ENV POSTGRESQL17_VERSION=17.4-r0
# renovate: datasource=repology depName=alpine_3_21/readline versioning=loose
ENV READLINE_VERSION=8.2.13-r0
# renovate: datasource=repology depName=alpine_3_21/pkgconf versioning=loose
ENV PKGCONF_VERSION=2.3.0-r0
# renovate: datasource=repology depName=alpine_3_21/libcrypto3 versioning=loose
ENV LIBCRYPTO3_VERSION=3.3.3-r0
# renovate: datasource=repology depName=alpine_3_21/libssl3 versioning=loose
ENV LIBSSL3_VERSION=3.3.3-r0
# renovate: datasource=repology depName=alpine_3_21/libproc2 versioning=loose
ENV LIBPROC2_VERSION=4.0.4-r2
# renovate: datasource=repology depName=alpine_3_21/procps-ng versioning=loose
ENV PROCPS_NG_VERSION=4.0.4-r2
# renovate: datasource=repology depName=alpine_3_21/busybox versioning=loose
ENV BUSYBOX_VERSION=1.37.0-r12
# renovate: datasource=repology depName=alpine_3_21/busybox-binsh versioning=loose
ENV BUSYBOX_BINSH_VERSION=1.37.0-r12
# renovate: datasource=repology depName=alpine_3_21/ssl_client versioning=loose
ENV SSL_CLIENT_VERSION=1.37.0-r12


RUN apk add --no-cache --virtual runtime-deps \
    busybox="${BUSYBOX_VERSION}" \
    busybox-binsh="${BUSYBOX_BINSH_VERSION}" \
    libcrypto3="${LIBCRYPTO3_VERSION}" \
    libffi="${LIBFFI_VERSION}" \
    libproc2="${LIBPROC2_VERSION}" \
    libssl3="${LIBSSL3_VERSION}" \
    nodejs="${NODEJS_VERSION}" \
    pkgconf="${PKGCONF_VERSION}" \
    postgresql17-client="${POSTGRESQL17_VERSION}" \
    procps-ng="${PROCPS_NG_VERSION}" \
    readline="${READLINE_VERSION}" \
    ssl_client="${SSL_CLIENT_VERSION}"

EXPOSE 3000

RUN addgroup -g 10000 rails && adduser --no-create-home --uid 10001 --disabled-password --ingroup rails rails

COPY --chown=rails:rails --from=dependencies /bundle /bundle
COPY --chown=rails:rails --from=dependencies  /app .
USER rails

CMD ["bundle", "exec", "puma", "-Cconfig/puma.rb"]
