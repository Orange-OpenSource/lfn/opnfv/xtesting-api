# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

# frozen_string_literal: true

class DetailsJSONValidator < ActiveModel::Validator
  def validate(record)
    JSON.parse(record.details) if record.details
  rescue JSON::ParserError, TypeError => _e
    record.errors.add :details, 'Details MUST be valid JSON'
  end
end
