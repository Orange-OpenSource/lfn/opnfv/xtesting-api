# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

# frozen_string_literal: true

module API
  module V1
    class ProjectsController < APIController
      def index
        @projects = Project.all

        render json: {
          projects: @projects.map { |project| ProjectSerializer.new(project).serialize }
        }
      end

      def create
        @project = Project.new(project_params)
        if @project.save
          render json: { "href": api_v1_project_path(@project) }
        else
          render json: @project.errors, status: 400
        end
      end

      def show
        @project = Project.find_by(name: params[:name])
        if @project
          render json: ProjectSerializer.new(@project).serialize
        else
          head 404
        end
      end

      def destroy
        @project = Project.find_by(name: params[:name])
        if @project
          @project.delete
          head 204
        else
          head 404
        end
      end

      def update
        @project = Project.find_by(name: params[:name])
        if @project
          if @project.update(project_params)
            render json: ProjectSerializer.new(@project).serialize
          else
            render json: @project.errors, status: 400
          end
        else
          head 404
        end
      end

      private

      def project_params
        params.permit(:name, :description)
      end
    end
  end
end
