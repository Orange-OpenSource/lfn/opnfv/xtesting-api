# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

# frozen_string_literal: true

module API
  module V1
    class ScenariosController < APIController
      SCENARIO_PARAMETERS = [
        :name,
        { installers: InstallersController::INSTALLER_PARAMETERS }
      ].freeze

      def index
        render json: { scenarios: ScenarioSerializer.serialize(Scenario.all) }
      end

      def show
        @scenario = Scenario.find_by(name: params[:name])
        if @scenario
          render json: ScenarioSerializer.serialize(@scenario)
        else
          head 404
        end
      end

      def destroy
        @scenario = Scenario.find_by(name: params[:name])
        if @scenario
          head 204 if @scenario.destroy
        else
          head 404
        end
      end

      def create
        @scenario = Scenario.new(name: scenario_params[:name])
        @scenario.installers = build_installers(scenario_params)

        if @scenario.save
          render json: { "href": api_v1_scenario_path(@scenario) }
        else
          render json: @scenario.errors, status: 400
        end
      end

      def update
        @scenario = Scenario.find_by(name: params[:old_name])
        @scenario.name = params[:name]
        @scenario.save
        head 204
      end

      def update_owner
        @scenario = Scenario.find_by(name: params[:scenario_name])
        @scenario.user = User.find_by(name: params[:owner]) || User.new(name: params[:owner])

        if @scenario.save
          head 200
        else
          head 400
        end
      end

      private

      def scenario_params
        params.permit(SCENARIO_PARAMETERS)
      end
    end
  end
end
