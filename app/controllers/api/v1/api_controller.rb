# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

# frozen_string_literal: true

module API
  module V1
    class APIController < ActionController::API
      def build_installers(scenario_params)
        scenario_params.fetch(:installers) { [] }.map do |installer_params|
          installer = Installer.new(installer: installer_params[:installer])
          installer.versions = build_versions(installer_params)
          installer
        end
      end

      def build_versions(installer_params)
        installer_params.fetch(:versions) { [] }.map do |version_params|
          version = InstallerVersion.new(owner: version_params[:owner], version: version_params[:version])
          version.projects = build_projects(version_params)
          version
        end
      end

      def build_projects(version_params)
        version_params.fetch(:projects) { [] }.map do |project_params|
          project = InstallerVersionProject.new(project: Project.find_by(name: project_params[:project]))
          project.scores = build_scores(project_params)
          project.customs = build_customs(project_params)
          project.trust_indicators = build_trust_indicators(project_params)
          project
        end
      end

      def build_scores(project_params)
        project_params.fetch(:scores) { [] }.map do |score_params|
          IVPScore.new(score_params)
        end
      end

      def build_customs(project_params)
        project_params.fetch(:customs) { [] }.map do |custom|
          IVPCustom.new(value: custom)
        end
      end

      def build_trust_indicators(project_params)
        project_params.fetch(:trust_indicators) { [] }
                      .map do |trust_indicator_params|
          TrustIndicator.new(trust_indicator_params)
        end
      end
    end
  end
end
