# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

# frozen_string_literal: true

module API
  module V1
    class InstallersController < APIController
      INSTALLER_PARAMETERS = [
        :installer,
        { versions: VersionsController::VERSION_PARAMETERS }
      ].freeze

      def create
        @scenario = Scenario.find_by(name: params[:scenario_name])
        @scenario.installers << build_installers({ installers: installer_params[:_json] })
        if @scenario.save
          head 201
        else
          head 400
        end
      end

      def destroy
        @scenario = Scenario.find_by(name: params[:scenario_name])
        @installers = @scenario.installers.select { |installer| params[:_json].include?(installer.installer) }

        @installers.each(&:destroy)
        head 204
      end

      def update
        @scenario = Scenario.find_by(name: params[:scenario_name])
        @scenario.installers = build_installers({ installers: installer_params[:_json] })
        @scenario.save
        head 204
      end

      private

      def installer_params
        params.permit(_json: INSTALLER_PARAMETERS)
      end
    end
  end
end
