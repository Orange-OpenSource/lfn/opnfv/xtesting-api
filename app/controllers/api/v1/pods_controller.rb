# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

# frozen_string_literal: true

module API
  module V1
    class PodsController < APIController
      def index
        @pods = Pod.all
        render json: { pods: PodSerializer.serialize(@pods) }
      end

      def create
        @pod = Pod.new(pod_params)
        @pod.user = User.find_or_create_by(params[:creator])

        if @pod.save
          render json: { 'href': api_v1_pod_path(@pod) }
        else
          render json: @pod.errors, status: 400
        end
      end

      def show
        @pod = Pod.find_by(name: params[:name])
        if @pod
          render json: PodSerializer.serialize(@pod)
        else
          head 404
        end
      end

      private

      def pod_params
        params.permit(:name, :details)
      end
    end
  end
end
