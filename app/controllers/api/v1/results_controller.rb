# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

# frozen_string_literal: true

module API
  module V1
    class ResultsController < APIController
      PAGE_SIZE = 20

      before_action :load_hierarchy, only: :create

      def create
        @result = Result.new(result_params)
        @result.pod = @pod
        @result.test_case = @case
        @result.installer_version_project = @installer_version_project

        @result.details = params[:details]

        if @result.save
          render json: { href: api_v1_result_path(@result) }, status: 201
        else
          render json: @result.errors, status: 400
        end
      end

      def index
        @results = Result.order(created_at: :desc)
        filter_results(params)

        render json: {
                 pagination: {
                   current_page: page_param,
                   total_pages:
                 },
                 results: @results[(page_param - 1) * PAGE_SIZE..page_param * PAGE_SIZE - 1]
               },
               status: 200
      end

      def show
        @result = Result.find(params[:id])

        render json: @result
      end

      private

      def load_hierarchy
        @pod = Pod.find_by(name: params[:pod_name])
        @project = Project.find_by(name: params[:project_name])
        @case = TestCase.find_by(name: params[:case_name], project: @project)
        load_or_create_ivp
      end

      def load_or_create_ivp
        @scenario = Scenario.find_or_create_by(name: params[:scenario])
        @installer = Installer.find_or_create_by(installer: params[:installer], scenario: @scenario)
        @version = InstallerVersion.find_or_create_by(version: params[:version], installer: @installer)
        @installer_version_project = InstallerVersionProject.find_or_create_by(
          project: @project,
          installer_version: @version
        )
      end

      FILTERS = {
        case: ->(result, value) { result.case_name == value },
        buildTag: ->(result, value) { result.build_tag == value },
        scenario: ->(result, value) { result.scenario_name == value },
        project: ->(result, value) { result.project_name == value },
        criteria: ->(result, value) { result.criteria == value },
        pod: ->(result, value) { result.pod_name == value },
        installer: ->(result, value) { result.installer_name == value },
        from: ->(result, date) { result.start_date > date },
        to: ->(result, date) { result.stop_date > date },
        last: ->(result, days) { result.start_date > DateTime.now - days.to_i.days }
      }.freeze

      def filter_results(params)
        params.select { |key| FILTERS.keys.include? key.to_sym }.each do |key, filter|
          @results = @results.select { |result| FILTERS[key.to_sym].call(result, filter) }
        end
      end

      def result_params
        params.permit(%i[criteria start_date stop_date build_tag details page])
      end

      def page_param
        params.permit(:page)
        (params[:page] || 1).to_i
      end

      def total_pages
        (@results.size % PAGE_SIZE).zero? ? @results.size / PAGE_SIZE : @results.size / PAGE_SIZE + 1
      end
    end
  end
end
