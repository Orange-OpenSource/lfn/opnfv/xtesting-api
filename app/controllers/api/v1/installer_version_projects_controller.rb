# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

# frozen_string_literal: true

module API
  module V1
    class InstallerVersionProjectsController < APIController
      INSTALLER_VERSION_PROJECT_PARAMS = [
        :project,
        {
          trust_indicators: %i[
            date
            status
          ],
          scores: %i[
            date
            score
          ],
          customs: []
        }
      ].freeze

      before_action :fetch_version

      def create
        @version.projects << build_projects({ projects: project_params[:_json] })

        if @version.save
          head 201
        else
          render json: @version.errors, status: 400
        end
      end

      def update
        @version.projects = build_projects({ projects: project_params[:_json] })

        if @version.save
          head 201
        else
          render json: @version.errors, status: 400
        end
      end

      def destroy
        @version = InstallerVersion.find_by(version: params[:version])
        @projects = @version.projects.select { |project| params[:_json].include?(project.project_name) }

        @projects.each(&:destroy)

        head 204
      end

      private

      def fetch_version
        @scenario = Scenario.find_by(name: params[:scenario_name])
        @installer = Installer.find_by(installer: params[:installer], scenario: @scenario)
        @version = InstallerVersion.find_by(version: params[:version], installer: @installer)
      end

      def project_params
        params.permit(_json: INSTALLER_VERSION_PROJECT_PARAMS)
      end
    end
  end
end
