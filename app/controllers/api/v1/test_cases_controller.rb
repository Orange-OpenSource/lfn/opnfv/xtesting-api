# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

# frozen_string_literal: true

module API
  module V1
    class TestCasesController < APIController
      FLAT_PARAMETERS = %i[
        blocking
        catalog_description
        ci_loop
        criteria
        description
        name
        run
        tier
        trust
        url
        version
      ].freeze

      PERMITTED_PARAMETERS = FLAT_PARAMETERS + [
        { domains: [],
          tags: [],
          dependencies: [] }
      ].freeze

      def create
        @project = Project.find_by(name: params[:project_name])
        @test_case = build_test_case(params)
        @test_case.project = @project

        if @test_case.save
          head 201
        else
          render json: @test_case.errors, status: 400
        end
      end

      def update
        @project = Project.find_by(name: params[:project_name])
        @test_case = TestCase.find_by(name: params[:name], project: @project)
        build_test_case(test_case_params, @test_case)

        if @test_case.save
          head 204
        else
          render json: @test_case.errors, status: 400
        end
      end

      def show
        @project = Project.find_by(name: params[:project_name])
        @test_case = @project.test_cases.find { |test_case| test_case.name == params[:name] }

        if @test_case
          render json: TestCaseSerializer.serialize(@test_case)
        else
          head 404
        end
      end

      def index
        @project = Project.find_by(name: params[:project_name])
        render json: { 'testcases': TestCaseSerializer.serialize(@project.test_cases) }
      end

      def destroy
        @project = Project.find_by(name: params[:project_name])
        @test_case = @project.test_cases.find { |test_case| test_case.name == params[:name] }

        if @test_case
          head 204 if @test_case.destroy
        else
          head 404
        end
      end

      private

      def test_case_params
        params.permit(*PERMITTED_PARAMETERS)
      end

      def build_test_case(test_case_params, test_case = nil)
        test_case ||= TestCase.new
        FLAT_PARAMETERS.each do |param|
          test_case.send("#{param}=", test_case_params[param])
        end
        test_case.tags = build_tags(test_case_params)
        test_case.dependencies = build_dependencies(test_case_params)
        test_case.domains = build_domains(test_case_params)
        test_case
      end

      def build_tags(params)
        params.fetch(:tags) { [] }.map do |value|
          Tag.find_or_create_by(value:)
        end
      end

      def build_dependencies(params)
        params.fetch(:dependencies) { [] }.map do |dependency|
          TestCaseDependency.new(name: dependency)
        end
      end

      def build_domains(params)
        params.fetch(:domains) { [] }.map do |domain|
          TestCaseDomain.new(name: domain)
        end
      end
    end
  end
end
