# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

# frozen_string_literal: true

module API
  module V1
    class TrustIndicatorsController < APIController
      before_action :fetch_project

      def create
        @ivp.trust_indicators << build_trust_indicators({ trust_indicators: trust_indicators_params[:_json] })

        if @ivp.save
          head 201
        else
          render json: @ivp.errors, status: 400
        end
      end

      private

      def trust_indicators_params
        params.permit(_json: %i[date status])
      end

      def fetch_project
        @scenario = Scenario.find_by(name: params[:scenario_name])
        @installer = Installer.find_by(installer: params[:installer], scenario: @scenario)
        @version = InstallerVersion.find_by(version: params[:version], installer: @installer)
        @project = Project.find_by(name: params[:project])
        @ivp = InstallerVersionProject.find_by(project: @project, installer_version: @version)
      end
    end
  end
end
