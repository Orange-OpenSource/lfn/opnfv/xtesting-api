# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

# frozen_string_literal: true

module API
  module V1
    class VersionsController < APIController
      VERSION_PARAMETERS = [
        :owner,
        :version,
        { projects: InstallerVersionProjectsController::INSTALLER_VERSION_PROJECT_PARAMS }
      ].freeze

      before_action :fetch_installer

      def create
        @installer.versions << build_versions({ versions: version_params[:_json] })

        if @installer.save
          head 201
        else
          render json: @installer.errors, status: :bad_request
        end
      end

      def destroy
        @versions = @installer.versions.select { |version| params[:_json].include?(version.version) }

        @versions.each(&:destroy)
        head 204
      end

      def update
        @installer.versions = build_versions({ versions: version_params[:_json] })
      end

      private

      def fetch_installer
        @scenario = Scenario.find_by(name: params[:scenario_name])
        @installer = Installer.find_by(installer: params[:installer], scenario: @scenario)
      end

      def version_params
        params.permit(_json: VERSION_PARAMETERS)
      end
    end
  end
end
