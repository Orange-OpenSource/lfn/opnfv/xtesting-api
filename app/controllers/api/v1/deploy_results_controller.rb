# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

# frozen_string_literal: true

module API
  module V1
    class DeployResultsController < APIController
      before_action :load_installer_version, only: [:create]

      def create
        @pod = Pod.find_by(name: params[:pod_name])

        @deploy_result = DeployResult.new(deploy_result_params)
        @deploy_result.installer_version = @installer_version
        @deploy_result.pod = @pod

        if @deploy_result.save
          head 201
        else
          render json: @deploy_result.errors, status: :bad_request
        end
      end

      def index
        @deploy_results = DeployResult.all

        render json: {
          pagination: {},
          deployresults: @deploy_results
        }
      end

      def show
        @deploy_result = DeployResult.find(params[:id])

        render json: @deploy_result
      end

      private

      def deploy_result_params
        params.permit(:build_id, :stop_date, :start_date, :job_name, :upstream_job_name, :details, :upstream_build_id,
                      :criteria)
      end

      def load_installer_version
        @scenario = Scenario.find_by(name: params[:scenario])
        @installer = Installer.find_by(installer: params[:installer], scenario: @scenario)
        @installer_version = InstallerVersion.find_by(version: params[:version], installer: @installer)
      end
    end
  end
end
