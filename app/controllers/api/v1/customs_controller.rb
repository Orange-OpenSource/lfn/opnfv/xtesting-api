# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

# frozen_string_literal: true

module API
  module V1
    class CustomsController < APIController
      before_action :fetch_project

      def create
        @installer_version_project.customs << build_customs({ customs: customs_params[:_json] })

        if @installer_version_project.save
          head 201
        else
          render json: @installer_version_project.errors, status: 400
        end
      end

      def destroy
        @customs = @installer_version_project.customs.select { |custom| params[:_json].include? custom.value }

        @customs.each(&:destroy)

        head 204
      end

      def update
        @installer_version_project.customs = build_customs({ customs: customs_params[:_json] })

        if @installer_version_project.save
          head 201
        else
          render json: @installer_version_project.errors, status: 400
        end
      end

      private

      def customs_params
        params.permit(_json: [])
      end

      def fetch_project
        @scenario = Scenario.find_by(name: params[:scenario_name])
        @installer = Installer.find_by(installer: params[:installer], scenario: @scenario)
        @version = InstallerVersion.find_by(version: params[:version], installer: @installer)
        @project = Project.find_by(name: params[:project])
        @installer_version_project = InstallerVersionProject.find_by(project: @project, installer_version: @version)
      end
    end
  end
end
