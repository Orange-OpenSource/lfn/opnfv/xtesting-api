# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

# frozen_string_literal: true

class PrometheusController < ApplicationController
  def metrics
    set_project_metrics
    set_results_metrics

    render plain: Prometheus::Metrics.dump
  end

  def set_project_metrics
    Prometheus::Metrics::PROJECTS_GAUGE.set(Project.count)
  end

  def set_results_metrics
    group_results
    set_metrics_keys
    set_gauges
  end

  def group_results
    request = Result.left_joins(
      :pod,
      :test_case,
      installer_version_project: [:project, { installer_version: [installer: [:scenario]] }]
    ).group('pods.name', 'projects.name', 'scenarios.name', 'test_cases.name', :build_tag, 'installers.installer')
                    .where(created_at: metrics_range)

    @counts = request.count(:id)
    @passed_counts = request.where(criteria: 'PASS').count(:id)
    @durations = request.sum(Result.sql_duration_in_seconds)
  end

  private

  def set_metrics_keys
    @counts.transform_keys! do |keys|
      Hash[Prometheus::Metrics::RESULTS_LABELS.zip(keys)]
    end

    @passed_counts.transform_keys! do |keys|
      Hash[Prometheus::Metrics::RESULTS_LABELS.zip(keys)]
    end

    @durations.transform_keys! do |keys|
      Hash[Prometheus::Metrics::RESULTS_LABELS.zip(keys)]
    end
  end

  def set_gauges
    @counts.each do |labels, value|
      Prometheus::Metrics::RESULTS_GAUGE.set(value, labels:)
    end
    @passed_counts.each do |labels, value|
      Prometheus::Metrics::PASS_RESULTS_GAUGE.set(value, labels:)
    end

    @durations.each do |labels, value|
      Prometheus::Metrics::RESULTS_DURATION_GAUGE.set(value, labels:)
    end
  end

  def metrics_range
    Rails.application.config.metrics_days.days.ago..Time.current
  end
end
