# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

# frozen_string_literal: true

class ApplicationController < ActionController::Base
  def decode_token
    auth_header = request.headers['Authorization']
    return {} unless auth_header&.start_with?('Bearer ')

    token = auth_header[7..]

    info = JWT.decode(
      token,
      Rails.application.config.jwt_secret_key,
      false,
      { algorithm: Rails.application.config.jwt_algorithm }
    )
    info.first || {}
  end

  def groups
    if Rails.application.config.rbac_enabled
      decode_token.fetch('groups') { [] }
    else
      [Rails.application.config.admin_group]
    end
  end
end
