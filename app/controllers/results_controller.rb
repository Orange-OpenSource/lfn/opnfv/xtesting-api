# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

# frozen_string_literal: true

class ResultsController < ApplicationController
  def show
    @result = Result.find(params[:id])
    return if @result.authorized?(groups)

    head :forbidden
  end

  def index
    @results = Result.left_outer_joins(test_case: :tags)
                     .where(filters)
                     .order(created_at: :desc)
    rbac_filter
    @results = @results.page params[:page]
    @tags = Tag.all
  end

  def search_date_range
    return (Date.today)..(Date.today + 1.days) unless params['[from(1i)]']

    search_from..search_to
  end

  private

  def rbac_filter
    return unless Rails.application.config.rbac_enabled && !groups.include?(Rails.application.config.admin_group)

    @results = @results.select { |result| groups.any? { |group| group.start_with?(result.build_tag) } }
    @results = Kaminari.paginate_array(@results)
  end

  def filters
    filters = {
      start_date: search_date_range,
      stop_date: search_date_range
    }
    filters[:tags] = { value: params['tags'] } if !params['tags'].nil? && !params['tags'].empty?
    filters
  end

  def search_from
    Date.new(
      params['[from(1i)]'].to_i,
      params['[from(2i)]'].to_i,
      params['[from(3i)]'].to_i
    )
  end

  def search_to
    Date.new(
      params['[to(1i)]'].to_i,
      params['[to(2i)]'].to_i,
      params['[to(3i)]'].to_i
    ) + 1.days
  end
end
