// Software Name : NIF TZ
// SPDX-FileCopyrightText: Copyright (c) Orange SA
// SPDX-License-Identifier: Apache-2.0
//
// This software is distributed under the Apache-2.0
// ,
// See the "LICENSES" directory for more details.
//
// Authors:
// - Efflam Castel <efflam.castel@orange.com>

import { Application } from "@hotwired/stimulus";

const application = Application.start();

// Configure Stimulus development experience
application.debug = false;
window.Stimulus = application;

export { application };
