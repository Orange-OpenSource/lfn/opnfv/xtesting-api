// Software Name : NIF TZ
// SPDX-FileCopyrightText: Copyright (c) Orange SA
// SPDX-License-Identifier: Apache-2.0
//
// This software is distributed under the Apache-2.0
// ,
// See the "LICENSES" directory for more details.
//
// Authors:
// - Efflam Castel <efflam.castel@orange.com>

// Configure your import map in config/importmap.rb. Read more: https://github.com/rails/importmap-rails
import "sortable";
import "boosted";
