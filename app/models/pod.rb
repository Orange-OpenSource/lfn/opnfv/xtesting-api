# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

# frozen_string_literal: true

class Pod < ApplicationRecord
  belongs_to :user, optional: true

  validates_presence_of :name
  validates_uniqueness_of :name

  validates_with DetailsJSONValidator

  def user_name
    (user || NullUser.new).name
  end
end
