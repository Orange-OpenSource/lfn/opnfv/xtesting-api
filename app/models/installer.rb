# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

# frozen_string_literal: true

class Installer < ApplicationRecord
  belongs_to :scenario
  has_many :versions, class_name: 'InstallerVersion', dependent: :delete_all

  validates_uniqueness_of :installer, scope: :scenario

  delegate :name, to: :scenario, prefix: true
end
