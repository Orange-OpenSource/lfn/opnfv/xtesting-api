# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

# frozen_string_literal: true

class TestCase < ApplicationRecord
  belongs_to :project
  has_many :test_case_tags, dependent: :destroy
  has_many :tags, through: :test_case_tags
  has_many :dependencies, class_name: 'TestCaseDependency', dependent: :destroy
  has_many :domains, class_name: 'TestCaseDomain', dependent: :destroy

  validates_presence_of :name

  delegate :name, to: :project, prefix: true

  delegate :scenario_name, to: :project
end
