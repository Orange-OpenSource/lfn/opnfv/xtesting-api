# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

# frozen_string_literal: true

class Result < ApplicationRecord
  belongs_to :pod
  belongs_to :test_case
  belongs_to :installer_version_project

  attribute :details, default: {}

  def case_name
    test_case.name
  end

  delegate :scenario_name, to: :installer_version_project
  delegate :installer_name, to: :installer_version_project
  delegate :version, to: :installer_version_project

  delegate :project_name, to: :installer_version_project

  delegate :name, to: :pod, prefix: true

  def self.passed
    where(criteria: 'PASS')
  end

  def self.failed
    where(criteria: 'FAIL')
  end

  def duration
    stop_date - start_date
  end

  def passed?
    criteria == 'PASS'
  end

  def links
    if details.instance_of?(Hash)
      details.fetch('links') { [] }
    else
      []
    end
  end

  def authorized?(groups)
    groups.include?(Rails.configuration.admin_group) || groups.any? { |group| group.start_with?(build_tag) }
  end

  def self.sql_duration_in_seconds
    if ActiveRecord::Base.connection.adapter_name.downcase == 'postgresql'
      'EXTRACT(EPOCH FROM (results.stop_date - results.start_date))'
    elsif ActiveRecord::Base.connection.adapter_name.downcase == 'sqlite'
      'unixepoch(results.stop_date) - unixepoch(results.start_date)'
    end
  end
end
