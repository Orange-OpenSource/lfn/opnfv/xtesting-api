# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

# frozen_string_literal: true

class Project < ApplicationRecord
  belongs_to :user, optional: true
  has_many :test_cases

  validates_presence_of :name

  def to_param
    name
  end

  def user_name
    (user || NullUser.new).name
  end
end
