# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

# frozen_string_literal: true

class InstallerVersionProject < ApplicationRecord
  belongs_to :installer_version
  belongs_to :project

  has_many :trust_indicators
  has_many :customs, class_name: 'IVPCustom', dependent: :delete_all
  has_many :scores, class_name: 'IVPScore'

  delegate :name, to: :project, prefix: true

  delegate :scenario_name, to: :installer_version
  delegate :installer_name, to: :installer_version
  delegate :version, to: :installer_version
end
