# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

# frozen_string_literal: true

class InstallerVersion < ApplicationRecord
  belongs_to :installer
  has_many :projects, class_name: 'InstallerVersionProject', dependent: :delete_all

  delegate :scenario_name, to: :installer

  def installer_name
    installer.installer
  end
end
