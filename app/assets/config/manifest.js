// Software Name : NIF TZ
// SPDX-FileCopyrightText: Copyright (c) Orange SA
// SPDX-License-Identifier: Apache-2.0
//
// This software is distributed under the Apache-2.0
// ,
// See the "LICENSES" directory for more details.
//
// Authors:
// - Efflam Castel <efflam.castel@orange.com>

//= link_tree ../images
//= link_directory ../stylesheets .css
//= link_tree ../../javascript .js
//= link_tree ../../../vendor/javascript .js
