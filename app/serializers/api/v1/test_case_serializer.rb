# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

# frozen_string_literal: true

module API
  module V1
    class TestCaseSerializer
      def self.serialize(test_case)
        if test_case.respond_to?(:each)
          test_case.map do |t|
            serializable_hash(t)
          end
        else
          serializable_hash(test_case)
        end
      end

      # rubocop: disable Metrics/MethodLength
      def self.serializable_hash(test_case)
        {
          _id: test_case.id,
          name: test_case.name,
          project_name: test_case.project_name,
          run: test_case.run,
          description: test_case.description,
          tags: test_case.tags,
          creation_date: test_case.created_at,
          dependencies: test_case.dependencies,
          tier: test_case.tier,
          trust: test_case.trust,
          blocking: test_case.blocking,
          ci_loop: test_case.ci_loop,
          url: test_case.url,
          version: test_case.version,
          criteria: test_case.criteria,
          domains: test_case.domains,
          catalog_description: test_case.catalog_description
        }
      end
      # rubocop: enable Metrics/MethodLength
    end
  end
end
