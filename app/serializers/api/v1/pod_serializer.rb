# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

# frozen_string_literal: true

module API
  module V1
    class PodSerializer
      def self.serialize(pod)
        if pod.respond_to?(:each)
          pod.map do |p|
            serializable_hash(p)
          end
        else
          serializable_hash(pod)
        end
      end

      def self.serializable_hash(pod)
        {
          '_id' => pod.id,
          'creation_date' => pod.created_at,
          'name' => pod.name,
          'role' => pod.role,
          'creator' => pod.user_name,
          'details' => pod.details,
          'mode' => pod.mode
        }
      end
    end
  end
end
