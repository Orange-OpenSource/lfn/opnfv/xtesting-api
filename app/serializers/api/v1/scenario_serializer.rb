# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

# frozen_string_literal: true

module API
  module V1
    class ScenarioSerializer
      def self.serialize(scenario)
        if scenario.respond_to?(:each)
          scenario.map do |s|
            serializable_hash(s)
          end
        else
          serializable_hash(scenario)
        end
      end

      def self.serializable_hash(scenario)
        {
          '_id' => scenario.id,
          'creation_date' => scenario.created_at,
          'name' => scenario.name,
          'creator' => scenario.user_name,
          'installers' => scenario.installers
        }
      end
    end
  end
end
