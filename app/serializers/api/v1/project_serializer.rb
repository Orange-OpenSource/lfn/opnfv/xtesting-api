# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Efflam Castel <efflam.castel@orange.com>

# frozen_string_literal: true

module API
  module V1
    class ProjectSerializer
      def initialize(project)
        @project = project
      end

      def serialize
        {
          'name' => @project.name,
          '_id' => @project.id,
          'creator' => @project.user_name,
          'description' => @project.description,
          'creation_date' => @project.created_at
        }
      end
    end
  end
end
