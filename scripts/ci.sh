#!/bin/sh

# Software Name : NIF TZ
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the Apache-2.0
# ,
# See the "LICENSES" directory for more details.
#
# Authors:
# - Sylvain Desbureaux <sylvain.desbureaux@orange.com>

set -eu
echo "*** show environment value"
echo "RAILS_ENV: $RAILS_ENV"
echo "*** copy db configuration"
cp /app/scripts/database.yml /app/config/database.yml
cat /app/config/database.yml
echo "*** prepare DB"
rails db:prepare
echo "*** starting XTesting"
bundle exec puma -C/app/config/puma.rb
